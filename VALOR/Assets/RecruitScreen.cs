﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecruitScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button backButton;
	public Button settingBtn;
	public GameObject barracksBtnObject;
	public GameObject stableBtnObject;
	public GameObject workshopBtnObject;

	public ScrollRect scrollView;
	public GameObject barracksVerticalContent;
	public GameObject stableVerticalContent;
	public GameObject workshopVerticalContent;

	private Button barracksBtn;
	private Button stableBtn;
	private Button workshopBtn;

	private Image barracksBtnImage;
	private Image stableBtnImage;
	private Image workshopBtnImage;


	// Use this for initialization
	void Start () {
		this.setupScreen ();
		this.hideSelf ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.barracksBtnPressed ();
	}

	private void addObject() {
		this.barracksBtn = barracksBtnObject.GetComponent<Button> ();
		this.stableBtn = stableBtnObject.GetComponent<Button> ();
		this.workshopBtn = workshopBtnObject.GetComponent<Button> ();

		this.barracksBtnImage = barracksBtnObject.GetComponent<Image> ();
		this.stableBtnImage = stableBtnObject.GetComponent<Image> ();
		this.workshopBtnImage = workshopBtnObject.GetComponent<Image> ();
	}

	private void addListeners() {
		this.barracksBtn.onClick.AddListener (barracksBtnPressed);
		this.stableBtn.onClick.AddListener (stableBtnPressed);
		this.workshopBtn.onClick.AddListener (workshopBtnPressed);

		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}

	void barracksBtnPressed() {
		this.setBarracksBtnColor ();
		this.clearStableBtnColor ();
		this.clearWorkshopBtnColor ();

		scrollView.content = barracksVerticalContent.GetComponent<RectTransform> ();
		Utility.showGameObject (this.barracksVerticalContent);
		Utility.hideGameObject (this.stableVerticalContent);
		Utility.hideGameObject (this.workshopVerticalContent);
	}

	void stableBtnPressed() {
		this.setStableBtnColor ();
		this.clearBarracksBtnColor ();
		this.clearWorkshopBtnColor ();

		scrollView.content = stableVerticalContent.GetComponent<RectTransform> ();
		Utility.showGameObject (this.stableVerticalContent);
		Utility.hideGameObject (this.barracksVerticalContent);
		Utility.hideGameObject (this.workshopVerticalContent);
	}

	void workshopBtnPressed() {
		this.setWorkshopBtnColor ();
		this.clearBarracksBtnColor ();
		this.clearStableBtnColor ();

		scrollView.content = workshopVerticalContent.GetComponent<RectTransform> ();
		Utility.showGameObject (this.workshopVerticalContent);
		Utility.hideGameObject (this.barracksVerticalContent);
		Utility.hideGameObject (this.stableVerticalContent);
	}
		

	private void setBarracksBtnColor() {
		var tempColor = barracksBtnImage.color;
		tempColor.a = 255;
		barracksBtnImage.color = tempColor;
	}

	private void setStableBtnColor() {
		var tempColor = stableBtnImage.color;
		tempColor.a = 255;
		stableBtnImage.color = tempColor;
	}

	private void setWorkshopBtnColor() {
		var tempColor = workshopBtnImage.color;
		tempColor.a = 255;
		workshopBtnImage.color = tempColor;
	}

	private void clearBarracksBtnColor() {
		var tempColor = barracksBtnImage.color;
		tempColor.a = 0;
		barracksBtnImage.color = tempColor;
	}

	private void clearStableBtnColor() {
		var tempColor = stableBtnImage.color;
		tempColor.a = 0;
		stableBtnImage.color = tempColor;
	}

	private void clearWorkshopBtnColor() {
		var tempColor = workshopBtnImage.color;
		tempColor.a = 0;
		workshopBtnImage.color = tempColor;
	}







}
