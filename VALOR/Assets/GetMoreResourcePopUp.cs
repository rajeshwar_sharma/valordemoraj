﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetMoreResourcePopUp : MonoBehaviour {

	public GameObject getMoreGoldView;
	public Button closeButton;
	public Button getMoreGoldBtn;
	public GameObject buyWoodMaxCell;
	public GameObject buyClayMaxCell;
	public GameObject buyIronMaxCell;
	public GameObject buyForGoldCell;


	private Button increaseBtn;
	private Button decreaseBtn;
	private Text amountOfResource;
	private Button buyForGoldBtn;
	private Text buyForGoldText;
	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}
	
	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.hideSelf ();
		this.amountOfResource.text = "1";
	}

	private void addObject() {
		foreach (Button buttn in buyWoodMaxCell.GetComponentsInChildren<Button>()) {
			if (buttn.tag == "DecreaseBtn") {
				this.decreaseBtn = buttn;
			} 
			if (buttn.tag == "IncreaseBtn") {
				this.increaseBtn = buttn;
			}
		}
		foreach (Text text in buyWoodMaxCell.GetComponentsInChildren<Text>()) {
			if (text.tag == "AmountOfResource") {
				amountOfResource = text;
			} 
		}
		this.buyForGoldBtn = this.buyForGoldCell.GetComponent<Button> ();
		this.buyForGoldText = this.buyForGoldCell.GetComponentInChildren<Text> ();
	}

	private void addListeners() {
		this.closeButton.onClick.AddListener (closeBtnPressed);
		this.getMoreGoldBtn.onClick.AddListener (getMoreGoldBtnPressed);
		this.decreaseBtn.onClick.AddListener (decreaseBtnPressed);
		this.increaseBtn.onClick.AddListener (inceaseBtnPressed);
	}

	public void decreaseBtnPressed() {
		int amount = System.Convert.ToInt32(amountOfResource.text);
		if (amount > 1) {
			amountOfResource.text = (amount - 1).ToString ();
			int multipleAmt = ((amount - 1) * 5);
			buyForGoldText.text = "BUY FOR "+ multipleAmt + " GOLD";
		}
	}

	public void inceaseBtnPressed() {
		int amount = System.Convert.ToInt32(amountOfResource.text);
		amountOfResource.text = (amount + 1).ToString ();
		int multipleAmt = ((amount + 1) * 5);
		buyForGoldText.text = "BUY FOR "+ multipleAmt + " GOLD";
	}

	public void getMoreGoldBtnPressed() {
		if (getMoreGoldView.activeInHierarchy) {
			Utility.hideGameObject (this.getMoreGoldView);
		} else {
			Utility.showGameObjectAsLastSibling (this.getMoreGoldView);
		}
	}


	private void closeBtnPressed () {
		Utility.hideGameObject (this.gameObject);
	}

	public void hideSelf() {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf() {
		Utility.showGameObject (this.gameObject);
	}


}
