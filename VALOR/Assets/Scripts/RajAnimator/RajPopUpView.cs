﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class RajPopUpView : MonoBehaviour
{
	public Button showButton;
	public Button hideButton;
	public GameObject animatorPopUp;

	// Use this for initialization
	void Start ()
	{
		this.showButton.onClick.AddListener (showBtnClicked);
		this.hideButton.onClick.AddListener (hideBtnClicked);
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	void showBtnClicked(){
		animatorPopUp.SetActive (true);
	}
	void hideBtnClicked(){
		animatorPopUp.SetActive (false);
	}
}

