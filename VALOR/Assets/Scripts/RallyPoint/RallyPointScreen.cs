﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RallyPointScreen : MonoBehaviour {

	public Button troopsBtn;
	public Button commandBtn;
	public Button movementsBtn;
	public Button simulatorBtn;
	public GameObject troopsScreen;
	public GameObject commandsScreen;
	public GameObject movementScreen;
	public GameObject simulatorScreen;
	public GameObject backBtn;
	public GameObject settingsBtn;
	public GameObject settingsScreen;
	public Button rallyPointBtn;
	public GameObject rallyPointScreen;

	// Use this for initialization
	void Start () {
		this.setupView ();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.addListerners ();
		this.inActiveRallyPoint ();
		this.inActiveMovements ();
	}

	void addListerners() {
		this.troopsBtn.onClick.AddListener (showTroopsTab);
		this.commandBtn.onClick.AddListener (showCommands);
		this.simulatorBtn.onClick.AddListener (showSimulators);
		this.movementsBtn.onClick.AddListener (showMovements);
		this.backBtn.GetComponent<Button> ().onClick.AddListener (inActiveRallyPoint);
		this.settingsBtn.GetComponent<Button> ().onClick.AddListener (showSettingScreen);
		this.rallyPointBtn.onClick.AddListener (activeRallyPoint);
		this.simulatorBtn.onClick.AddListener (showSimulators);
	}

	void hideAllTabs() {
		this.inActiveTroopsScreen ();
		this.inActiveMovements ();
		this.inActiveSimulator ();
	}
		

	void showTroopsTab() {
		this.hideAllTabs ();
		this.activeTroops ();
	}

	void showCommands() {
		this.hideAllTabs ();
		this.activeCommands ();
	}

	void showMovements() {
		this.hideAllTabs ();
		this.activeMovements ();
	}

	void showSimulators() {
		this.hideAllTabs ();
		this.activeSimulator ();
	}


	/// <summary>
	/// Active / inActive logic for different tabs.
	/// </summary>

	void activeTroops() {
		Utility.showGameObject (this.troopsScreen);
	}

	void activeCommands() {
		Utility.showGameObject (this.commandsScreen);
	}

	void activeMovements() {
		Utility.showGameObject (this.movementScreen);
	}

	void activeSimulator() {
		Utility.showGameObject (this.simulatorScreen);
	}

	void inActiveTroopsScreen() {
		Utility.hideGameObject (this.troopsScreen);
	}

	void inActiveRallyPoint() {
		Utility.hideGameObject (this.gameObject);
	}

	void inActiveMovements() {
		Utility.hideGameObject (this.movementScreen);
	}

	void inActiveSimulator() {
		Utility.hideGameObject (this.simulatorScreen);
	}

	void activeRallyPoint() {
		Utility.showGameObject (this.gameObject);
	}
		

	void showSettingScreen() {
		this.settingsScreen.GetComponent<SettingsScreenOperations> ().openPopUp ();
	}
}
