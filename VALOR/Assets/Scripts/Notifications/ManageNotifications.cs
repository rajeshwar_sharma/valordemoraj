﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageNotifications : MonoBehaviour {

	public VerticalLayoutGroup notificationVerticalLayout;
	public GameObject notificationCell;
	private RectTransform notificationVerticalRect;

	// Use this for initialization
	void Start () {
		this.getObjects ();
		this.addObjectsToVerticallayout ();
	}

	void getObjects() {
		this.notificationVerticalLayout = GameObject.Find (Objects.Notifications.notificationVerticallayout).GetComponent<VerticalLayoutGroup> ();
		this.notificationVerticalRect = (RectTransform) this.notificationVerticalLayout.transform;
 	}	
	
	// Update is called once per frame
	void Update () {
		
	}

	void addObjectsToVerticallayout() { 
		this.notificationVerticalLayout.CalculateLayoutInputHorizontal ();
		this.notificationVerticalLayout.CalculateLayoutInputVertical ();
		for (int i = 0; i <= 3; i++) {
			GameObject newCell = Instantiate (notificationCell) as GameObject;
			newCell.transform.SetParent (notificationVerticalLayout.transform);
			newCell.transform.localScale = new Vector2 (1, 1);
			newCell.transform.localPosition = new Vector3 (0, 0, 0);
			//newCell.transform.position.Set (this.notificationVerticalLayout.transform.position.x,this.notificationCell.transform.position.y,this.notificationCell.transform.position.z);
		}
	}
}
