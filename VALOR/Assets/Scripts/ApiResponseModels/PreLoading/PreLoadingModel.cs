﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class PreLoadingModel{
	[JsonProperty(PropertyName = "loading_screen")]
	public Dictionary<string,int> loading_screen { get; set;}

	[JsonProperty(PropertyName = "current_server_time")]
	public string current_server_time { get; set;} 
}

