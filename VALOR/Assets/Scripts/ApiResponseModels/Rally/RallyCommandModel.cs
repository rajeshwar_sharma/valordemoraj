﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class RallyCommandModel
{
	[JsonProperty(PropertyName = "queue_rally")]
	public QueueRally queue_rally; 

	public Dictionary<string,int> troops_in_village { get; set;}

	public Dictionary<string,int> troops_total { get; set;}

	[JsonProperty(PropertyName = "support")]
	public Support support; 

	[JsonProperty(PropertyName = "current_server_time")]
	public string current_server_time { get; set;} 

	/*Not possible to parse*/
	[JsonProperty(PropertyName = "world")]
	public Dictionary<string, Dictionary<string, Dictionary<string,int>>> world { get; set;}

	[JsonProperty(PropertyName = "resources")]
	public Dictionary<string,int> resources { get; set;}

	[JsonProperty(PropertyName = "resources_last_update_time")]
	public Dictionary<string,string> resources_last_update_time { get; set;}

	[JsonProperty(PropertyName = "num_incoming_attacks")]
	public int num_incoming_attacks { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_attacks_current")]
	public int num_incoming_attacks_current { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_total")]
	public int num_incoming_total { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_merchants")]
	public int num_incoming_merchants { get; set;} 

	[JsonProperty(PropertyName = "new_mail")]
	public int new_mail { get; set;} 

	[JsonProperty(PropertyName = "new_report")]
	public int new_report { get; set;} 

	[JsonProperty(PropertyName = "villages_version")]
	public int villages_version { get; set;} 

	[JsonProperty(PropertyName = "prompts")]
	public PromptModel[] prompts; 

}


/// <summary>
/// Queue rally.
/// </summary>
[System.Serializable]
public class QueueRally
{
	[JsonProperty(PropertyName = "ra_i")]
	public string[] ra_i { get; set;} 

	[JsonProperty(PropertyName = "ra_o")]
	public string[] ra_o { get; set;} 

}

/// <summary>
/// Support.
/// </summary>
[System.Serializable]
public class Support
{
	[JsonProperty(PropertyName = "received")]
	public string[] received { get; set;} 

	[JsonProperty(PropertyName = "sent")]
	public string[] sent { get; set;} 

}


