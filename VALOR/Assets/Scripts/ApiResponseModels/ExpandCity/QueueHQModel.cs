﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class QueueHQModel {
	[JsonProperty(PropertyName = "name")]
	public string name { get; set;} 

	[JsonProperty(PropertyName = "level")]
	public int level { get; set;} 

	[JsonProperty(PropertyName = "duration")]
	public string[] duration;

	[JsonProperty(PropertyName = "end_time")]
	public string end_time { get; set;}

	[JsonProperty(PropertyName = "start_time")]
	public string start_time { get; set;}

	[JsonProperty(PropertyName = "assignment_id")]
	public int assignment_id { get; set;}

	[JsonProperty(PropertyName = "is_upgrade")]
	public int is_upgrade { get; set;}

	[JsonProperty(PropertyName = "speedup_timespan")]
	public int speedup_timespan { get; set;}

	[JsonProperty(PropertyName = "has_used_speedup")]
	public int has_used_speedup { get; set;}

	[JsonProperty(PropertyName = "key")]
	public string key { get; set;}

}
