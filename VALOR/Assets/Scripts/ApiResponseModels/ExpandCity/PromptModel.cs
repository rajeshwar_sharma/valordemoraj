﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class PromptModel  {
	[JsonProperty(PropertyName = "prompt_type")]
	public string prompt_type { get; set;} 

	[JsonProperty(PropertyName = "title")]
	public string title { get; set;} 

	[JsonProperty(PropertyName = "text")]
	public string text { get; set;} 

}
