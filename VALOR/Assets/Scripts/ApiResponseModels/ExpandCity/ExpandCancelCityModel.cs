﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class ExpandCancelCityModel{
	[JsonProperty(PropertyName = "quests")]
	public Dictionary<string,QuestModel> quests { get; set;}

	[JsonProperty(PropertyName = "queue_hq")]
	public QueueHQModel[] queue_hq; 

	[JsonProperty(PropertyName = "buildings")]
	public BuildingModel[] buildings; 

	[JsonProperty(PropertyName = "active_subscriptions")]
	public string[] active_subscriptions; 

	[JsonProperty(PropertyName = "can_demolish")]
	public int can_demolish { get; set;} 

	[JsonProperty(PropertyName = "has_free_name_change")]
	public int has_free_name_change { get; set;} 

	[JsonProperty(PropertyName = "current_population")]
	public int current_population { get; set;} 

	[JsonProperty(PropertyName = "current_server_time")]
	public string current_server_time { get; set;} 

	public Dictionary<string,int> resources { get; set;}

	public Dictionary<string,string> resources_last_update_time { get; set;}

	[JsonProperty(PropertyName = "num_incoming_attacks")]
	public int num_incoming_attacks { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_attacks_current")]
	public int num_incoming_attacks_current { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_total")]
	public int num_incoming_total { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_merchants")]
	public int num_incoming_merchants { get; set;} 

	[JsonProperty(PropertyName = "new_mail")]
	public int new_mail { get; set;} 

	[JsonProperty(PropertyName = "new_report")]
	public int new_report { get; set;} 

	[JsonProperty(PropertyName = "villages_version")]
	public int villages_version { get; set;} 

	[JsonProperty(PropertyName = "Success")]
	public string Success { get; set;} 

	[JsonProperty(PropertyName = "prompts")]
	public PromptModel[] prompts; 
}