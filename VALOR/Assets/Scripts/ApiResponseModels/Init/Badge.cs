﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Badge {
	public string description { get; set; }
	public string img { get; set; }
	public string name { get; set; }
	public int tier { get; set; }
}

