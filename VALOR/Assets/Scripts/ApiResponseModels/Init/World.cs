﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class World  {
	public Dictionary<string,AllWorldsDetailsModel[]> all_worlds_details { get; set;}
	public int has_edited_login;
	public int hide_login;
	public string[]  my_worlds_order;
	public List<QuestCategoryModel>  quest_categories;
	public Dictionary<string,WorldDetailModel> world_details { get; set;}
	public List<List<WorldSelectionValueModel>>  world_selection_order;
	public string[]  worlds_deleted;
	public string[]  worlds_in;
	public Dictionary<string,string[]> worlds_in_v2 { get; set;}
	public int[]  worlds_not_in;
	public Dictionary<string,int[]> worlds_not_in_v2 { get; set;}
}

[System.Serializable]
public class AllWorldsDetailsModel  {
	public List<WorldDetailValue> detail { get; set;}
}

[System.Serializable]
public class WorldDetailValue  {
	public string key {get; set;}
	public string value {get; set;}
}

[System.Serializable]
public class QuestCategoryModel  {
	public string cid;
	public string name;
	public int[] order;
}

[System.Serializable]
public class WorldDetailModel  {
	public string close;
	public string[] gate;
	public string join;
	public string open;
}

[System.Serializable]
public class WorldSelectionOrderModel  {
	public WorldSelectionValueModel[] order;
}

[System.Serializable]
public class WorldSelectionValueModel  {
	public string image;
	public string key;
	public string subtitle;
	public string title;
}


[System.Serializable]
public class WorldsIn_v2Model  {

}





