﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.Serialization;



	[System.Serializable]
	public class Tickets
	{
		public Data data { get; set; }
	}


	[System.Serializable]
	public class Data
	{
	[JsonProperty(PropertyName = "10000000010")]
		public DataType data_1 { get; set; }
	[JsonProperty(PropertyName = "10000000011")]
		public DataType data_2 { get; set; }
	[JsonProperty(PropertyName = "10000000012")]
		public DataType data_3 { get; set; }
	}


	[System.Serializable]
	public class DataType
	{
		public int @base { get; set; }
		public string base_text { get; set; }
		public int bonus { get; set; }
		public string bonus_text { get; set; }
		public int cost { get; set; }
		public bool enabled { get; set; }
		public int quantity { get; set; }
	}
	

