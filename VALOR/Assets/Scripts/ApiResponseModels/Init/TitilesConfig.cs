﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public partial class Welcome
{
	[JsonProperty("titles_config")]
	public TitlesConfig titlesConfig { get; set; }
}

public partial class TitlesConfig
{
	[JsonProperty("details")]
	public Details details { get; set; }
}

public partial class Details
{
	[JsonProperty("battlebaron")]
	public Battlebaron battlebaron { get; set; }

	[JsonProperty("born_of_fire")]
	public BornOfFire bornOfFire { get; set; }

	[JsonProperty("champion")]
	public Battlebaron champion { get; set; }

	[JsonProperty("chieftan")]
	public Battlebaron chieftan { get; set; }

	[JsonProperty("conqueror")]
	public Battlebaron conqueror { get; set; }

	[JsonProperty("decapolis")]
	public Battlebaron decapolis { get; set; }

	[JsonProperty("gambler")]
	public Battlebaron gambler { get; set; }

	[JsonProperty("guildmaster")]
	public BornOfFire guildmaster { get; set; }

	[JsonProperty("imperial")]
	public Battlebaron imperial { get; set; }

	[JsonProperty("invader")]
	public Battlebaron invader { get; set; }

	[JsonProperty("legend")]
	public Battlebaron legend { get; set; }

	[JsonProperty("lion")]
	public BornOfFire lion { get; set; }

	[JsonProperty("lone_wolf")]
	public Battlebaron loneWolf { get; set; }

	[JsonProperty("lord_of_tactics")]
	public BornOfFire lordOfTactics { get; set; }

	[JsonProperty("machiavellian")]
	public Battlebaron machiavellian { get; set; }

	[JsonProperty("newbie")]
	public BornOfFire newbie { get; set; }

	[JsonProperty("point_hoarder")]
	public Battlebaron pointHoarder { get; set; }

	[JsonProperty("selfpwn")]
	public BornOfFire selfpwn { get; set; }

	[JsonProperty("soldier")]
	public BornOfFire soldier { get; set; }

	[JsonProperty("speeddemon")]
	public Battlebaron speeddemon { get; set; }

	[JsonProperty("subjugator")]
	public Battlebaron subjugator { get; set; }

	[JsonProperty("trader")]
	public Battlebaron trader { get; set; }

	[JsonProperty("valorian")]
	public Battlebaron valorian { get; set; }

	[JsonProperty("valorian_elite")]
	public BornOfFire valorianElite { get; set; }

	[JsonProperty("valorian_officer")]
	public BornOfFire valorianOfficer { get; set; }

	[JsonProperty("warrior")]
	public Battlebaron warrior { get; set; }
}

public partial class Battlebaron
{
	[JsonProperty("0")]
	public TitleBattlebaron title { get; set; }

	[JsonProperty("1")]
	public BadgeBattlebaron firstBadge { get; set; }

	[JsonProperty("2")]
	public BadgeBattlebaron secondBadge { get; set; }

	[JsonProperty("3")]
	public BadgeBattlebaron thirdBadge { get; set; }

	[JsonProperty("4")]
	public BadgeBattlebaron4 fourthBadge { get; set; }
}

public partial class TitleBattlebaron
{
	[JsonProperty("image")]
	public string Image { get; set; }

	[JsonProperty("levels")]
	public string[] Levels { get; set; }

	[JsonProperty("obtain")]
	public string Obtain { get; set; }

	[JsonProperty("subtitle")]
	public string Subtitle { get; set; }

	[JsonProperty("text")]
	public string Text { get; set; }

	[JsonProperty("title")]
	public string Title { get; set; }

	[JsonProperty("type")]
	public string[] Type { get; set; }

	[JsonProperty("icon")]
	public string Icon { get; set; }
}

public partial class BadgeBattlebaron
{
	[JsonProperty("icon")]
	public string Icon { get; set; }

	[JsonProperty("next_level")]
	public string NextLevel { get; set; }

	[JsonProperty("text")]
	public string Text { get; set; }
}

public partial class BadgeBattlebaron4
{
	[JsonProperty("icon")]
	public string Icon { get; set; }

	[JsonProperty("text")]
	public string Text { get; set; }
}

public partial class BornOfFire
{
	[JsonProperty("0")]
	public BadgeBattlebaron bornOfFire { get; set; }
}
