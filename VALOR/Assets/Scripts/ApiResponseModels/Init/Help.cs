﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

/// <summary>
/// Help.
/// </summary>

[System.Serializable]
public class Help  {
	[JsonProperty(PropertyName = "110000")]
	public HelpLegal legal { get; set;}
	[JsonProperty(PropertyName = "120000")]
	public HelpData support { get; set;}
	[JsonProperty(PropertyName = "130000")]
	public HelpDataWithChangeLog changeLog { get; set;}
	[JsonProperty(PropertyName = "140000")]
	public HelpBasics basics { get; set;}
	[JsonProperty(PropertyName = "150000")]
	public Buildings buildings  { get; set;}
	[JsonProperty(PropertyName="160000")]
	public HelpUnits helpUnits { get; set;}
	[JsonProperty(PropertyName = "170000")]
	public HelpArtOfWar artOfWar { get; set;}
	[JsonProperty(PropertyName = "180000")]
	public HelpGuilds guild { get; set;}
	[JsonProperty(PropertyName="190000")]
	public HelpMap map { get; set;}
	[JsonProperty(PropertyName="200000")]
	public HelpAdvanced advance { get; set;}
	[JsonProperty(PropertyName="210000")]
	public HelpResearch research { get; set;}
	[JsonProperty(PropertyName="220000")]
	public HelpMisc misc { get; set;}
	public string title { get; set;}
}

/// <summary>
/// Help data.
/// </summary>

[System.Serializable]
public class HelpData {
	public string text { get; set;}
	public string title { get; set;}
}

/// <summary>
/// Help data with change log.
/// </summary>

[System.Serializable]
public class HelpDataWithChangeLog:HelpData  {
	public int is_changelog { get; set;}
}

/// <summary>
/// Legal.
/// </summary>

[System.Serializable]
public class HelpLegal {
	public int display_type { get; set;}
	public string title { get; set;}
	[JsonProperty(PropertyName = "110100")]
	public PmTerms pmTerms {get; set;}
	[JsonProperty(PropertyName = "110200")]
	public PrivacyPolicy privacyPolicy;
}

/// <summary>
/// Pm terms.
/// </summary>

[System.Serializable]	
public class PmTerms {
	[JsonProperty(PropertyName = "110101")]
	public HelpData part_1 {get; set;}
	[JsonProperty(PropertyName = "110102")]
	public HelpData part_2 {get; set;}
	[JsonProperty(PropertyName = "110103")]
	public HelpData part_3 {get; set;}
	[JsonProperty(PropertyName = "110104")]
	public HelpData part_4 {get; set;}
	[JsonProperty(PropertyName = "110105")]
	public HelpData part_5 {get; set;}
	[JsonProperty(PropertyName = "110106")]
	public HelpData part_6 {get; set;}
	[JsonProperty(PropertyName = "110107")]
	public HelpData part_7 {get; set;}
	[JsonProperty(PropertyName = "110108")]
	public HelpData part_8 {get; set;}
	[JsonProperty(PropertyName = "110109")]
	public HelpData part_9 {get; set;}
	[JsonProperty(PropertyName = "110110")]
	public HelpData part_10 {get; set;}
	[JsonProperty(PropertyName = "110111")]
	public HelpData part_11 {get; set;}
	[JsonProperty(PropertyName = "110112")]
	public HelpData part_12 {get; set;}
	[JsonProperty(PropertyName = "110113")]
	public HelpData part_13 {get; set;}
	[JsonProperty(PropertyName = "110114")]
	public HelpData part_14 {get; set;}
	[JsonProperty(PropertyName = "110115")]
	public HelpData part_15 {get; set;}
	[JsonProperty(PropertyName = "110116")]
	public HelpData part_16 {get; set;}
	[JsonProperty(PropertyName = "110117")]
	public HelpData part_17 {get; set;}
	[JsonProperty(PropertyName = "110118")]
	public HelpData part_18 {get; set;}
	public string title { get; set;}
}


/// <summary>
/// Privacy policy.
/// </summary>


[System.Serializable]	
public class PrivacyPolicy {
	[JsonProperty(PropertyName = "110201")]
	public HelpData part_1 {get; set;}
	[JsonProperty(PropertyName = "110202")]
	public HelpData part_2 {get; set;}
	[JsonProperty(PropertyName = "110203")]
	public HelpData part_3 {get; set;}
	[JsonProperty(PropertyName = "110204")]
	public HelpData part_4 {get; set;}
	[JsonProperty(PropertyName = "110205")]
	public HelpData part_5 {get; set;}
	[JsonProperty(PropertyName = "110206")]
	public HelpData part_6 {get; set;}
	[JsonProperty(PropertyName = "110207")]
	public HelpData part_7 {get; set;}
	[JsonProperty(PropertyName = "110208")]
	public HelpData part_8 {get; set;}
	[JsonProperty(PropertyName = "110209")]
	public HelpData part_9 {get; set;}
	[JsonProperty(PropertyName = "110210")]
	public HelpData part_10 {get; set;}
	[JsonProperty(PropertyName = "110211")]
	public HelpData part_11 {get; set;}
	[JsonProperty(PropertyName = "110212")]
	public HelpData part_12 {get; set;}
	[JsonProperty(PropertyName = "110213")]
	public HelpData part_13 {get; set;}
	public string title { get; set;}
}

/// <summary>
/// Basics.
/// </summary>

[System.Serializable]
public class HelpBasics  {
	public string title { get; set;}
	[JsonProperty(PropertyName = "140100")]
	public HelpData justStarting { get; set;} 
	[JsonProperty(PropertyName = "140200")]
	public HelpData resources { get; set;} 
	[JsonProperty(PropertyName = "140300")]
	public HelpData cityOverview { get; set;} 
	[JsonProperty(PropertyName = "140400")]
	public HelpData changeCityName { get; set;} 
	[JsonProperty(PropertyName = "140500")]
	public HelpData construction { get; set;} 
}

/// <summary>
/// Buildings.
/// </summary>

[System.Serializable]
public class Buildings {
	public string title { get; set;}
	[JsonProperty(PropertyName = "150100")]
	public PointsOverview pointsOverview;
	[JsonProperty(PropertyName = "150200")]
	public HelpData cityHall {get;set;}
	[JsonProperty(PropertyName = "150300")]
	public HelpData barracks {get;set;}
	[JsonProperty(PropertyName = "150400")]
	public HelpData stable {get;set;}
	[JsonProperty(PropertyName = "150500")]
	public HelpData workshop {get;set;}
	[JsonProperty(PropertyName = "150600")]
	public HelpAcademy academy {get;set;}
	[JsonProperty(PropertyName = "150700")]
	public HelpRallyPoint rallyPoint {get;set;}
	[JsonProperty(PropertyName = "150800")]
	public HelpData lumbermill {get;set;}
	[JsonProperty(PropertyName = "150900")]
	public HelpData quarry {get;set;}
	[JsonProperty(PropertyName = "151000")]
	public HelpData ironMine {get;set;}
	[JsonProperty(PropertyName = "151100")]
	public HelpData farm {get;set;}
	[JsonProperty(PropertyName = "151200")]
	public HelpData warehouse {get;set;}
	[JsonProperty(PropertyName = "151300")]
	public HelpData hidingPlace {get;set;}
	[JsonProperty(PropertyName = "151400")]
	public HelpData wall {get;set;}
	[JsonProperty(PropertyName = "151500")]
	public HelpData market {get;set;}
	[JsonProperty(PropertyName = "151600")]
	public HelpData forge {get;set;}

}

/// <summary>
/// Help academy.
/// </summary>

public class HelpAcademy
{
	[JsonProperty(PropertyName = "151601")]
	public HelpData takingOverCities { get; set; }
	[JsonProperty(PropertyName = "151602")]
	public HelpData loyalty { get; set; }
	[JsonProperty(PropertyName = "151603")]
	public HelpData buildingScholars { get; set; }
	[JsonProperty(PropertyName = "151604")]
	public HelpData cityCaptured { get; set; }
	public string text { get; set; }
	public string title { get; set; }
}

/// <summary>
/// Help rally point.
/// </summary>

public class HelpRallyPoint
{
	[JsonProperty(PropertyName = "150701")]
	public HelpData managingTroops { get; set; }
	[JsonProperty(PropertyName = "150702")]
	public HelpData simulatingBattle { get; set; }
	public string text { get; set; }
	public string title { get; set; }
}

/// <summary>
/// Points overview.
/// </summary>

[System.Serializable]
public class PointsOverview {
	public List<Menu> menu { get; set; }
	public List<int> table_width { get; set; }
	public string title { get; set; }
}

/// <summary>
/// Menu.
/// </summary>

[System.Serializable]
public class Menu
{
	public string label { get; set; }
	public List<List<object>> table { get; set; }
	public string title { get; set; }
}

/// <summary>
/// Help units.
/// </summary>

[System.Serializable]
public class HelpUnits
{
	[JsonProperty (PropertyName="160100")]
	public UnitsOverview unitsOverview { get; set;}
	[JsonProperty (PropertyName="160200")]
	public HelpData lancer { get; set;}
	[JsonProperty (PropertyName="160300")]
	public HelpData sentry { get; set;}
	[JsonProperty (PropertyName="160400")]
	public HelpData berserker { get; set;}
	[JsonProperty (PropertyName="160500")] 
	public HelpData scout {get; set;}
	[JsonProperty (PropertyName="160600")]
	public HelpData knight {get; set;}
	[JsonProperty (PropertyName="160700")]
	public HelpData guardian { get; set;}
	[JsonProperty (PropertyName="160800")]
	public HelpData ram { get; set;}
	[JsonProperty (PropertyName="160900")]
	public HelpData ballista { get; set;}
	[JsonProperty (PropertyName="161000")]
	public HelpData scholar { get; set;}
}

/// <summary>
/// Units overview.
/// </summary>

[System.Serializable]
public class UnitsOverview {
	public List<List<object>> table { get; set; }
	public int table_legend { get; set; }
	public List<int> table_width { get; set; }
	public string text { get; set; }
	public string text_bottom { get; set; }
	public string title { get; set; }
}

/// <summary>
/// Art of war.
/// </summary>

[System.Serializable]
public class HelpArtOfWar {
	public string title { get; set;}
	[JsonProperty(PropertyName = "170100")]
	public HelpData attacking { get; set;} 
	[JsonProperty(PropertyName = "170200")]
	public HelpData defending { get; set;} 
	[JsonProperty(PropertyName = "170300")]
	public HelpData supporting { get; set;} 
	[JsonProperty(PropertyName = "170400")]
	public HelpData cityBasicsDefences { get; set;} 
	[JsonProperty(PropertyName = "170500")]
	public HelpData morale { get; set;} 
	[JsonProperty(PropertyName = "170600")]
	public HelpData luck { get; set;} 
	[JsonProperty(PropertyName = "170700")]
	public HelpData barbarianCity { get; set;} 
	[JsonProperty(PropertyName = "170800")]
	public HelpData ballistas { get; set;} 
	[JsonProperty(PropertyName = "170900")]
	public HelpData simulator { get; set;} 
}

/// <summary>
/// Guilds.
/// </summary>

[System.Serializable]
public class HelpGuilds {
	public string title { get; set;}
	public string text { get; set;}

	[JsonProperty(PropertyName = "180100")]
	public HelpData guildFounder { get; set;}

	[JsonProperty(PropertyName = "180200")]
	public HelpNestedData guildAristocrasy {get;set;}

	[JsonProperty(PropertyName = "180300")]
	public HelpData leavingTheGuild {get;set;}

	[JsonProperty(PropertyName = "180400")]
	public HelpData findingAGuild { get; set;}
}

/// <summary>
/// Help nested data.
/// </summary>

[System.Serializable]
public class HelpNestedData {
	[JsonProperty(PropertyName = "180201")]
	public HelpData diplomancy;
	public string title;
	public string text;
}

/// <summary>
/// Map.
/// </summary>

[System.Serializable]
public class Map
{
	[JsonProperty(PropertyName="190100")]
	public HelpData cityDetails { get; set; }
	[JsonProperty(PropertyName="190200")]
	public HelpData playerDetails { get; set; }
	[JsonProperty(PropertyName="190300")]
	public HelpData guildDetails { get; set; }
	public string text { get; set; }
	public string title { get; set; }
}

/// <summary>
/// Advanced.
/// </summary>

[System.Serializable]
public class HelpAdvanced {
	[JsonProperty(PropertyName="200100")]
	public HelpData rankings { get; set;}
	[JsonProperty(PropertyName="200200")]
	public Inbox inbox { get; set;}
	[JsonProperty (PropertyName = "200300")]
	public HelpData QuestsScreen { get; set;}
	[JsonProperty(PropertyName="200400")]
	public HelpData wheelOfFortune { get; set;}
	[JsonProperty(PropertyName="200500")]
	public HelpData security { get; set;}
	[JsonProperty (PropertyName="200600")]
	public HelpData worldStats { get; set;}
	[JsonProperty(PropertyName ="200700")]
	public HelpData startingOver { get; set;}

}

/// <summary>
/// Inbox.
/// </summary>

[System.Serializable]
public class Inbox
{
	[JsonProperty (PropertyName="200201")]
	public HelpData mail { get; set; }
	[JsonProperty (PropertyName="200202")]
	public HelpData report { get; set; }
	public string text { get; set; }
	public string title { get; set; }
}

/// <summary>
/// Research.
/// </summary>

[System.Serializable]
public class HelpResearch {
	[JsonProperty (PropertyName="210100")]
	public HelpData lancer { get; set;}
	[JsonProperty (PropertyName="210200")]
	public HelpData sentry {get; set;}
	[JsonProperty (PropertyName="210300")]
	public HelpData berserker { get; set;}
	[JsonProperty (PropertyName="210400")]
	public HelpData scout { get; set;}
	[JsonProperty (PropertyName = "210500")]
	public HelpData knight {get; set;}
	[JsonProperty (PropertyName ="210600")]
	public HelpData gaurdian {get; set;}
	[JsonProperty (PropertyName="210700")]
	public HelpData ram { get; set;}
	[JsonProperty (PropertyName="210800")]
	public HelpData ballista { get; set;}
}

/// <summary>
/// Misc.
/// </summary>

[System.Serializable] 
public class HelpMisc {
	[JsonProperty (PropertyName="220300")]
	public HelpData store { get; set;}
	[JsonProperty (PropertyName="220200")]
	public HelpData bB_codes { get; set;}
	[JsonProperty (PropertyName="220100")]
	public HelpData forums { get; set;}
	public string title;
}
	
