﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuildInfoViewModel {

	public string nameText { get; set; }
	public string tagText { get; set; }
	public string memberText { get; set; }
	public string rankText { get; set; }
	public string totalPointsText { get; set; }
	public string averageText { get; set; }
	public string pointsOfBestText { get; set; }


	// Use this for initialization
	void Start () {
		
	}

}
