﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AndroidGold {
	public string gold_text;
	public int amount_owned;
	public string[] order;
	public Dictionary<string,GoldModel> data { get; set;}
}

[System.Serializable]
public class GoldModelCollection {
	public GoldModel gold_01;
	public GoldModel gold_02;
	public GoldModel gold_03;
	public GoldModel gold_04;
	public GoldModel gold_05;
	public GoldModel gold_06;
}

[System.Serializable]
public class GoldModel  {
	public int gold;
	public string image;
	public double price;
	public string sub_title;
	public string title;
}

[System.Serializable]
public class CheckStatusModel {
	public bool show_dialog;
	public string body;
	public string title;
	public string current_server_time;
}


