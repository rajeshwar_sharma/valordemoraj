﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Languages {
	public string de;
	public string en;
	public string es;
	public string fr;
	public string it;
}
