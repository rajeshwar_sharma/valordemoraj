﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.Serialization;

/// <summary>
/// Tutorial client detail.
/// </summary>

[System.Serializable]
public class TutorialClientDetail
{
	public string descr { get; set; }
	public string title { get; set; }
}