﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Newtonsoft.Json;

/// <summary>
/// Init model.
/// </summary>

[System.Serializable]
public class InitModel  {
	public AndroidGold android_google_gold  { get; set; }
	public string current_server_time  { get; set; }
	public Dictionary<string,Badge> badges  { get; set; }
	public PlayerName player_name { get; set;}
	public Help help { get; set;}
	public string help_version { get; set;}
	public int hide_login { get; set;}
	public Languages languages { get; set;}
	public Tickets tickets { get; set;}
	public TitlesConfig title_config { get; set;}
	public Dictionary<string,TutorialClientDetail> tutorial_client { get; set; }
	public World world  { get; set; }
	public Config config { get; set;}
}
	
