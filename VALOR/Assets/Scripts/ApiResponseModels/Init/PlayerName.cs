﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;


[System.Serializable]
public class PlayerName
{
	public int cost { get; set; }
	public string descr { get; set; }
	public string name { get; set; }
}

