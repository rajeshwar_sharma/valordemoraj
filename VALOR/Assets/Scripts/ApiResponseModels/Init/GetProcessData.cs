﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class GetProcessData  {

	[JsonProperty(PropertyName="chat_enabled")]
	public bool chatEnabled { get; set;}
	[JsonProperty(PropertyName="guild_channel")]
	public string guildChannel { get; set;}
	[JsonProperty(PropertyName="disable_v2_tutorial")]
	public bool disabletutorial { get; set;}
	[JsonProperty(PropertyName="tutorial_quests")]
	public TutorialQuests tutorialQuests { get; set;} 
	[JsonProperty(PropertyName="beginners_projection_on")]
	public int beginnersProtectionOn { get; set;}
	[JsonProperty(PropertyName="can_revoke")]
	public int canRevoke { get; set;}
	[JsonProperty(PropertyName="tutorial_progressive")]
	public string tutorialProgressive { get; set;}
	[JsonProperty(PropertyName="unlimited_consumables")]
	public int unlimitedConsumables { get; set;}
	[JsonProperty(PropertyName="attack_confirmation_boosts")]
	public int attackConfirmationBoosts { get; set;}
	[JsonProperty(PropertyName="flag_research")]
	public int flagResearch { get; set;}
	[JsonProperty(PropertyName="flag_market")]
	public int flagMarket { get; set;}
	//[JsonProperty(PropertyName="")] 
}

public class TutorialQuests {

}
