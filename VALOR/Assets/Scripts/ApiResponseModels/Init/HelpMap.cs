﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

	[System.Serializable]
	public class HelpMap
	{
		public int b_a { get; set; }
		public int b_b { get; set; }
		public int b_c { get; set; }
		public int b_f { get; set; }
		public int b_h { get; set; }
		public int b_hq { get; set; }
		public int b_i { get; set; }
		public int b_m { get; set; }
		public int b_r { get; set; }
		public int b_s { get; set; }
		public int b_st { get; set; }
		public int b_w { get; set; }
		public int b_wa { get; set; }
		public int b_wh { get; set; }
		public int b_wo { get; set; }
		public int barb_city { get; set; }
		public int @default { get; set; }
		public int details_c { get; set; }
		public int details_g { get; set; }
		public int details_p { get; set; }
		public int guild { get; set; }
		public int guild_d { get; set; }
		public int mail { get; set; }
		public int mail_report { get; set; }
		public int map { get; set; }
		public int market { get; set; }
		public int quests { get; set; }
		public int rally { get; set; }
		public int rankings { get; set; }
		public int rd_a { get; set; }
		public int rd_c { get; set; }
		public int rd_hc { get; set; }
		public int rd_lc { get; set; }
		public int rd_r { get; set; }
		public int rd_sc { get; set; }
		public int rd_sf { get; set; }
		public int rd_sw { get; set; }
		public int recruit { get; set; }
		public int reports { get; set; }
		public int research { get; set; }
		public int shrine { get; set; }
		public int simulator { get; set; }
		public int store { get; set; }
		public int troops { get; set; }
		public int wheel { get; set; }
		public int world_stats { get; set; }
	}


		
