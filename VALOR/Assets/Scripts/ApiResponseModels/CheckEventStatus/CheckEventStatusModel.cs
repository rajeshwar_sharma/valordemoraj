﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class CheckEventStatusModel{
	[JsonProperty(PropertyName = "show_dialog")]
	public bool show_dialog { get; set;} 

	[JsonProperty(PropertyName = "body")]
	public string body { get; set;} 

	[JsonProperty(PropertyName = "title")]
	public string title { get; set;}

	[JsonProperty(PropertyName = "current_server_time")]
	public string current_server_time { get; set;} 


}

