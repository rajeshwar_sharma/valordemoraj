﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class GetTranslationModel{
	[JsonProperty(PropertyName = "translations")]
	public Dictionary<string,string> translations { get; set;}

	[JsonProperty(PropertyName = "current_server_time")]
	public string current_server_time { get; set;} 
}

