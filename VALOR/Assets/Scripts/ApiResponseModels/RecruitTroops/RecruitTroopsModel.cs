﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class RecruitTroopsModel
{
	[JsonProperty(PropertyName = "tutorial_quests")]
	public Dictionary<string,string> tutorial_quests { get; set;}

	[JsonProperty(PropertyName = "academy")]
	public AcademyModel academy; 

	[JsonProperty(PropertyName = "queue_troops")]
	public QueueTroopsModel queue_troops; 

	[JsonProperty(PropertyName = "troops")]
	public TroopsModel troops; 

	[JsonProperty(PropertyName = "active_subscriptions")]
	public string[] active_subscriptions; 

	[JsonProperty(PropertyName = "current_population")]
	public string current_population { get; set;}  

	[JsonProperty(PropertyName = "current_server_time")]
	public string current_server_time { get; set;} 

	/*Not possible to parse*/
	[JsonProperty(PropertyName = "world")]
	public Dictionary<string, Dictionary<string, Dictionary<string,int>>> world { get; set;}

	[JsonProperty(PropertyName = "resources")]
	public Dictionary<string,int> resources { get; set;}

	[JsonProperty(PropertyName = "resources_last_update_time")]
	public Dictionary<string,string> resources_last_update_time { get; set;}

	[JsonProperty(PropertyName = "num_incoming_attacks")]
	public int num_incoming_attacks { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_attacks_current")]
	public int num_incoming_attacks_current { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_total")]
	public int num_incoming_total { get; set;} 

	[JsonProperty(PropertyName = "num_incoming_merchants")]
	public int num_incoming_merchants { get; set;} 

	[JsonProperty(PropertyName = "new_mail")]
	public int new_mail { get; set;} 

	[JsonProperty(PropertyName = "new_report")]
	public int new_report { get; set;} 

	[JsonProperty(PropertyName = "villages_version")]
	public int villages_version { get; set;} 




}

