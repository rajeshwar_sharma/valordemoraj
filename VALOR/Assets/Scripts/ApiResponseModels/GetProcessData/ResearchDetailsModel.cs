

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class ResearchDetailsModel  {

    [JsonProperty(PropertyName = "rd_sf")]
	public ResearchDetailKeyModel researchLancer;

    [JsonProperty(PropertyName = "rd_sw")]
	public ResearchDetailKeyModel researchSentry;

	[JsonProperty(PropertyName = "rd_a")]
	public ResearchDetailKeyModel researchBerserker;

	[JsonProperty(PropertyName = "rd_sc")]
	public ResearchDetailKeyModel researchScout;

	[JsonProperty(PropertyName = "rd_lc")]
	public ResearchDetailKeyModel researchKnight;

	[JsonProperty(PropertyName = "rd_hc")]
	public ResearchDetailKeyModel researchGuardian;

	[JsonProperty(PropertyName = "rd_r")]
	public ResearchDetailKeyModel researchRam;

	[JsonProperty(PropertyName = "rd_c")]
	public ResearchDetailKeyModel researchBallista;


	public Dictionary<string,int> status { get; set;}


	[JsonProperty(PropertyName = "max_points")]
    public int max_points { get; set;}

	[JsonProperty(PropertyName = "allocated_points")]
    public int allocated_points { get; set;}

    [JsonProperty(PropertyName = "reached_max_text")]
    public string reached_max_text { get; set;}



}

public class ResearchDetailKeyModel  {

    [JsonProperty(PropertyName = "research_details")]
    public string research_details { get; set;}  
    
    [JsonProperty(PropertyName = "level")]
    public int level { get; set;}  

 	[JsonProperty(PropertyName = "max_level")]
    public int max_level { get; set;}  

    [JsonProperty(PropertyName = "min_level")]
    public int min_level { get; set;}

 	[JsonProperty(PropertyName = "tables")]
    public ResearchDetailTableKeyModel[] tables; 
	
}


public class ResearchDetailTableKeyModel  {

	[JsonProperty(PropertyName = "header")]
    public string header { get; set;}  
    
	[JsonProperty(PropertyName = "table")]
    public string[][] table { get; set;}  


}


