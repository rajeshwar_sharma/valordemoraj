using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class GetProcessDataModel  {
  
    public Dictionary<string,QuestModel> quests { get; set;}
	public Dictionary<string,int> troops_in_village { get; set;}
	public Dictionary<string,int> troops_total { get; set;}
    [JsonProperty(PropertyName = "max_wall_level")]
	public int max_wall_level{ get; set;}
    [JsonProperty(PropertyName = "max_building_level")]
    public int max_building_level;
	[JsonProperty(PropertyName = "buildings")]
    public BuildingModel[] buildings;
    [JsonProperty(PropertyName = "city_name")]
    public CityName city_name;
    
	[JsonProperty(PropertyName = "building_details")]
    public BuildingDetailModel building_details;
    
	[JsonProperty(PropertyName = "queue_troops")]
    public QueueTroopsModel queue_troops;

    [JsonProperty(PropertyName = "troops")]
    public TroopsModel troops;

    [JsonProperty(PropertyName = "academy")]
    public AcademyModel academy;

    [JsonProperty(PropertyName = "research")]
    public ResearchModel[] research;

    [JsonProperty(PropertyName = "research_details")]
    public  ResearchDetailsModel research_details;

    [JsonProperty(PropertyName = "research_levels")]
    public  ResearchLevelsModel research_levels;

    [JsonProperty(PropertyName = "market_info")]
    public  MarketInfoModel market_info;

    [JsonProperty(PropertyName = "speed")]
    public  SpeedModel speed;

    [JsonProperty(PropertyName = "player_details")]
    public Dictionary<string,object> player_details { get; set;}

    public PlayerDetailsModel[] playerArray;
//
////    foreach(KeyValuePair<string, string> players in player_details)
////    {
////    [JsonProperty(PropertyName = players.Key)]
////    public  PlayerDetailsModel player; 
////
////    }
//
    [JsonProperty(PropertyName = "amulet")]
    public  AmuletModel amulet;

    [JsonProperty(PropertyName = "wheel")]
    public WheelModel wheel;

    [JsonProperty(PropertyName = "speed_up")]
    public SpeedUpModel[] speedUp;

    [JsonProperty(PropertyName = "buy_resources")]
    public BuyResourcesModel buyResources;

    [JsonProperty(PropertyName = "report_info")]
    public ReportInfoModel report_info;

    [JsonProperty(PropertyName = "guild_info")]
    public GuildInfoModel guild_info;

    [JsonProperty(PropertyName = "config_limits")]
    public ConfigLimitsModel config_limits;

    [JsonProperty(PropertyName = "resources_production")]
    public ResourcesProductionModel resources_production;

    [JsonProperty(PropertyName = "village")]
    public VillageModel village;

    [JsonProperty(PropertyName = "resources_capacity")]
    public int resources_capacity { get; set;}  

    [JsonProperty(PropertyName = "max_population")]
    public int max_population { get; set;}  

    [JsonProperty(PropertyName = "building_levels")]
    public BuildingLevelsModel building_levels;

    [JsonProperty(PropertyName = "current_population")]
    public int current_population { get; set;}  

    [JsonProperty(PropertyName = "total_points")]
    public int total_points { get; set;}  

    [JsonProperty(PropertyName = "resources")]
    public ResourcesModel resources;
}
	

[System.Serializable]
public class ResourcesModel {
    [JsonProperty(PropertyName = "r_w")]
    public string r_w { get; set;}  
    [JsonProperty(PropertyName = "r_c")]
    public string r_c { get; set;}  
    [JsonProperty(PropertyName = "r_i")]
    public string r_i { get; set;}  
}

[System.Serializable]
public class CityName {
    [JsonProperty(PropertyName = "name")]
    public string name { get; set;}  
    [JsonProperty(PropertyName = "descr")]
    public string descr { get; set;}  
    [JsonProperty(PropertyName = "cost")]
    public string cost { get; set;}  
}


[System.Serializable]
public class ResearchLevelsModel {
    [JsonProperty(PropertyName = "rd_sf")]
    public int researchLancer { get; set;}  
    [JsonProperty(PropertyName = "rd_sw")]
    public int researchSentry { get; set;}  
    [JsonProperty(PropertyName = "rd_a")]
    public int researchBerserker { get; set;}  
    [JsonProperty(PropertyName = "rd_sc")]
    public int researchScout { get; set;}  
    [JsonProperty(PropertyName = "rd_lc")]
    public int researchKnight { get; set;} 
    [JsonProperty(PropertyName = "rd_hc")]
    public int researchGuardian { get; set;}  
    [JsonProperty(PropertyName = "rd_r")]
    public int researchRam { get; set;} 
    [JsonProperty(PropertyName = "rd_c")]
    public int researchBallista { get; set;} 
}

[System.Serializable]
public class MarketInfoModel {
    [JsonProperty(PropertyName = "available_merchants")]
    public int available_merchants { get; set;}  
    [JsonProperty(PropertyName = "total_merchants")]
    public int total_merchants { get; set;}  
    [JsonProperty(PropertyName = "max_transport_amount")]
    public int max_transport_amount { get; set;}  
    [JsonProperty(PropertyName = "max_w")]
    public int max_w { get; set;}  
    [JsonProperty(PropertyName = "max_c")]
    public int max_c { get; set;} 
    [JsonProperty(PropertyName = "max_i")]
    public int max_i { get; set;}  
    [JsonProperty(PropertyName = "your_offers")]
    public string[] your_offers { get; set;} 
    [JsonProperty(PropertyName = "max_trade_radius")]
    public int max_trade_radius { get; set;} 
    [JsonProperty(PropertyName = "resources_min")]
    public int resources_min { get; set;} 
    [JsonProperty(PropertyName = "resources_interval")]
    public int resources_interval { get; set;} 
}

[System.Serializable]
public class SpeedModel {

    [JsonProperty(PropertyName = "t_sf")]
	public double lancer { get; set;}  
    [JsonProperty(PropertyName = "t_sw")]
	public double sentry { get; set;}  
    [JsonProperty(PropertyName = "t_a")]
	public double berserker { get; set;}  
    [JsonProperty(PropertyName = "t_sc")]
	public double scout { get; set;} 
    [JsonProperty(PropertyName = "t_lc")]
	public double knight { get; set;}  
    [JsonProperty(PropertyName = "t_hc")]
	public double guardian { get; set;} 
    [JsonProperty(PropertyName = "t_r")]
	public double ram { get; set;} 
    [JsonProperty(PropertyName = "t_c")]
	public double ballista { get; set;}  
    [JsonProperty(PropertyName = "t_n")]
	public double scholars { get; set;} 

}

[System.Serializable]
public class AmuletModel {
    [JsonProperty(PropertyName = "cost")]
    public int cost { get; set;}  
    [JsonProperty(PropertyName = "daily_limit")]
    public int daily_limit { get; set;}  
    [JsonProperty(PropertyName = "item_type")]
    public string item_type { get; set;}  
    [JsonProperty(PropertyName = "image")]
    public string image { get; set;}  
    [JsonProperty(PropertyName = "name")]
    public string name { get; set;} 
    [JsonProperty(PropertyName = "descr")]
    public string descr { get; set;}  
    [JsonProperty(PropertyName = "num_owned")]
    public int num_owned { get; set;} 
    [JsonProperty(PropertyName = "purchased_today")]
    public int purchased_today { get; set;} 
    [JsonProperty(PropertyName = "purchased_yday")]
    public int purchased_yday { get; set;} 
}

[System.Serializable]
public class ReportInfoModel  {
//    [JsonProperty(PropertyName = "report_types")]
//    public ReportInfoDetailModel[] report_types;
//
    [JsonProperty(PropertyName = "reports_per_page")]
    public int reports_per_page { get; set;}  
}

[System.Serializable]
public class ReportInfoDetailModel  {
    public string value { get; set;}      
}

[System.Serializable]
public class ResourcesProductionModel  {
    [JsonProperty(PropertyName = "r_w")]
    public int resource_wood { get; set;} 

    [JsonProperty(PropertyName = "r_c")]
    public int resource_clay { get; set;} 

    [JsonProperty(PropertyName = "r_i")]
    public int resource_iron { get; set;} 
}

[System.Serializable]
public class BuildingLevelsModel {

    [JsonProperty(PropertyName = "b_a")]
    public int academy { get; set;}
    [JsonProperty(PropertyName = "b_b")]
    public int barracks { get; set;}  
    [JsonProperty(PropertyName = "b_c")]
    public int quarry { get; set;}  
    [JsonProperty(PropertyName = "b_f")]
    public int farm { get; set;} 
    [JsonProperty(PropertyName = "b_h")]
    public int hidingPlace { get; set;}  
    [JsonProperty(PropertyName = "b_hq")]
    public int cityHall { get; set;} 
    [JsonProperty(PropertyName = "b_i")]
    public int ironMine { get; set;} 
    [JsonProperty(PropertyName = "b_m")]
    public int market { get; set;}  
    [JsonProperty(PropertyName = "b_r")]
    public int ralllyPoint { get; set;} 
    
    [JsonProperty(PropertyName = "b_s")]
    public int forge { get; set;} 
    [JsonProperty(PropertyName = "b_st")]
    public int stable { get; set;}  
    [JsonProperty(PropertyName = "b_w")]
    public int lumbermill { get; set;} 

    [JsonProperty(PropertyName = "b_wh")]
    public int warehousr { get; set;} 
    [JsonProperty(PropertyName = "b_wa")]
    public int wall { get; set;}  
    [JsonProperty(PropertyName = "b_wo")]
    public int workshp { get; set;} 


}



