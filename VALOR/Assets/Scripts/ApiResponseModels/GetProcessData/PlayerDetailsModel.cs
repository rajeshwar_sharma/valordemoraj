using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class PlayerDetailsModel  {
    [JsonProperty(PropertyName = "target_char")]
	public PlayerModel player;
}


public class PlayerModel {
	[JsonProperty(PropertyName = "login")]
	public string login { get; set;}  

	[JsonProperty(PropertyName = "user_id")]
	public int user_id { get; set;} 

	[JsonProperty(PropertyName = "total_points")]
	public int total_points { get; set;} 

	[JsonProperty(PropertyName = "bp")]
	public int bp { get; set;} 

	[JsonProperty(PropertyName = "bp_ts")]
	public string bp_ts { get; set;} 

	[JsonProperty(PropertyName = "gid")]
	public int gid { get; set;} 

	[JsonProperty(PropertyName = "truce_enabled")]
	public int truce_enabled { get; set;} 

	[JsonProperty(PropertyName = "truce_enabled_ts")]
	public string truce_enabled_ts { get; set;} 

	[JsonProperty(PropertyName = "scores_and_ranks")]
	public ScoresRanksModel[] scores_and_ranks;

	[JsonProperty(PropertyName = "villages")]	
	public List<VillagesModel> villages;

}


public class ScoresRanksModel {
	[JsonProperty(PropertyName = "Rank")]
	public string Rank { get; set;} 

	[JsonProperty(PropertyName = "Total Points")]
	public string TotalPoints { get; set;} 

	[JsonProperty(PropertyName = "Total Kill Score")]
	public string TotalKillScore { get; set;} 

	[JsonProperty(PropertyName = "Attacker Kill Score")]
	public string AttackerKillScore { get; set;} 

	[JsonProperty(PropertyName = "Defender Kill Score")]
	public string DefenderKill { get; set;} 


}

public class VillagesModel {
	[JsonProperty(PropertyName = "id")]
	public int id { get; set;} 

	[JsonProperty(PropertyName = "coords")]
	public string coords { get; set;} 

	[JsonProperty(PropertyName = "continent")]
	public int continent { get; set;} 

	[JsonProperty(PropertyName = "chunk_coords")]
	public string chunk_coords { get; set;} 

	[JsonProperty(PropertyName = "city_name")]
	public string city_name { get; set;} 

	[JsonProperty(PropertyName = "points")]
	public int points { get; set;} 

	[JsonProperty(PropertyName = "loyalty")]
	public int loyalty { get; set;} 

	[JsonProperty(PropertyName = "influence")]
	public InfluenceModel influence;

	[JsonProperty(PropertyName = "influence_growth_rates")]
	public InfluenceGrowthModel influence_growth_rates;

	[JsonProperty(PropertyName = "civilization_level")]
	public int civilization_level { get; set;} 

	[JsonProperty(PropertyName = "is_savage")]
	public int is_savage { get; set;} 
}

public class InfluenceGrowthModel {
	[JsonProperty(PropertyName = "base_rate")]
	public int base_rate { get; set;} 

	[JsonProperty(PropertyName = "cityhall_modifier")]
	public int cityhall_modifier { get; set;} 

	[JsonProperty(PropertyName = "support_modifier")]
	public int support_modifier { get; set;} 
}




