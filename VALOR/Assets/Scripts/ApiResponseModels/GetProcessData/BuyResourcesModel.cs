using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class BuyResourcesModel  {

    [JsonProperty(PropertyName = "wood_1000")]
	public BuyResourcesDetailModel wood_1000;

	[JsonProperty(PropertyName = "clay_1000")]
	public BuyResourcesDetailModel clay_1000;

	[JsonProperty(PropertyName = "iron_1000")]
	public BuyResourcesDetailModel iron_1000;

	[JsonProperty(PropertyName = "all_1000")]
	public BuyResourcesDetailModel all_1000;

	[JsonProperty(PropertyName = "instant_npc_trade")]
	public BuyResourcesDetailModel instant_npc_trade;


}

public class BuyResourcesDetailModel  {

    [JsonProperty(PropertyName = "cost")]
    public int cost { get; set;}  
    
    [JsonProperty(PropertyName = "daily_limit")]
    public int daily_limit { get; set;}  

 	[JsonProperty(PropertyName = "item_type")]
    public string item_type { get; set;}  

 	[JsonProperty(PropertyName = "image")]
    public string image { get; set;}  

    [JsonProperty(PropertyName = "resource_key")]
    public string resource_key { get; set;}

    [JsonProperty(PropertyName = "resource_amt")]
    public int resource_amt { get; set;}

    [JsonProperty(PropertyName = "name")]
    public string name { get; set;}

 	[JsonProperty(PropertyName = "descr")]
    public string descr { get; set;}  

    [JsonProperty(PropertyName = "num_owned")]
    public string num_owned { get; set;}

 	[JsonProperty(PropertyName = "purchased_today")]
    public string purchased_today { get; set;}  

	
}
