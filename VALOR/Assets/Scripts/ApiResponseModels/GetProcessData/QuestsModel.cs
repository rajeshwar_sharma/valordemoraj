

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class QuestModel  {
    
    [JsonProperty(PropertyName = "id")]
    public string id { get; set;}  
    [JsonProperty(PropertyName = "level")]
    public string level { get; set;}  
    [JsonProperty(PropertyName = "done")]
    public string done { get; set;}  
    [JsonProperty(PropertyName = "name")]
    public string name { get; set;}  
    [JsonProperty(PropertyName = "type")]
    public string type { get; set;}

    [JsonProperty(PropertyName = "award")]
    public AwardsModel award;   

    [JsonProperty(PropertyName = "descr")]
    public string descr { get; set;}  
    [JsonProperty(PropertyName = "obj")]
    public string obj { get; set;}  
    [JsonProperty(PropertyName = "ref_key")]
    public string ref_key { get; set;} 
	
	[JsonProperty(PropertyName = "cid")]
    public string cid { get; set;}  
    [JsonProperty(PropertyName = "gid")]
    public string gid { get; set;} 


}

[System.Serializable]
public class AwardsModel{
	[JsonProperty(PropertyName = "amulet")]
    public string amulet { get; set;}  

}
