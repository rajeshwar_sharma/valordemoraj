using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class TroopsModel  {

    [JsonProperty(PropertyName = "b_b")]
	public TroopsValueModel[] barracks;

	[JsonProperty(PropertyName = "b_st")]
	public TroopsValueModel[] stable;

    [JsonProperty(PropertyName = "b_wo")]
	public TroopsValueModel[] workshp;


}

[System.Serializable]
public class TroopsValueModel  {

    [JsonProperty(PropertyName = "key")]
    public string key { get; set;}  
    
    [JsonProperty(PropertyName = "name")]
    public string name { get; set;}  

 	[JsonProperty(PropertyName = "r_w")]
    public string r_w { get; set;}  

 	[JsonProperty(PropertyName = "r_c")]
    public string r_c { get; set;}  

	[JsonProperty(PropertyName = "r_i")]
    public string r_i { get; set;}  
    
    [JsonProperty(PropertyName = "p_u")]
    public string p_u { get; set;}  

 	[JsonProperty(PropertyName = "time")]
    public string time { get; set;}  

 	[JsonProperty(PropertyName = "has_prereqs")]
    public string has_prereqs { get; set;}  

	[JsonProperty(PropertyName = "prereqs_string")]
    public string prereqs_string { get; set;}  

	[JsonProperty(PropertyName = "ref_key")]
    public string ref_key { get; set;}  



 }