using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class SpeedUpModel  {

    [JsonProperty(PropertyName = "15")]
	public SpeedUpModelDetailModel fifteen;

	[JsonProperty(PropertyName = "60")]
	public SpeedUpModelDetailModel sixty;

	[JsonProperty(PropertyName = "150")]
	public SpeedUpModelDetailModel oneFifty;

	[JsonProperty(PropertyName = "480")]
	public SpeedUpModelDetailModel fourEighty;


}


public class SpeedUpModelDetailModel  {

    [JsonProperty(PropertyName = "cost")]
    public int cost { get; set;}  
    
    [JsonProperty(PropertyName = "daily_limit")]
    public int daily_limit { get; set;}  

 	[JsonProperty(PropertyName = "item_type")]
    public string item_type { get; set;}  

    [JsonProperty(PropertyName = "speed_up_time")]
    public int speed_up_time { get; set;}

 	[JsonProperty(PropertyName = "image")]
    public string image { get; set;}  

    [JsonProperty(PropertyName = "name")]
    public string name { get; set;}

 	[JsonProperty(PropertyName = "descr")]
    public string descr { get; set;}  

    [JsonProperty(PropertyName = "num_owned")]
    public string num_owned { get; set;}

 	[JsonProperty(PropertyName = "purchased_today")]
    public string purchased_today { get; set;}  

	
}
