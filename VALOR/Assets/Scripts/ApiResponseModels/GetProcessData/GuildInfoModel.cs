using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class GuildInfoModel  {

    [JsonProperty(PropertyName = "invites")]
	public GuildInfoDetailModel[] invites;

	[JsonProperty(PropertyName = "requests")]
	public GuildInfoDetailModel[] requests; 

    [JsonProperty(PropertyName = "last_guilds")]
	public GuildInfoDetailModel[] last_guilds;

}

public class GuildInfoDetailModel  {

	[JsonProperty(PropertyName = "gid")]
    public int gid { get; set;}  

	[JsonProperty(PropertyName = "gname")]
    public string gname { get; set;}  

	
}
