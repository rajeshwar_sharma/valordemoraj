using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class TroopsInVillage  {

    [JsonProperty(PropertyName = "t_sf")]
    public string lancer { get; set;}  
 	[JsonProperty(PropertyName = "t_sw")]
    public string sentry { get; set;}  

	[JsonProperty(PropertyName = "t_a")]
    public string berserker { get; set;}  
 	[JsonProperty(PropertyName = "t_sc")]
    public string scout { get; set;} 
    [JsonProperty(PropertyName = "t_lc")]
    public string knight { get; set;}  
 	[JsonProperty(PropertyName = "t_hc")]
    public string guardian { get; set;} 

    [JsonProperty(PropertyName = "t_r")]
    public string ram { get; set;} 
    [JsonProperty(PropertyName = "t_c")]
    public string ballista { get; set;}  
 	[JsonProperty(PropertyName = "t_n")]
    public string scholars { get; set;} 


   }

   