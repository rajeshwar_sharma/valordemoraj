
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class ConfigLimitsModel  {

    [JsonProperty(PropertyName = "guild_input_limits")]
	public GuildInputLimitsModel guild_input_limits;

	[JsonProperty(PropertyName = "mail_input_limits")]
	public MailInputLimitsModel requesmail_input_limitsts; 

    [JsonProperty(PropertyName = "guild_max_send_requests_allowed")]
	public int guild_max_send_requests_allowed { get; set;}  

	[JsonProperty(PropertyName = "guild_max_receive_requests_allowed")]
	public int guild_max_receive_requests_allowed { get; set;}  

	[JsonProperty(PropertyName = "guild_max_send_invites_allowed")]
	public int guild_max_send_invites_allowed { get; set;}  

	[JsonProperty(PropertyName = "guild_max_receive_invites_allowed")]
	public int guild_max_receive_invites_allowed { get; set;}  


}

public class GuildInputLimitsModel  {

	[JsonProperty(PropertyName = "name_length_min")]
    public int name_length_min { get; set;}  

	[JsonProperty(PropertyName = "name_length_max")]
    public int name_length_max { get; set;}  

	[JsonProperty(PropertyName = "description_length_min")]
    public int description_length_min { get; set;}  

	[JsonProperty(PropertyName = "tag_length_min")]
    public int tag_length_min { get; set;}  

	[JsonProperty(PropertyName = "tag_length_max")]
    public int tag_length_max { get; set;}  

	[JsonProperty(PropertyName = "news_length_min")]
    public int news_length_min { get; set;}  

	[JsonProperty(PropertyName = "news_length_max")]
    public int news_length_max { get; set;}  

	
}

public class MailInputLimitsModel  {

	[JsonProperty(PropertyName = "subject_length_min")]
    public int subject_length_min { get; set;}  

	[JsonProperty(PropertyName = "subject_length_max")]
    public int subject_length_max { get; set;}  

	[JsonProperty(PropertyName = "body_length_min")]
    public int body_length_min { get; set;}  

	[JsonProperty(PropertyName = "body_length_max")]
    public int body_length_max { get; set;}  


	
}



