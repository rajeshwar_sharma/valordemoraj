
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class VillageModel  {

 	[JsonProperty(PropertyName = "id")]
	public int id { get; set;}  

	[JsonProperty(PropertyName = "coords")]
	public string coords { get; set;}  

	[JsonProperty(PropertyName = "continent")]
	public int continent { get; set;}  

	[JsonProperty(PropertyName = "chunk_coords")]
	public string chunk_coords { get; set;}  

	[JsonProperty(PropertyName = "city_name")]
	public string city_name { get; set;}  

	[JsonProperty(PropertyName = "points")]
	public int points { get; set;}  

	[JsonProperty(PropertyName = "loyalty")]
	public int loyalty { get; set;}  

    [JsonProperty(PropertyName = "influence")]
	public InfluenceModel influence;

	[JsonProperty(PropertyName = "influence_growth_rates")]
	public InfluenceGrowthRatesModel influence_growth_rates; 

    [JsonProperty(PropertyName = "civilization_level")]
	public int civilization_level { get; set;}  

	[JsonProperty(PropertyName = "is_savage")]
	public int is_savage { get; set;}  


}

public class InfluenceModel  {
	
}

public class InfluenceGrowthRatesModel  {

	[JsonProperty(PropertyName = "base_rate")]
    public int base_rate { get; set;}  

	[JsonProperty(PropertyName = "cityhall_modifier")]
    public int cityhall_modifier { get; set;}  

	[JsonProperty(PropertyName = "support_modifier")]
    public int support_modifier { get; set;}  



	
}



