

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class BuildingDetailModel  {

    [JsonProperty(PropertyName = "b_hq")]
	public BuildingDetailValueModel cityHall;

    [JsonProperty(PropertyName = "b_b")]
	public BuildingDetailValueModel barracks;

	[JsonProperty(PropertyName = "b_st")]
	public BuildingDetailValueModel stable;

	[JsonProperty(PropertyName = "b_wo")]
	public BuildingDetailValueModel workshp;

	[JsonProperty(PropertyName = "b_a")]
	public BuildingDetailValueModel academy;

	[JsonProperty(PropertyName = "b_s")]
	public BuildingDetailValueModel forge;

	[JsonProperty(PropertyName = "b_r")]
	public BuildingDetailValueModel ralllyPoint;

	[JsonProperty(PropertyName = "b_m")]
	public BuildingDetailValueModel market;

	[JsonProperty(PropertyName = "b_w")]
	public BuildingDetailValueModel lumbermill;

	[JsonProperty(PropertyName = "b_c")]
	public BuildingDetailValueModel quarry;

	[JsonProperty(PropertyName = "b_i")]
	public BuildingDetailValueModel  ironMine;

	[JsonProperty(PropertyName = "b_f")]
	public BuildingDetailValueModel farm;

	[JsonProperty(PropertyName = "b_wh")]
	public BuildingDetailValueModel warehousr;

	[JsonProperty(PropertyName = "b_h")]
	public BuildingDetailValueModel hidingPlace;

	[JsonProperty(PropertyName = "b_wa")]
	public BuildingDetailValueModel wall;

}

[System.Serializable]
public class BuildingDetailValueModel  {

    [JsonProperty(PropertyName = "building_details")]
    public string building_details { get; set;}  
    
    [JsonProperty(PropertyName = "level")]
    public int level { get; set;}  

 	[JsonProperty(PropertyName = "max_level")]
    public int max_level { get; set;}  

 	[JsonProperty(PropertyName = "tables")]
	public BuildingDetailTableKeyModel[] tables; 
	
}
	

[System.Serializable]
public class BuildingDetailTableKeyModel  {

	[JsonProperty(PropertyName = "header")]
    public string header { get; set;}  
    
	[JsonProperty(PropertyName = "table")]
	public Dictionary<string, string>[] table { get; set;}  


}

//[System.Serializable]
//public class BuildingStats {
//	
//}


