﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class BuildingModel  {

	[JsonProperty(PropertyName = "key")]
	public string key { get; set;}  
	[JsonProperty(PropertyName = "name")]
	public string name { get; set;}  
	[JsonProperty(PropertyName = "level")]
	public string level { get; set;}  
	[JsonProperty(PropertyName = "next_level")]
	public string next_level { get; set;}  
	[JsonProperty(PropertyName = "max")]
	public string max { get; set;}  
	[JsonProperty(PropertyName = "time")]
	public string time { get; set;}  
	[JsonProperty(PropertyName = "r_w")]
	public string resourceWood { get; set;}  
	[JsonProperty(PropertyName = "r_c")]
	public string resourceClay { get; set;}  
	[JsonProperty(PropertyName = "r_i")]
	public string resourceIron { get; set;}  
	[JsonProperty(PropertyName = "p_u")]
	public string population { get; set;}  
	[JsonProperty(PropertyName = "min_level")]
	public string minimumLevel { get; set;}
	[JsonProperty(PropertyName = "prereqs_string_demolish")]
	public string preRequestDemolish { get; set;}
	[JsonProperty(PropertyName = "next_level_demolish")]
	public string nextLevelDemolish { get; set;}
	[JsonProperty(PropertyName = "time_demolish")]
	public string timeDemolish { get; set;}
	[JsonProperty(PropertyName = "prereqs_string")]
	public string preRequestString { get; set;}
	[JsonProperty(PropertyName = "has_prereqs")]
	public int has_prereqs { get; set;}

}
