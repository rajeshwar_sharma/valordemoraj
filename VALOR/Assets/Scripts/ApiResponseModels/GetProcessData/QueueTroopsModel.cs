

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class QueueTroopsModel  {

    [JsonProperty(PropertyName = "b_b")]
	public QueueTroopsValueModel[] barracks;

	[JsonProperty(PropertyName = "b_st")]
	public QueueTroopsValueModel[] stable;

    [JsonProperty(PropertyName = "b_wo")]
	public QueueTroopsValueModel[] workshp;

	[JsonProperty(PropertyName = "b_a")]
	public QueueTroopsValueModel[] academy;
}



public class QueueTroopsValueModel  {

    [JsonProperty(PropertyName = "id")]
    public int id { get; set;}  
    
    [JsonProperty(PropertyName = "start_time")]
    public string start_time { get; set;}  

 	[JsonProperty(PropertyName = "end_time")]
    public string end_time { get; set;}  

 	[JsonProperty(PropertyName = "duration")]
    public string duration { get; set;}  


	[JsonProperty(PropertyName = "total_duration")]
    public string total_duration { get; set;}  
    
    [JsonProperty(PropertyName = "origin_coords")]
    public string origin_coords { get; set;}  

 	[JsonProperty(PropertyName = "target_coords")]
    public string target_coords { get; set;}  

 	[JsonProperty(PropertyName = "origin_name")]
    public string origin_name { get; set;}  


	[JsonProperty(PropertyName = "target_name")]
    public string target_name { get; set;}  
    
    [JsonProperty(PropertyName = "assignments_key")]
    public string assignments_key { get; set;}  

 	[JsonProperty(PropertyName = "primary_type")]
    public string primary_type { get; set;}  

 	[JsonProperty(PropertyName = "units")]
    public string units { get; set;}  


    [JsonProperty(PropertyName = "last_update_time")]
    public string last_update_time { get; set;}  

 	[JsonProperty(PropertyName = "unit_duration")]
    public string unit_duration { get; set;}  

 	[JsonProperty(PropertyName = "num_units")]
    public string num_units { get; set;}  

	
}