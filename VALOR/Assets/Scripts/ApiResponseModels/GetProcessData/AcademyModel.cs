using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class AcademyModel  {

    [JsonProperty(PropertyName = "scholars_in_village")]
	public int scholars_in_village { get; set;}  

	[JsonProperty(PropertyName = "scholars_total")]
	public int scholars_total { get; set;}  

	[JsonProperty(PropertyName = "num_conquered_villages")]
	public int num_conquered_villages { get; set;}  

	[JsonProperty(PropertyName = "scholarships")]
	public int scholarships { get; set;}  

	[JsonProperty(PropertyName = "scholar_limit")]
	public int scholar_limit { get; set;}  

	[JsonProperty(PropertyName = "next_scholar_limit")]
	public int next_scholar_limit { get; set;}  

	[JsonProperty(PropertyName = "num_missing_scholarships_for_next_scholar_limit")]
	public int num_missing_scholarships_for_next_scholar_limit { get; set;}  

	[JsonProperty(PropertyName = "num_saved_for_next_scholar_limit")]
	public int num_saved_for_next_scholar_limit { get; set;}  

	[JsonProperty(PropertyName = "scholar_cost")]
	public ScholarCostModel scholar_cost;  

	[JsonProperty(PropertyName = "scholarship_cost")]
	public ScholarCostModel scholarship_cost;  

	[JsonProperty(PropertyName = "scholars_in_education")]
	public int scholars_in_education { get; set;}  

	[JsonProperty(PropertyName = "num_scholars_can_still_educate")]
	public int num_scholars_can_still_educate { get; set;}  

    [JsonProperty(PropertyName = "details")]
	public AcademyDetailsKeyModel[] details;



}


[System.Serializable]
public class ScholarCostModel {

	[JsonProperty(PropertyName = "r_w")]
	public int r_w { get; set;} 

	[JsonProperty(PropertyName = "r_c")]
	public int r_c { get; set;} 

	[JsonProperty(PropertyName = "r_i")]
	public int r_i { get; set;} 

	[JsonProperty(PropertyName = "p_u")]
	public int p_u { get; set;} 

	[JsonProperty(PropertyName = "time")]
	public string time { get; set;} 

	[JsonProperty(PropertyName = "parent")]
	public string parent { get; set;} 

}


[System.Serializable]
public class AcademyDetailsKeyModel {

	[JsonProperty(PropertyName = "header")]
	public string header { get; set;} 

	[JsonProperty(PropertyName = "content")]
	public AcademyContentKeyModel[] content { get; set;} 

}

[System.Serializable]
public class AcademyContentKeyModel {

	[JsonProperty(PropertyName = "key")]
	public string key { get; set;} 

	[JsonProperty(PropertyName = "value")]
	public int value { get; set;} 

	[JsonProperty(PropertyName = "highlight")]
	public int highlight { get; set;} 


}




