using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

[System.Serializable]
public class ResearchModel  {

     [JsonProperty(PropertyName = "key")]
	public string key { get; set;}  

	[JsonProperty(PropertyName = "image_key")]
	public string image_key { get; set;}  

	[JsonProperty(PropertyName = "name")]
	public string name { get; set;}  

	[JsonProperty(PropertyName = "level")]
	public int level { get; set;}  

	[JsonProperty(PropertyName = "next_level")]
	public int next_level { get; set;}  

	[JsonProperty(PropertyName = "prereqs_string")]
	public string prereqs_string { get; set;}  

	[JsonProperty(PropertyName = "has_prereqs")]
	public int has_prereqs { get; set;}  

	[JsonProperty(PropertyName = "ref_key")]
	public string ref_key { get; set;}  

	[JsonProperty(PropertyName = "r_w")]
	public int r_w { get; set;}  

	[JsonProperty(PropertyName = "r_c")]
	public int r_c { get; set;}  

	[JsonProperty(PropertyName = "r_i")]
	public int r_i { get; set;}  

	[JsonProperty(PropertyName = "p_u")]
	public int p_u { get; set;}  

	[JsonProperty(PropertyName = "time")]
	public string time { get; set;}  

	[JsonProperty(PropertyName = "gold")]
	public int gold { get; set;}  

	[JsonProperty(PropertyName = "prereqs_string_revoke")]
	public string prereqs_string_revoke { get; set;} 


}