using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class WheelModel  {

    [JsonProperty(PropertyName = "0")]
	public WheelDetailModel zero;

    [JsonProperty(PropertyName = "1")]
	public WheelDetailModel one;

    [JsonProperty(PropertyName = "3")]
	public WheelDetailModel three;

    [JsonProperty(PropertyName = "4")]
	public WheelDetailModel four;

    [JsonProperty(PropertyName = "5")]
	public WheelDetailModel five;

    [JsonProperty(PropertyName = "6")]
	public WheelDetailModel six;

    [JsonProperty(PropertyName = "7")]
	public WheelDetailModel seven;

    [JsonProperty(PropertyName = "8")]
	public WheelDetailModel eight;

    [JsonProperty(PropertyName = "9")]
	public WheelDetailModel nine;

    [JsonProperty(PropertyName = "10")]
	public WheelDetailModel ten;

    [JsonProperty(PropertyName = "11")]
	public WheelDetailModel eleven;

	[JsonProperty(PropertyName = "12")]
	public WheelDetailModel twelve;

	[JsonProperty(PropertyName = "13")]
	public WheelDetailModel thirteen;

	[JsonProperty(PropertyName = "14")]
	public WheelDetailModel fourteen;

	[JsonProperty(PropertyName = "15")]
	public WheelDetailModel fifteen;

	[JsonProperty(PropertyName = "16")]
	public WheelDetailModel sixten;

}


public class WheelDetailModel  {

    [JsonProperty(PropertyName = "type")]
    public string type { get; set;}  
    
    [JsonProperty(PropertyName = "amount")]
    public int amount { get; set;}  

 	[JsonProperty(PropertyName = "image")]
    public string image { get; set;}  

    [JsonProperty(PropertyName = "descr")]
    public string descr { get; set;}

 	[JsonProperty(PropertyName = "popup_title")]
    public string popup_title { get; set;}  

    [JsonProperty(PropertyName = "popup_descr")]
    public string popup_descr { get; set;}
	
}
