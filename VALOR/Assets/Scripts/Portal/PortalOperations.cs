﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalOperations : MonoBehaviour {

	public GameObject worldTypeScreen;
	public GameObject settingsView;
	public Button worldBtn;
	public Button settingsBtn;
	public MyTitlesScreen myTitleScreen;
	public Button ticketBtn;
	public GameObject getMoreTicketScreen;
	private Button myTitleBtn;
	private WorldType worldTypeScript;

	// Use this for initialization
	void Start () {
		this.setupPortal ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupPortal() {
		this.getObjects ();
		this.addListerners ();
		this.hideAllObjects ();
	}

	void getObjects() {
		this.settingsBtn = GameObject.Find (Objects.Portal.portalSettingsBtn).GetComponent<Button> ();
		this.myTitleBtn = GameObject.Find (Objects.Portal.portalTitleBtn).GetComponent<Button> ();
		this.worldTypeScript = worldTypeScreen.GetComponent<WorldType> ();

	}

	void addListerners() {
		this.worldBtn.onClick.AddListener (worldBtnPressed);
		this.settingsBtn.onClick.AddListener (settingsBtnPressed);
		this.myTitleBtn.onClick.AddListener (showTitlesScreen);
		this.ticketBtn.onClick.AddListener (showTicketsPopUp);
	}

	public void worldBtnPressed() {
		if (worldTypeScreen.activeInHierarchy) {
			Utility.hideGameObject (worldTypeScreen);
		} else {
			Utility.showGameObject (worldTypeScreen);
		}  

	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (settingsView);
		}
	}

	void hideAllObjects() {
		worldTypeScript.hideMyWorld() ;
		worldTypeScript.hideStandardWorld ();
		Utility.hideGameObject (worldTypeScreen);
	}

	void showTitlesScreen() {
		this.myTitleScreen.showMyTitleScreen ();
	}

	void showTicketsPopUp() {
		this.getMoreTicketScreen.GetComponent<GetMoreTicketsScreen> ().showGetMoreTicketScreen ();
	}
}
