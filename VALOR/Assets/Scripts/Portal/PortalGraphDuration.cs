﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalGraphDuration : MonoBehaviour {

	public Button leftBtn;
	public Button rightBtn;
	public GameObject durationLbl;
	public GameObject portalGraphLine;

	private int portalDurationPresentState; 

	// Use this for initialization
	void Start () {
		this.setupPortalDuration ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void setupPortalDuration() {
		this.portalDurationPresentState = 0;
		this.addListener ();
	}

	void addListener() {
		this.leftBtn.onClick.AddListener (onLeftClick);
		this.rightBtn.onClick.AddListener (onRightClick);
	}

	void onLeftClick() {
		print (this.portalDurationPresentState);
		if(this.portalDurationPresentState > 0) {
			this.portalDurationPresentState = this.portalDurationPresentState - 1;
			this.setDurationLabel ();
		}
	}

	void onRightClick() {
		if(this.portalDurationPresentState < 3) {
			this.portalDurationPresentState = this.portalDurationPresentState + 1;
			this.setDurationLabel ();
		}
	}

	void setDurationLabel() {
		if (this.portalDurationPresentState == 0) {
			this.durationLbl.GetComponent<Text>().text = "Duration: 3 Days";
		} else if (this.portalDurationPresentState == 1) {
			this.durationLbl.GetComponent<Text>().text = "Duration: 1 Week";
		} else if (this.portalDurationPresentState == 2){
			this.durationLbl.GetComponent<Text>().text = "Duration: 2 Weeks";
		} else {
			this.durationLbl.GetComponent<Text>().text = "Duration: 4 Weeks";
		}
	}

	void plotGraphForRenderer() {
		
	}

}
