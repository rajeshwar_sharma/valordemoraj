﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum pointsButtonState {
	
}


public class PortalPointsSelectionSection : MonoBehaviour {

	public Button pointsBtn;
	public Button KSTBtn;
	public Button KSABtn;
	public Button KSDBtn;
	public Button RankBtn;

	private GameObject pointsView;
	private GameObject KSTView;
	private GameObject KSAView;
	private GameObject KSDView;
	private GameObject RankView;

	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	/// <summary>
	/// Setups the view.
	/// </summary>

	void setupView() {
		this.getObjects ();
		this.addListeners ();
		this.pointsBtnPressed ();
	}

	void initailTheme() {
		
	}

	/// <summary>
	/// Gets the objects.
	/// </summary>

	private void getObjects() {
		this.pointsView = GameObject.Find (Objects.Portal.pointsView);
		this.KSAView = GameObject.Find (Objects.Portal.KSAView);
		this.KSDView = GameObject.Find (Objects.Portal.KSDView);
		this.KSTView = GameObject.Find (Objects.Portal.KSTView);
		this.RankView = GameObject.Find (Objects.Portal.RankView);
	}

	/// <summary>
	/// Adds the listeners.
	/// </summary>

	private void addListeners() {
		this.pointsBtn.onClick.AddListener (pointsBtnPressed);
		this.KSABtn.onClick.AddListener (KSABtnPressed);
		this.KSDBtn.onClick.AddListener (KSDBtnPressed);
		this.KSTBtn.onClick.AddListener (KSTBtnPressed);
		this.RankBtn.onClick.AddListener (RankBtnPressed);
	}

	/// <summary>
	/// Pointses the button pressed.
	/// </summary>
		
	private void pointsBtnPressed() {
		this.deselectAllButtons ();
		this.highlightButton (this.pointsView);

	}

	/// <summary>
	/// KSAs the button pressed.
	/// </summary>

	private void KSABtnPressed() {
		this.deselectAllButtons ();
		this.highlightButton (this.KSAView);
	}

	/// <summary>
	/// KSDs the button pressed.
	/// </summary>

	private void KSDBtnPressed() {
		this.deselectAllButtons ();
		this.highlightButton (this.KSDView);
	}

	private void KSTBtnPressed() {
		this.deselectAllButtons ();
		this.highlightButton (this.KSTView);
	}

	private void RankBtnPressed() {
		this.deselectAllButtons ();
		this.highlightButton (this.RankView);

	}

	private void deselectAllButtons() {
		this.normalButton (this.pointsView);
		this.normalButton (this.KSAView);
		this.normalButton (this.KSTView);
		this.normalButton (this.KSDView);
		this.normalButton (this.RankView);
	}

	private void highlightButton(GameObject view) {
		view.GetComponent<Image> ().color = Colors.portalBtnHighlight;
		view.GetComponentInChildren<Text> ().color = Colors.portalTxtHighlight;
	}

	private void normalButton(GameObject view) {
		Image img = view.GetComponent<Image> ();
		img.color = Colors.portalBtn;
		view.GetComponentInChildren<Text> ().color = Colors.portalTxt;
	}
}
