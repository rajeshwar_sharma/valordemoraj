﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingOperations : MonoBehaviour {

	public  GameObject globalView;
	public  GameObject playerView;
	public GameObject pointsView;
	private Button globalButton;
	private Button playerButton;
	public Button pointsButton;


	// Use this for initialization
	void Start () {
		this.setupRankingMenus ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupRankingMenus() {
		this.getObjects ();
		this.addListerners ();
		this.hideMenus ();
	}

	void getObjects() {
		this.globalButton = GameObject.Find (Objects.Ranking.globalBtn).GetComponent<Button> ();
		this.playerButton = GameObject.Find (Objects.Ranking.playerBtn).GetComponent<Button> ();
		//this.pointsButton = GameObject.Find (Objects.Ranking.pointsBtn).GetComponent<Button> ();
	}

	void addListerners() {
		this.globalButton.onClick.AddListener (globalButtonPressed);
		this.playerButton.onClick.AddListener (playerButtonPressed);
		this.pointsButton.onClick.AddListener (pointsButtonPressed);
	}

	void globalButtonPressed() {
		if (this.globalView.activeInHierarchy) {
			this.globalView.SetActive (false);
		} else {
			this.globalView.SetActive (true);
			this.playerView.SetActive (false);
			this.pointsView.SetActive (false);
		}
	}

	void playerButtonPressed() {
		if (this.playerView.activeInHierarchy) {
			this.playerView.SetActive (false);
		} else {
			this.playerView.SetActive (true);
			this.globalView.SetActive (false);
			this.pointsView.SetActive (false);
		}
	}

	void pointsButtonPressed() {
		if (this.pointsView.activeInHierarchy) {
			this.pointsView.SetActive (false);
		} else {
			this.pointsView.SetActive(true);
			this.globalView.SetActive(false);
			this.playerView.SetActive(false);
		}
	}

	void hideMenus() {
		this.playerView.SetActive (false);
		this.globalView.SetActive (false);
		this.pointsView.SetActive (false);
	}
}
