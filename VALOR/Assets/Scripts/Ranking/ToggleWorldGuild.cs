﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum worldGuildState {
	World=1,
	Guild
};

public class ToggleWorldGuild : MonoBehaviour {

	private worldGuildState state;
	private Text worldText;
	private Text guildText;
	private Button worldBtn;
	private Button guildBtn;
	public GameObject worldContent;
	public GameObject guildContent;


	// Use this for initialization
	void Start () {
		this.setupRanking ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Setups the report and mail.
	/// </summary>

	void setupRanking() {
		this.getButtons ();
		this.addListeners ();
		this.setIntialState ();
	}

	void getButtons() {
		this.worldBtn = GameObject.Find (Objects.Ranking.worldRankBtn).GetComponent<Button> ();
		this.guildBtn = GameObject.Find (Objects.Ranking.guildRankBtn).GetComponent<Button> ();
	}

	/// <summary>
	/// Adds the listeners.
	/// </summary>

	void addListeners() {
		worldBtn.onClick.AddListener (changeStateToWorld);
		guildBtn.onClick.AddListener (changeStateToGuild);
	}

	/// <summary>
	/// Sets the state initially.
	/// </summary>

	void setIntialState() {
		state = worldGuildState.World;
		this.guildContent.SetActive (false);
	}

	/// <summary>
	/// Changes the state to world.
	/// </summary>

	void changeStateToWorld() {
		state = worldGuildState.World;
		Utility.setImageToGameObject (Objects.Ranking.worldRankBtn,Sprites.ReportsAndMails.mailTextBg);
		Utility.setImageToGameObject (Objects.Ranking.guildRankBtn, Sprites.ReportsAndMails.mailReportTransparentBg);
		Utility.setColorToText (Objects.Ranking.worldRankTxt, Color.white);
		Utility.setColorToText (Objects.Ranking.guildRankTxt, Color.black);
		this.hideShowGuildWorldViews (this.worldContent, this.guildContent);
	}

	/// <summary>
	/// Changes the state to guild.
	/// </summary>

	void changeStateToGuild() {
		state = worldGuildState.Guild;
		Utility.setImageToGameObject(Objects.Ranking.worldRankBtn,Sprites.ReportsAndMails.mailReportTransparentBg);
		Utility.setImageToGameObject (Objects.Ranking.guildRankBtn, Sprites.ReportsAndMails.reportTextBg);
		Utility.setColorToText (Objects.Ranking.guildRankTxt, Color.white);
		Utility.setColorToText (Objects.Ranking.worldRankTxt, Color.black);
		this.hideShowGuildWorldViews (this.guildContent, this.worldContent);
	}

	void hideShowGuildWorldViews(GameObject showObject,GameObject hideObject) {
		if (hideObject.activeInHierarchy) {
			hideObject.SetActive (false);
			showObject.SetActive (true);
		}
	}

}
