﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IronPopUp : MonoBehaviour {

	public Button closeBtn;
	public Button okBtn;
	public Text title;
	public Image centerImageObj;
	public Text detailText;

	private Image okBtnImage;
	private Text okBtnText;
	private RectTransform okBtnRectTransform;

	const string okImage = "StoreItem/support_btn@2x";
	const string activateImage = "StoreItem/accept_brown_btn@2x";
	const string amuletImage = "WheelOfFurtune/wheelAmulet@2x";
	const string clayImage = "WheelOfFurtune/wheelClay@2x";
	const string happyPrompt = "WheelOfFurtune/happy_prompt_img@2x";
	const string ironImage = "WheelOfFurtune/wheelIron@2x";
	const string speedImage = "WheelOfFurtune/speed_1@2x";
	const string woodImage = "WheelOfFurtune/wheelWood@2x";


	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}
	
	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.hideSelf ();
	}

	private void addObject() {
		okBtnText = okBtn.GetComponentInChildren<Text> ();
		okBtnImage = okBtn.GetComponentInParent<Image> ();
		okBtnRectTransform = okBtn.GetComponentInParent<RectTransform> ();

	}

	private void addListeners() {
		this.closeBtn.onClick.AddListener (closeBtnPressed);
		this.okBtn.onClick.AddListener (closeBtnPressed);
	}

	private void closeBtnPressed () {
		okBtnText.text = "OK";
		okBtnRectTransform.sizeDelta = new Vector2(228, 90);
		Utility.setImageToImageObject (okBtnImage, okImage);
		Utility.hideGameObject (this.gameObject);
	}

	public void hideSelf() {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelfWith(string headerTitle, string description ) {
		this.title.text = headerTitle;
		this.detailText.text = description;

		if (headerTitle.Contains ("IRON")) {
			Utility.setImageToImageObject (centerImageObj, ironImage);
		} else if (headerTitle.Contains ("WOOD")) {
			Utility.setImageToImageObject (centerImageObj, woodImage);
		} else if (headerTitle.Contains ("CLAY")) {
			Utility.setImageToImageObject (centerImageObj, clayImage);
		} else if (headerTitle.Contains ("AMULET")) {
					Utility.setImageToImageObject (centerImageObj, amuletImage);
		} else if (headerTitle.Contains ("SPEED")) {
			Utility.setImageToImageObject (centerImageObj, speedImage);
		} else if (headerTitle.Contains ("PRESTIGE")) {
			Utility.setImageToImageObject (centerImageObj, happyPrompt);
		} else if (headerTitle.Contains ("PREMIUM")) {
			okBtnText.text = "ACTIVATE";
			okBtnRectTransform.sizeDelta = new Vector2(340, 180);
			Utility.setImageToImageObject (okBtnImage, activateImage);
			Utility.setImageToImageObject (centerImageObj, happyPrompt);
		}


		Utility.showGameObjectAsLastSibling (this.gameObject);
	}




}
