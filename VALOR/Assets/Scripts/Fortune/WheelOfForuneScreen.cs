﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WheelOfForuneScreen : MonoBehaviour {

	public GameObject settingsView;
	public GameObject amuletPopUpView;
	public IronPopUp ironPopUpScript;
	public Button backButton;
	public Button settingBtn;
	public Button amuletsBtn;

	public Button button1;
	public Button button2;
	public Button button3;
	public Button button4;
	public Button button5;
	public Button button6;
	public Button button7;
	public Button button8;
	public Button button9;
	public Button button10;
	public Button button11;
	public Button button12;
	public Button button13;
	public Button button14;
	public Button button15;
	public Button button16;

	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addListeners ();
		this.hideSelf ();
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
		this.amuletsBtn.onClick.AddListener (amuletBtnPressed);
		this.button1.onClick.AddListener (() => popUpButtonPressed("iron", 100));
		this.button2.onClick.AddListener (() => popUpButtonPressed("iron", 100));
		this.button3.onClick.AddListener (() => popUpButtonPressed("wood", 500));
		this.button4.onClick.AddListener (() => popUpButtonPressed("clay", 100));
		this.button5.onClick.AddListener (() => popUpButtonPressed("iron", 2000));
		this.button6.onClick.AddListener (() => popUpButtonPressed("clay", 500));
		this.button7.onClick.AddListener (() => popUpButtonPressed("iron", 500));
		this.button8.onClick.AddListener (() => popUpButtonPressed("amulet", 2));
		this.button9.onClick.AddListener (() => popUpButtonPressed("clay", 500));
		this.button10.onClick.AddListener (() => popUpButtonPressed("speed", 1));
		this.button11.onClick.AddListener (() => popUpButtonPressed("wood", 100));
		this.button12.onClick.AddListener (() => popUpButtonPressed("wood", 2000));
		this.button13.onClick.AddListener (() => popUpButtonPressed("wood", 100));
		this.button14.onClick.AddListener (() => popUpButtonPressed("speed", 1));
		this.button15.onClick.AddListener (() => popUpButtonPressed("clay", 100));
		this.button16.onClick.AddListener (() => popUpButtonPressed("speed", 1));
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void amuletBtnPressed() {
		Utility.showGameObject (this.amuletPopUpView);
	}

	public void popUpButtonPressed(string btnType, int value) {

		switch (btnType) {

		case "iron":
			this.ironPopUpScript.showSelfWith (value + " IRON", "Win "+value+" iron to spend");
			break;
		case "wood": 
			this.ironPopUpScript.showSelfWith (value + " WOOD", "Win "+value+" wood to spend");
			break;
		case "clay": 
			this.ironPopUpScript.showSelfWith (value + " CLAY", "Win "+value+" clay to spend");
			break;
		case "amulet": 
			this.ironPopUpScript.showSelfWith (value + " AMULET", "Win "+value+" amulet to spend");
			break;
		case "speed": 
			this.ironPopUpScript.showSelfWith (value + " SPEED", "Win "+value+" SpeedUps to spend");
			break;
		
		default:
			print("Default case");
			break;

		}
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}

	


}
