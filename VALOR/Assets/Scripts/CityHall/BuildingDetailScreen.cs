﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingDetailScreen : MonoBehaviour {

	public GameObject backBtn;
	public GameObject settingBtn;
	public GameObject settingsScreen;
	public GameObject buildingDetailScreen;

	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.addListerners ();
		this.hideBuildingDetailScreen ();
	}

	void addListerners() {
		this.backBtn.GetComponent<Button> ().onClick.AddListener (hideBuildingDetailScreen);
		this.settingBtn.GetComponent<Button> ().onClick.AddListener (showSettingsScreen);
	}

	public void showBuildingDetailScreen() {
		Utility.showGameObject (this.buildingDetailScreen);
	}

	void hideBuildingDetailScreen() {
		Utility.hideGameObject (this.buildingDetailScreen);
	}

	void showSettingsScreen() {
		this.settingsScreen.GetComponent<SettingsScreenOperations> ().openPopUp ();
	}
}
