﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CityHallVerticalLayout : MonoBehaviour {

	public GameObject cityHallCellPrefab;
	public GameObject settingsBtn;
	public GameObject collapseExpandBtn;
	public GameObject collapseExpandText;
	public bool collapseState;

	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.collapseExpandBtn.GetComponent<Button> ().onClick.AddListener (toggleCollapseExpand);
	}

	void createTableView() {
		GameObject cHCell = Instantiate(cityHallCellPrefab) as GameObject;
		cHCell.transform.SetParent (gameObject.transform);
		cHCell.transform.localScale = new Vector3 (1, 1 , 1);
		cHCell.transform.localPosition = new Vector3 (0, 0, 0);

	}

	void toggleCollapseExpand() {
		if (collapseState == true) {
			Utility.setImageToGameObject (this.collapseExpandBtn, Sprites.CityHall.expandIcon);
			this.collapseExpandText.GetComponent<Text>().text = "Collapse all sections";
		} else {
			Utility.setImageToGameObject (this.collapseExpandBtn, Sprites.CityHall.collapseIcon);
			this.collapseExpandText.GetComponent<Text>().text = "Expand all sections";
		}
		collapseState = !collapseState;
	}
}
