﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CityHallCell : MonoBehaviour {


	public GameObject toggleBtn;
	public GameObject cellInfo;
	public GameObject detailBtn;
	public GameObject cityHallArrowBtn;
	public bool cellState;
	public float cellInfoHeight;
	public float cellInfoWidth;


	// Use this for initialization
	void Start () {
		this.setupCell ();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupCell() {
		this.setCellHeightWidth ();
		this.addListeners ();
		this.setDataForState ();
	}

	void addListeners() {
		this.toggleBtn.GetComponent<Button>().onClick.AddListener (toggleBtnPressed);
		this.detailBtn.GetComponent<Button>().onClick.AddListener (moveToDetailBtnPressed);
		this.cityHallArrowBtn.GetComponent<Button> ().onClick.AddListener (toggleBtnPressed);
	}

	void setCellHeightWidth() {
		this.cellInfoHeight = this.cellInfo.GetComponent<RectTransform> ().rect.height;
		this.cellInfoWidth = this.cellInfo.GetComponent<RectTransform> ().rect.width;
	}

	/// <summary>
	/// Sets the state of the data for.
	/// </summary>

	void setDataForState() {
		if (this.cellState == true) {
			this.shrinkCellAndPerformOtherActions ();
			this.changeArrowIcon ();
			this.cellState = false;
		} else {
			this.expandCellAndPerformOtherActions ();
			this.changeArrowIcon ();
			this.cellState = true;
		}
	}

	void toggleBtnPressed() {
		this.setDataForState ();
	}

	void expandCellAndPerformOtherActions() {
		Utility.showGameObject(this.cellInfo);
		//GameObject.Find("CityhallInfo").GetComponent<RectTransform>().rect.Set(0,0,this.cellInfoWidth,this.cellInfoHeight);
		this.showDetailBtn ();
	}

	void shrinkCellAndPerformOtherActions() {
		Utility.hideGameObject (this.cellInfo);
		//GameObject.Find("CityhallInfo").GetComponent<RectTransform>().rect.Set(0,0,this.cellInfoWidth,0);
		this.hideDetailBtn ();
	}

	void hideDetailBtn() {
		Utility.hideGameObject (this.detailBtn);
	}

	void showDetailBtn() {
		Utility.showGameObject (this.detailBtn);
	}

	void hideToggleBtn() {
		Utility.hideGameObject (this.toggleBtn);
	}

	void showToggleBtn() {
		Utility.showGameObject (this.toggleBtn);
	}

	void moveToDetailBtnPressed() {
		
	}

	void changeArrowIcon() {
		if (this.cellState == true) {
			Utility.setImageToGameObject (this.cityHallArrowBtn, Sprites.Quests.expandArrow);
		} else {
			Utility.setImageToGameObject (this.cityHallArrowBtn, Sprites.Quests.collapseArrow);
		}
	}

}
