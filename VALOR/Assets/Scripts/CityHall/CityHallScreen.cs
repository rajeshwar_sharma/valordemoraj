﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CityHallScreen : MonoBehaviour {

	public GameObject backBtn;
	public GameObject settingsBtn;
	public GameObject cityHallScreen;
	public GameObject settingsScreen;
	public Button buildingDetailScreenBtn;
	public GameObject buildingDetailScreen;


	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.addListeners ();
		this.hideCityHallScreen ();
	}
		
	void addListeners() {
		this.backBtn.GetComponent<Button> ().onClick.AddListener (backButtonPressed);
		this.settingsBtn.GetComponent<Button> ().onClick.AddListener (settingsButtonPressed);
		this.buildingDetailScreenBtn.onClick.AddListener (showBuildingDetailScreen);
	}

	void backButtonPressed() {
		this.hideCityHallScreen ();
	}

	public void hideCityHallScreen() {
		Utility.hideGameObject (this.cityHallScreen);	
	}

	public void showCityHallScreen() {
		Utility.showGameObject (this.cityHallScreen);
	}

	void settingsButtonPressed() {
		this.settingsScreen.GetComponent<SettingsScreenOperations> ().openPopUp ();
	}

	void showBuildingDetailScreen() {
		this.buildingDetailScreen.GetComponent<BuildingDetailScreen> ().showBuildingDetailScreen ();
	}
}
