﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestsScreen : MonoBehaviour {

	public GameObject settingsView;
	public GameObject auroraPopUp;
	public QuestLevelPopUp questLevelPopUpScript;

	private GameObject dailyAmulet;
	private GameObject lumbermill;
	private GameObject quarry;
	private GameObject ironmill;
	private GameObject farm;
	private GameObject cityhall;
	private GameObject barracks;
	private GameObject pointsCell;
	private GameObject lancer;
	private GameObject guildMember;

	private Button dailyAmuletButton;
	private Button lumbermillButton;
	private Button quarryButton;
	private Button ironmillButton;
	private Button farmButton;
	private Button cityhallButton;
	private Button barracksButton;
	private Button pointsCellButton;
	private Button lancerButton;
	private Button guildMemberButton;

	private Button auroraButton;
	private Button questLevelButton;
	private Button dailyQuest;
	private Button cityExpansion;
	private Button recruitment;
	private Button questSettingButton;
	private Button guild;

	const string collapseArrow = "Quests/collapse_arrow@2x";
	const string expandArrow = "Quests/expand_arrow@2x";

	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.setImages ();
		this.hideAllObject ();
	}

	private void addObject() {
		this.dailyAmulet 	= GameObject.Find (Objects.Quests.dailyAmulets);
		this.lumbermill 	= GameObject.Find (Objects.Quests.lumbermill);
		this.quarry 		= GameObject.Find (Objects.Quests.quarry);
		this.ironmill 		= GameObject.Find (Objects.Quests.ironmill);
		this.farm 			= GameObject.Find (Objects.Quests.farm);
		this.cityhall 		= GameObject.Find (Objects.Quests.cityhall);
		this.barracks 		= GameObject.Find (Objects.Quests.barracks);
		this.pointsCell 	= GameObject.Find (Objects.Quests.pointsCell);
		this.lancer 		= GameObject.Find (Objects.Quests.lancer);
		this.guildMember 	= GameObject.Find (Objects.Quests.guildMember);
		this.auroraPopUp 	= GameObject.Find (Objects.Quests.questAuroraPopUp);

		this.dailyAmuletButton 	=  dailyAmulet.GetComponent<Button> ();
		this.lumbermillButton 	=  lumbermill.GetComponent<Button> ();
		this.quarryButton 		=  quarry.GetComponent<Button> ();
		this.ironmillButton 	=  ironmill.GetComponent<Button> ();
		this.farmButton 		=  farm.GetComponent<Button> ();
		this.cityhallButton 	=  cityhall.GetComponent<Button> ();
		this.barracksButton 	=  barracks.GetComponent<Button> ();
		this.pointsCellButton 	=  pointsCell.GetComponent<Button> ();
		this.lancerButton 		=  lancer.GetComponent<Button> ();
		this.guildMemberButton 	=  guildMember.GetComponent<Button> ();

		this.questSettingButton = getButtonComponent (Objects.Quests.questSettingButton);
		this.auroraButton 		= getButtonComponent (Objects.Quests.auroraButton);
		this.questLevelButton 	= getButtonComponent (Objects.Quests.questLevel);
		this.dailyQuest 		= getButtonComponent (Objects.Quests.dailyQuest);
		this.cityExpansion 		= getButtonComponent (Objects.Quests.cityExpansion);
		this.recruitment 		= getButtonComponent (Objects.Quests.recruitment);
		this.guild 				= getButtonComponent(Objects.Quests.guild);
	}

	private Button getButtonComponent(string name) {
		return GameObject.Find (name).GetComponent<Button> ();
	}

	private void addListeners() {
		this.questSettingButton.onClick.AddListener (settingsBtnPressed);
		this.auroraButton.onClick.AddListener (auroraButtonAction);
		this.questLevelButton.onClick.AddListener (questLevelAction);

		this.dailyQuest.onClick.AddListener (dailyQuestAction);
		this.cityExpansion.onClick.AddListener (cityExpansionAction);
		this.recruitment.onClick.AddListener (recruitmentAction);
		this.guild.onClick.AddListener (guildAction);

		this.dailyAmuletButton.onClick.AddListener (dailyAmuletButtonPressed);
		this.lumbermillButton.onClick.AddListener (lumbermillButtonPressed);
		this.quarryButton.onClick.AddListener (quarryButtonPressed);
		this.ironmillButton.onClick.AddListener (ironmillButtonPressed);
		this.farmButton.onClick.AddListener (farmButtonPressed);
		this.cityhallButton.onClick.AddListener (cityhallButtonPressed);
		this.barracksButton.onClick.AddListener (barracksButtonPressed);
		this.pointsCellButton.onClick.AddListener (pointsCellButtonPressed);
		this.lancerButton.onClick.AddListener (lancerButtonPressed);
		this.guildMemberButton.onClick.AddListener (guildMemberButtonPressed);

	}

	private void setImages() {
		this.setDailyQuestCollapseArrowImage ();
		this.setCityExpansionCollapseArrowImage ();
		this.setRecruitmentCollapseArrowImage ();
		this.setGuildCollapseArrowImage ();
	}

	public void hideAllObject() {
//		this.hideDailyAmulet();
//		this.hideLumbermill();
//		this.hideQuarry();
//		this.hideIronmill();
//		this.hideFarm();
//		this.hideCityhall();
//		this.hideBarracks();
//		this.hidePointsCell();
//		this.hideLancer();
//		this.hideGuildMember();
		Utility.hideGameObject (auroraPopUp);
		this.questLevelPopUpScript.hideSelf ();
	}

	//MARK:- Buttons Actions

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void questLevelAction() {
		this.questLevelPopUpScript.showSelfWith ("AURORA'S QUEST");
	}

	public void auroraButtonAction() {
		this.auroraPopUp.transform.SetAsLastSibling();
		Utility.showGameObject (auroraPopUp);
	}

	public void dailyQuestAction() {
		if (dailyAmulet.activeInHierarchy) {
			this.hideDailyAmulet();
			this.setDailyQuestExpandArrowImage ();

		} else {
			this.showDailyAmulet();
			this.setDailyQuestCollapseArrowImage ();
		}
	}

	private void dailyAmuletButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("Daily Amulet");
	}

	private void lumbermillButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("Lumbermill");
	}

	private void quarryButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("QUARRY");
	}

	private void ironmillButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("IronMill");
	}

	private void farmButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("Farm");
	}

	private void cityhallButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("CITY HALL");
	}

	private void barracksButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("Barracks");
	}

	private void pointsCellButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("Points");
	}

	private void lancerButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("Lancer");
	}

	private void guildMemberButtonPressed() {
		this.questLevelPopUpScript.showSelfWith ("Guild Member");
	}

	private void cityExpansionAction() {
		if (lumbermill.activeInHierarchy) {
			this.hideLumbermill ();
			this.hideQuarry ();
			this.hideIronmill ();
			this.hideFarm ();
			this.hideCityhall ();
			this.hideBarracks ();
			this.hidePointsCell ();
			this.setCityExpansionExpandArrowImage ();
		} else {
			this.showLumbermill();
			this.showQuarry();
			this.showIronmill();
			this.showFarm();
			this.showCityhall();
			this.showBarracks();
			this.showPointsCell();
			this.setCityExpansionCollapseArrowImage ();
		}
	}

	private void recruitmentAction() {
		if (lancer.activeInHierarchy) {
			this.hideLancer ();
			this.setRecruitmentExpandArrowImage ();
		} else {
			this.showLancer();
			this.setRecruitmentCollapseArrowImage ();
		}
	}

	private void guildAction() {
		if (guildMember.activeInHierarchy) {
			this.hideGuildMember ();
			this.setGuildExpandArrowImage ();
		} else {
			this.showGuildMember();
			this.setGuildCollapseArrowImage ();
		}
	}


	//MARK:- Hide Objects

	private void hideDailyAmulet() {
		Utility.hideGameObject (dailyAmulet);
	}

	private void hideLumbermill() {
		Utility.hideGameObject (this.lumbermill);
	}

	private void hideQuarry() {
		Utility.hideGameObject (quarry);
	}

	private void hideIronmill() {
		Utility.hideGameObject (ironmill);
	}

	private void hideFarm() {
		Utility.hideGameObject (farm);
	}

	private void hideCityhall() {
		Utility.hideGameObject (this.cityhall);
	}

	private void hideBarracks() {
		Utility.hideGameObject (this.barracks);
	}

	private void hidePointsCell() {
		Utility.hideGameObject (pointsCell);
	}

	private void hideLancer() {
		Utility.hideGameObject (lancer);
	}

	private void hideGuildMember() {
		Utility.hideGameObject (guildMember);
	}

	//MARK:- Show Objects

	private void showDailyAmulet() {
		Utility.showGameObject (dailyAmulet);
	}

	private void showLumbermill() {
		Utility.showGameObject (lumbermill);
	}

	private void showQuarry() {
		Utility.showGameObject (quarry);
	}

	private void showIronmill() {
		Utility.showGameObject (ironmill);
	}

	private void showFarm() {
		Utility.showGameObject (farm);
	}

	private void showCityhall() {
		Utility.showGameObject (cityhall);
	}

	private void showBarracks() {
		Utility.showGameObject (barracks);
	}

	private void showPointsCell() {
		Utility.showGameObject (pointsCell);
	}

	private void showLancer() {
		Utility.showGameObject (lancer);
	}

	private void showGuildMember() {
		Utility.showGameObject (guildMember);
	}

	//MARK:- Set Images


	private void setDailyQuestExpandArrowImage() {
		Utility.setImageToGameObject (Objects.Quests.dailyQuestCollapseArrow, expandArrow);
	}

	private void setDailyQuestCollapseArrowImage() {
		Utility.setImageToGameObject (Objects.Quests.dailyQuestCollapseArrow, collapseArrow);
	}

	private void setCityExpansionExpandArrowImage() {
		Utility.setImageToGameObject (Objects.Quests.cityExpansionCollapseArrow, expandArrow);
	}

	private void setCityExpansionCollapseArrowImage() {
		Utility.setImageToGameObject (Objects.Quests.cityExpansionCollapseArrow, collapseArrow);
	}

	private void setRecruitmentExpandArrowImage() {
		Utility.setImageToGameObject (Objects.Quests.recruitmentCollapseArrow, expandArrow);
	}

	private void setRecruitmentCollapseArrowImage() {
		Utility.setImageToGameObject (Objects.Quests.recruitmentCollapseArrow, collapseArrow);
	}
		
	private void setGuildExpandArrowImage() {
		Utility.setImageToGameObject (Objects.Quests.guildCollapseArrow, expandArrow);
	}

	private void setGuildCollapseArrowImage() {
		Utility.setImageToGameObject (Objects.Quests.guildCollapseArrow, collapseArrow);
	}




}
