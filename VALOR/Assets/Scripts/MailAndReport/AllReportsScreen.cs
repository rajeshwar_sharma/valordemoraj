﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllReportsScreen : MonoBehaviour {

	public Button reportButton;
	public GameObject reportDetail;

	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
	}
	
	private void addObject() {
	
	}

	private void addListeners() {
		this.reportButton.onClick.AddListener (reportBtnPressed);

	}

	public void reportBtnPressed() {
		print ("All Report");
		Utility.showGameObject (reportDetail);

	}



}
