﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum reportMailState {
	Report=1,
	Mail
};

public class MailAndReports : MonoBehaviour {

	private reportMailState state;

	public GameObject selectAllBtn;
	public GameObject composeBackBtnObj;
	public GameObject composeMailTitle;

	public GameObject tabBar;
	public Button mailBtn;
	public Button reportBtn;
	public GameObject composeBtnObj;
	public GameObject reportContentView;
	public GameObject mailContentView;
	public GameObject composeMailContentView;

	public GameObject detailMessageContent;
	public RectTransform detailMessageCell;
	public Text detailMessageText;


	private Button composeBackBtn;
	private Button composeMainBtn;

	private Button mailCell;

	// Use this for initialization
	void Start () {
		this.setupReportAndMail ();
		this.setUpDetailMsgCotent ();
	}

	/// <summary>
	/// Setups the report and mail.
	/// </summary>

	void setupReportAndMail() {
		this.getObjects ();
		this.addListeners ();
		this.setIntialState ();
		Utility.hideGameObject (detailMessageContent); 
	}

	void getObjects() {
		this.composeBackBtn = this.composeBackBtnObj.GetComponent<Button> ();
		this.composeMainBtn = this.composeBtnObj.GetComponent<Button> ();
		this.mailCell = this.mailContentView.GetComponentInChildren<Button> ();
	}

	void setUpDetailMsgCotent(){
		print ( detailMessageText.preferredHeight);
		detailMessageCell.sizeDelta = new Vector2 (detailMessageCell.rect.width, detailMessageText.preferredHeight + 20);
	}

	/// <summary>
	/// Adds the listeners.
	/// </summary>

	void addListeners() {
		
		this.composeBackBtn.onClick.AddListener (composeBackBtnPressed);
		this.composeMainBtn.onClick.AddListener (composeMainBtnPressed);

		mailBtn.onClick.AddListener (changeStateToMail);
		reportBtn.onClick.AddListener (changeStateToReport);
		this.mailCell.onClick.AddListener (detailMailCellPressed);

	}

	/// <summary>
	/// Sets the state initially.
	/// </summary>

	void setIntialState() {
		state = reportMailState.Report;
		this.hideComposeAndDetailView ();
		this.changeStateToReport ();
	}

	void composeBackBtnPressed() {
		this.hideComposeAndDetailView ();
		this.changeStateToMail ();
	}

	void composeMainBtnPressed() {
		this.changeStateToComposeMail ();
	}

	/// <summary>
	/// Hides the compose button.
	/// </summary>

	void hideComposeAndDetailView() {
		composeBtnObj.SetActive (false);
		Utility.hideGameObject (composeBackBtnObj);
		Utility.hideGameObject (composeMailTitle);
		Utility.hideGameObject (composeMailContentView);
		Utility.hideGameObject (detailMessageContent);
	}

	void showComposeButton() {
		this.composeBtnObj.SetActive (true);
	}

	void detailMailCellPressed(){
		print ("detailMailCellPressed");
		this.changeStateToDetailMsg ();
	
	}

	/// <summary>
	/// Changes the state to mail.
	/// </summary>

	void changeStateToMail() {
		state = reportMailState.Mail;
		Utility.showGameObject (selectAllBtn);
		Utility.showGameObject (tabBar);
		Utility.setImageToGameObject (Objects.ReportsAndMail.mailBtn,Sprites.ReportsAndMails.mailTextBg);
		Utility.setImageToGameObject (Objects.ReportsAndMail.reportBtn, Sprites.ReportsAndMails.mailReportTransparentBg);
		Utility.setColorToText (Objects.ReportsAndMail.mailTxt, Color.white);
		Utility.setColorToText (Objects.ReportsAndMail.reportTxt, Color.black);
		this.composeBtnObj.SetActive(true);
		this.reportContentView.SetActive (false);
		this.mailContentView.SetActive (true);
	}

	/// <summary>
	/// Changes the state to report.
	/// </summary>

	void changeStateToReport() {
		state = reportMailState.Report;
		Utility.showGameObject (selectAllBtn);
		Utility.showGameObject (tabBar);
		Utility.setImageToGameObject(Objects.ReportsAndMail.reportBtn,Sprites.ReportsAndMails.reportTextBg);
		Utility.setImageToGameObject (Objects.ReportsAndMail.mailBtn, Sprites.ReportsAndMails.mailReportTransparentBg);
		Utility.setColorToText (Objects.ReportsAndMail.reportTxt, Color.white);
		Utility.setColorToText (Objects.ReportsAndMail.mailTxt, Color.black);
		this.composeBtnObj.SetActive(false);
		this.reportContentView.SetActive (true);
		this.mailContentView.SetActive (false);
	}

	void changeStateToComposeMail(){
		composeMailTitle.GetComponent<Text> ().text = "COMPOSE MAIL";
		Utility.hideGameObject (reportContentView);
		Utility.hideGameObject (mailContentView);
		Utility.hideGameObject (composeBtnObj);
		Utility.hideGameObject (selectAllBtn);
		Utility.hideGameObject (tabBar);

		Utility.showGameObject (composeBackBtnObj);
		Utility.showGameObject (composeMailTitle);
		Utility.showGameObject (composeMailContentView);
	}

	void changeStateToDetailMsg(){
		composeMailTitle.GetComponent<Text> ().text = "MAIL";
		Utility.hideGameObject (reportContentView);
		Utility.hideGameObject (mailContentView);
		Utility.hideGameObject (composeBtnObj);
		Utility.hideGameObject (selectAllBtn);
		Utility.hideGameObject (tabBar);

		Utility.showGameObject (composeBackBtnObj);
		Utility.showGameObject (composeMailTitle);
		Utility.showGameObject (detailMessageContent);
	}



}
