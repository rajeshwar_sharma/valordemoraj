﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StandardWorldCell : MonoBehaviour {

	public string nameLabel { get; set; }
	public int index { get; set; }

	public Text nameLbl;
	public GameObject infoButton;
	public GameObject endedBtn;

	const string returnBtnImage = "StandardWorld/button_search@2x";
	const string endedBtnImage = "StandardWorld/button_request_disabled@2x";

	// Use this for initialization
	void Start () {
		this.updateValue ();
		this.addListners ();
	}

	void updateValue() {
		this.nameLbl.text = nameLabel;
	}

	void addListners () {
		infoButton.GetComponent<Button> ().onClick.AddListener (infoAction);
		endedBtn.GetComponent<Button> ().onClick.AddListener (endedBtnAction);
	}

	public void infoAction() {
		print ("Info Action Btn pressed"+ index );
	}

	public void endedBtnAction() { 
		print (" Entry and Ended Btn Pressed" + index);
	}

	public void setReturnButton() {
		Utility.setImageToGameObject (this.endedBtn , returnBtnImage);
		this.endedBtn.GetComponentInChildren<Text>().text = "Return";
	}
}
