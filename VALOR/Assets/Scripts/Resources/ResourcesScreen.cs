﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourcesScreen : MonoBehaviour {

	public GameObject settingsView;
	public StoreItemBulkResScreen storeItemBulkResourceScreenScript;
	public StoreItemClayScreen storeItemClayScreenScript;
	public StoreItemWoodScreen storeItemWoodScreenScript;
	public StoreItemIronScreen storeItemIronScreenScript;
	public StoreItemClayBoosterScreen storeItemClayBoosterScreenScript;
	public StoreItemWoodBoosterScreen storeItemWoodBoosterScreen;
	public StoreItemIronBoosterScreen storeItemIronBoosterScreen;
	public StoreItemOfferDetailScreen storeItemOfferDetailScreen;

	private Button backButton;
	private Button resourcesSettingButton;
	private Button resourcesCell;
	private Button clayCell;
	private Button woodCell;
	private Button ironCell;
	private Button clayBoosterCell;
	private Button woodBoosterCell;
	private Button ironBoosterCell;
	private Button instantResourceCell;


	void Start () {
		this.setupScreen ();	
	}
	
	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
	}

	private void addObject() {
		this.resourcesSettingButton = getButtonComponent (Objects.Resources.resourcesSettingButton);
		this.backButton 	 = 	getButtonComponent (Objects.Resources.backButton);
		this.resourcesCell 	 = 	getButtonComponent (Objects.Resources.resourcesCell);
		this.clayCell 		 = 	getButtonComponent (Objects.Resources.clayCell);
		this.woodCell 		 = 	getButtonComponent (Objects.Resources.woodCell);
		this.ironCell 		 = 	getButtonComponent (Objects.Resources.ironCell);
		this.clayBoosterCell = 	getButtonComponent (Objects.Resources.clayBoosterCell);
		this.woodBoosterCell = 	getButtonComponent (Objects.Resources.woodBoosterCell);
		this.ironBoosterCell = 	getButtonComponent (Objects.Resources.ironBoosterCell);
		this.instantResourceCell = getButtonComponent (Objects.Resources.instantResourceCell);
	}

	private Button getButtonComponent(string name) {
		return GameObject.Find (name).GetComponent<Button> ();
	}

	void addListeners() {
		this.resourcesSettingButton.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
		this.resourcesCell.onClick.AddListener (resourcesCellPressed);
		this.clayCell.onClick.AddListener (clayCellPressed);
		this.woodCell.onClick.AddListener (woodCellPressed);
		this.ironCell.onClick.AddListener (ironCellPressed);
		this.clayBoosterCell.onClick.AddListener (clayBoosterCellPressed);
		this.woodBoosterCell.onClick.AddListener (woodBoosterCellPressed);
		this.ironBoosterCell.onClick.AddListener (ironBoosterCellPressed);
		this.instantResourceCell.onClick.AddListener (instantResourceCellPressed);
	}

	public void hideSelf() {
		if (this.gameObject.activeInHierarchy){
			Utility.hideGameObject (this.gameObject);
		}
	}

	public void showSelf() {
		if (!this.gameObject.activeInHierarchy){
			Utility.showGameObject (this.gameObject);
		}
	}

	public void hidAllObjects() {
		this.hideSelf ();
		this.storeItemBulkResourceScreenScript.hideSelf() ;
		this.storeItemClayScreenScript.hideSelf ();
		this.storeItemWoodScreenScript.hideSelf ();
		this.storeItemIronScreenScript.hideSelf ();
		this.storeItemClayBoosterScreenScript.hideSelf ();
		this.storeItemWoodBoosterScreen.hideSelf ();
		this.storeItemIronBoosterScreen.hideSelf ();
		this.storeItemOfferDetailScreen.hideSelf ();
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void resourcesCellPressed() {
		this.storeItemBulkResourceScreenScript.showSelf ();
	}

	public void clayCellPressed() {
		print ("clayCellPressed");
		this.storeItemClayScreenScript.showSelf ();
	}

	public void woodCellPressed() {
		this.storeItemWoodScreenScript.showSelf ();
	}

	public void ironCellPressed() {
		this.storeItemIronScreenScript.showSelf ();

	}

	public void clayBoosterCellPressed() {
		this.storeItemClayBoosterScreenScript.showSelf ();
	}

	public void woodBoosterCellPressed() {
		this.storeItemWoodBoosterScreen.showSelf ();
	}

	public void ironBoosterCellPressed() {
		this.storeItemIronBoosterScreen.showSelf ();
	}

	public void instantResourceCellPressed() {
		this.storeItemOfferDetailScreen.showSelf ();

	}

}
