﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForgeScreen : MonoBehaviour {


	public GameObject settingsView;
	public GameObject forgeDetailView;
	public Button backButton;
	public Button settingBtn;

	public Button getMoreResource;
	public GameObject lancer;
	public GameObject lancerDetail;
	public GameObject sentry;
	public GameObject sentryDetail;
	public GameObject berserker;
	public GameObject berserkerDetail;
	public GameObject scout;
	public GameObject scoutDetail;
	public GameObject knight;
	public GameObject knightDetail;
	public GameObject guardian;
	public GameObject guardianDetail;
	public GameObject ram;
	public GameObject ramDetail;
	public GameObject ballista;
	public GameObject ballistaDetail;
	public GameObject expandAllSection;


	private Button lancerBtn;
	private Button sentryBtn;
	private Button berserkerBtn;
	private Button scoutBtn;
	private Button knightBtn;
	private Button guardianBtn;
	private Button ramBtn;
	private Button ballistaBtn;
	private Button expandAllSectionButton;
	private Image  expandAllSectionImage;
	private Text   expandAllSectionText;

	private Image  lancerExpandArrowImg;
	private Image  lancerResearchImg;
	private Button lancerDetailScreenBtn;

	private bool isExpandable = true;
	const string collapseArrow = "Quests/collapse_arrow_btn@2x";
	const string expandArrow = "Quests/expand_arrow_btn@2x";
	const string collapseAllSectionImg = "Forge/collapse_all_sections_btn@2x";
	const string expandAllSectionImg = "Forge/expand_all_sections_btn@2x";

	// Use this for initialization
	void Start () {
		this.setupScreen ();
		this.hideSelf ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.hideAllObjects ();
	}

	private void addObject() {
		this.lancerBtn = lancer.GetComponent<Button> ();

		foreach (Button buttn in lancer.GetComponentsInChildren<Button>()) {
			if (buttn.gameObject.name == "RightBtn") {
				this.lancerDetailScreenBtn = buttn;
			}
		}
		foreach (Image image in lancer.GetComponentsInChildren<Image>()) {
			if (image.gameObject.name == "ExpandArrow") {
				this.lancerExpandArrowImg = image;
			}else if (image.gameObject.name == "ResearchImage") {
				this.lancerResearchImg = image;
			}
		}

		this.expandAllSectionButton =  expandAllSection.GetComponent<Button> ();
		this.expandAllSectionImage =  expandAllSection.GetComponentInChildren<Image> ();
		this.expandAllSectionText =  expandAllSection.GetComponentInChildren<Text> ();
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
		this.lancerBtn.onClick.AddListener (lancerBtnPressed);
		this.lancerDetailScreenBtn.onClick.AddListener (lancerDetailScreenBtnPressed);
		this.expandAllSectionButton.onClick.AddListener (expandAllSectionBtnPressed);
	}

	public void hideAllObjects() {
		this.lancerDetailScreenBtn.gameObject.SetActive (false);
		this.hideLancerDetail ();
		this.hideSentryDetail ();
		this.hideBerserkerDetail ();
		this.hideScoutDetail ();
		this.hideKnightDetail ();
		this.hideGuardianDetail ();
		this.hideRamDetail ();
		this.hideBallistaDetail ();
	}

	private void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	private void backBtnPressed() {
		this.hideSelf ();
	}

	public void hideSelf() {
		if (this.gameObject.activeInHierarchy){
			Utility.hideGameObject (this.gameObject);
		}
	}
		
	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}

	public void expandAllSectionBtnPressed() {
		if (isExpandable) {
			this.showLancerDetail ();
			this.showSentryDetail ();
			this.showBerserkerDetail ();
			this.showScoutDetail ();
			this.showKnightDetail ();
			this.showGuardianDetail ();
			this.showRamDetail ();
			this.showBallistaDetail ();

			this.setCollapseAllSectionImage ();
			this.expandAllSectionText.text = "Collapse All Sections"; 
		} else {
			hideAllObjects ();
			this.setExpandAllSectionImage ();
			this.expandAllSectionText.text = "Expand All Sections";
		}

		isExpandable = !isExpandable;
	}

	private void lancerBtnPressed() {
		if (!lancerDetail.activeInHierarchy) {
			this.showLancerDetail ();
			this.lancerDetailScreenBtn.gameObject.SetActive (true);
		} else {
			this.lancerDetailScreenBtn.gameObject.SetActive (false);
			this.hideLancerDetail ();
		}
	}

	private void lancerDetailScreenBtnPressed() {
		print ("Lancer Detail Screen Btn Pressed");
		this.forgeDetailView.GetComponent<ForgeDetailScreen> ().showSelf(Objects.forgeDetailScreen.lancerTitle);
	}

	private void sentryBtnPressed() {

	}

	private void berserkerBtnPressed() {

	}

	private void scoutBtnPressed() {

	}

	private void knightBtnPressed() {

	}

	private void guardianBtnPressed() {

	}

	private void ramBtnPressed() {

	}

	private void ballistaBtnPressed() {

	}

	//MARK:- Hide Objects

	private void hideLancerDetail() {
		Utility.hideGameObject (this.lancerDetail);
		this.setLancerExpandArrowImage ();
	}

	private void hideSentryDetail() {
		Utility.hideGameObject (this.sentryDetail);
	}

	private void hideBerserkerDetail() {
		Utility.hideGameObject (this.berserkerDetail);
	}

	private void hideScoutDetail() {
		Utility.hideGameObject (this.scoutDetail);
	}

	private void hideKnightDetail() {
		Utility.hideGameObject (this.knightDetail);
	}

	private void hideGuardianDetail() {
		Utility.hideGameObject (this.guardianDetail);
	}

	private void hideRamDetail() {
		Utility.hideGameObject (this.ramDetail);
	}

	private void hideBallistaDetail() {
		Utility.hideGameObject (this.ballistaDetail);
	}
		
	//MARK:- Show Objects

	private void showLancerDetail() {
		Utility.showGameObject (this.lancerDetail);
		this.setLancerCollapseArrowImage ();
	}

	private void showSentryDetail() {
		Utility.showGameObject (this.sentryDetail);
	}

	private void showBerserkerDetail() {
		Utility.showGameObject (this.berserkerDetail);
	}

	private void showScoutDetail() {
		Utility.showGameObject (this.scoutDetail);
	}

	private void showKnightDetail() {
		Utility.showGameObject (this.knightDetail);
	}

	private void showGuardianDetail() {
		Utility.showGameObject (this.guardianDetail);
	}

	private void showRamDetail() {
		Utility.showGameObject (this.ramDetail);
	}

	private void showBallistaDetail() {
		Utility.showGameObject (this.ballistaDetail);
	}

	//Mark:- Set Image

	private void setLancerExpandArrowImage() {
		Utility.setImageToImageObject (this.lancerExpandArrowImg, expandArrow);
	}

	private void setLancerCollapseArrowImage() {
		Utility.setImageToImageObject (this.lancerExpandArrowImg, collapseArrow);
	}


	private void setExpandAllSectionImage() {
		Utility.setImageToImageObject (this.expandAllSectionImage, expandAllSectionImg);
	}

	private void setCollapseAllSectionImage() {
		Utility.setImageToImageObject (this.expandAllSectionImage, collapseAllSectionImg);
	}



}
