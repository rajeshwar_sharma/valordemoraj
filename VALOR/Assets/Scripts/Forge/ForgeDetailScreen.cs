﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForgeDetailScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button backButton;
	public Button settingBtn;
	public Text title;
	public Image mainImage;
	public Text description;

	const string lancerImg = "RallyPoint/lancerWeaponIcon";
	const string researchImg = "Forge/research_unresearched_img@2x";


	// Use this for initialization   
	void Start () {
		this.setupScreen ();
		this.hideSelf ();
	}

	private void setupScreen() {
		this.addListeners ();
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf (string title) {
		switch (title) {
		case Objects.forgeDetailScreen.lancerTitle: 
			this.title.text = title;
			this.description.text = Objects.forgeDetailScreen.lanceDescription;
			Utility.setImageToImageObject (this.mainImage, lancerImg);
			break;
		case Objects.forgeDetailScreen.sentryTitle: 
			this.title.text = title;
			Utility.setImageToImageObject (this.mainImage, researchImg);
			break;
		case Objects.forgeDetailScreen.berserkerTitle: 
			this.title.text = title;
			Utility.setImageToImageObject (this.mainImage, researchImg);
			break;
		default:
			break;
		}
		Utility.showGameObject (this.gameObject);
	}



}
