﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IronMineScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button backButton;
	public Button settingBtn;

	// Use this for initialization
	void Start () {
		this.setupScreen ();
		this.hideSelf ();
	}

	private void setupScreen() {
		this.addListeners ();
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}




}
