﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomBarMenuButtons : MonoBehaviour {

	public GameObject portalBtn;
	public GameObject questsBtn;
	public GameObject mapBtn;
	public GameObject guildBtn;
	public GameObject storeBtn;
	public GameObject portalView;
	public GameObject questsView;
	public GameObject guildView;
	public GameObject storeView;


	// Use this for initialization
	void Start () {
		this.setupBottomBar ();
	}

	// Update is called once per frame
	void Update () {

	}

	/// <summary>
	/// Setups the bottom bar.
	/// </summary>

	void setupBottomBar() {
		this.addListeners ();
		this.getObjects ();
		this.hideAllViews ();
	}

	/// <summary>
	/// Gets the objects.
	/// </summary>

	void getObjects() {
	}

	/// <summary>
	/// Adds the listeners.
	/// </summary>

	void addListeners() {
		this.portalBtn.GetComponent<Button> ().onClick.AddListener (portalSelected);
		this.questsBtn.GetComponent<Button> ().onClick.AddListener (questsSelected);
		this.mapBtn.GetComponent<Button> ().onClick.AddListener (mapSelected);
		this.guildBtn.GetComponent<Button> ().onClick.AddListener (guildSelected);
		this.storeBtn.GetComponent<Button> ().onClick.AddListener (storeSelected);
	}

	/// <summary>
	/// Portals the selected.
	/// </summary>

	void portalSelected() {
		this.hideAllViews ();
		this.showSelectedView (this.portalView);
		Utility.setImageToGameObject (Objects.BottomBar.portalBtn,Sprites.BottomBar.portalSelectedButton);
	}

	/// <summary>
	/// Questses the selected.
	/// </summary>

	void questsSelected() {
		this.hideAllViews ();
		this.showSelectedView (this.questsView);
		Utility.setImageToGameObject (Objects.BottomBar.questsBtn,Sprites.BottomBar.questsSelectedButton);
	}


	void mapSelected() {
		this.hideAllViews ();
		Utility.setImageToGameObject (Objects.BottomBar.mapBtn, Sprites.BottomBar.mapSelectedButton);
	}

	void guildSelected() {
		this.hideAllViews ();
		this.showSelectedView (this.guildView);
		Utility.setImageToGameObject (Objects.BottomBar.guildBtn, Sprites.BottomBar.guildSelectedButton);
	}

	void storeSelected() {
		this.hideAllViews ();
		this.showSelectedView (this.storeView);
		Utility.setImageToGameObject (Objects.BottomBar.storeBtn, Sprites.BottomBar.storeSelectedButton);
	}

	void showSelectedView(GameObject view) {
		this.hideTopBarMenus ();
		this.showView (view);
	}

	/// <summary>
	/// Shows the portal view.
	/// </summary>

	void showView(GameObject view) {
		if (!view.activeInHierarchy) {
			view.SetActive (true);
		}
	}

	/// <summary>
	/// Hides the portal view.
	/// </summary>

	void hideView(GameObject view) {
		if (view.activeInHierarchy) {
			view.SetActive (false);
		}
	}


	/// <summary>
	/// Hides all views.
	/// </summary>

	public void hideAllViews() {
		//this.portalView.GetComponentInChildren<MyTitlesScreen> ().hideMyTitleScreen ();
		//this.portalView.GetComponentInChildren<GetMoreTicketsScreen> ().hideGetMoreTicketScreen ();
		this.hideView (this.portalView);
		this.hideView (this.questsView);
		this.hideView (this.guildView);
		this.hideView (this.storeView);
		this.deselectAllButtons ();
	}

	void hideTopBarMenus() {
		GameObject.Find (Objects.villageScreen.topMenuBar).GetComponent<MenuOperations> ().hidePopUps ();
	}

	void deselectAllButtons() {
		Utility.setImageToGameObject (Objects.BottomBar.portalBtn, Sprites.BottomBar.portatDeselectedButton);
		Utility.setImageToGameObject (Objects.BottomBar.questsBtn, Sprites.BottomBar.questsDeselectedButton);
		Utility.setImageToGameObject (Objects.BottomBar.guildBtn, Sprites.BottomBar.guildDeselectedButton);
		Utility.setImageToGameObject (Objects.BottomBar.mapBtn, Sprites.BottomBar.mapDeselectedButton);
		Utility.setImageToGameObject (Objects.BottomBar.storeBtn, Sprites.BottomBar.storeDeselectedButton);
	}
}
