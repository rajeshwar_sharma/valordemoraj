﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuarryScreen : MonoBehaviour {

	public GameObject quarryBackBtn;
	public GameObject settingsBtn;
	public GameObject settingsScreen;
	public GameObject quarryScreen;

	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.addListerners ();
		this.hideQuarryScreen ();
	}

	void addListerners() {
		this.settingsBtn.GetComponent<Button> ().onClick.AddListener (showSettingsScreen);
		this.quarryBackBtn.GetComponent<Button> ().onClick.AddListener (hideQuarryScreen);
	}

	public void hideQuarryScreen() {
		Utility.hideGameObject (this.quarryScreen);
	}

	public void showQuarryScreen() {
		Utility.showGameObject (this.gameObject);
	}

	void showSettingsScreen() {
		Utility.showGameObject (this.settingsScreen);
	}
}
