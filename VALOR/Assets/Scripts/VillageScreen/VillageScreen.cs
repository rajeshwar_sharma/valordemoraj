﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;



public class VillageScreen : MonoBehaviour {

	public Button settingBtn;
	public GameObject settingsView;
	public Button cityHallBtn;
	public GameObject cityHallScreen;
	public Button loyaltyBtn;
	public GameObject loyaltyScreen;
	public Button lumberMillBtn;
	public GameObject lumberMillScreen;
	public Button farmBtn;
	public GameObject farmScreen;
	public Button quarryBtn;
	public GameObject quarryScreen;
	public Button fortuneBtn;
	public GameObject wheelOfFortuneScreen;
	public Button hidingPlaceBtn;
	public GameObject hidingPlaceScreen;
	public Button ironMineBtn;
	public GameObject ironMineScreen;
	public Button forgeBtn;
	public GameObject forgeScreen;
	public Button academyBtn;
	public GameObject academyScreen;
	public Button wallBtn;
	public GameObject wallScreen;
	public Button marketBtn;
	public GameObject marketScreen;
	public Button warehouseBtn;
	public GameObject warehouseScreen;
	public Button barracksBtn;
	public GameObject recruitScreen;
	public GameObject barracksScreen;


	// Use this for initialization
	void Start () {
		this.setupView ();
		//callService ();
	}

	void setupView() {
		this.addListeners ();
	}


	void callService(){
		string url = "http://104.198.76.173/api/init";
		WWWForm wwwForm = new WWWForm ();

	//	wwwForm.AddField ("Accept","application/json");
	//	wwwForm.AddField ("Content-Type","application/json");

		wwwForm.AddField ("ifa","");
		wwwForm.AddField ("villages_version", 0);
		wwwForm.AddField ("locale", "en");
		wwwForm.AddField ("device_locale","en");
		wwwForm.AddField ("device_os","8.3");
		wwwForm.AddField ("last_notification_poll","1");
		wwwForm.AddField ("version","3.3.6");
		wwwForm.AddField ("device_model","iPhone");
		wwwForm.AddField ("device_platform","Unknown iPhone");
		wwwForm.AddField ("user_id","26442605");
		wwwForm.AddField ("wid","603");
		wwwForm.AddField ("skip_error",1);
		wwwForm.AddField ("game_name","Valor");
		wwwForm.AddField ("mac_address","020000000000");
		wwwForm.AddField ("uuid","132A891EFECF451392D72F16C51633ECFFFFFFFF");
		wwwForm.AddField ("aid","384844988");
		wwwForm.AddField ("userid","132A891EFECF451392D72F16C51633ECFFFFFFFF");

		//WWW www = new WWW (url, wwwForm);
		UnityWebRequest www = UnityWebRequest.Post(url, wwwForm);
		//www.SetRequestHeader ("Accept","application/json");
		//www.SetRequestHeader ("Content-Type","application/x-www-form-urlencoded");
		www.SetRequestHeader ("Accept", "application/json");
		www.uploadHandler.contentType = "application/json";

		StartCoroutine (request(www));
	}

	IEnumerator request(UnityWebRequest www){
		yield return www.SendWebRequest();
		print ("Request Result");
		if (www.isNetworkError || www.isHttpError) {
			print (" Error ");
			Debug.Log (www.error);
		} else {
			if (www.isDone) {
				print ("Request Done");
				print (www.downloadHandler.text);
				string jsonResult = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
				InitModel init = new InitModel ();
				init = JsonUtility.FromJson<InitModel>(jsonResult);
				Debug.Log(jsonResult);
			} else {
				print ("No error but Request Not Done");
			}
		}
	}

	void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.cityHallBtn.onClick.AddListener (showCityHallScreen);
		this.loyaltyBtn.onClick.AddListener (showLoyaltyScreen);
		this.lumberMillBtn.onClick.AddListener (showLumberMillScreen);
		this.farmBtn.onClick.AddListener (showFarmScreen);
		this.quarryBtn.onClick.AddListener (showQuarryScreen);
		this.fortuneBtn.onClick.AddListener (showFortuneScreen);
		this.hidingPlaceBtn.onClick.AddListener (showHidingPlaceScreen);
		this.ironMineBtn.onClick.AddListener (showIronMineScreen);
		this.forgeBtn.onClick.AddListener (showForgeScreen);
		this.academyBtn.onClick.AddListener (showAcademyScreen);
		this.wallBtn.onClick.AddListener (showWallScreen);
		this.marketBtn.onClick.AddListener (showMarketScreen);
		this.warehouseBtn.onClick.AddListener (showWarehouseScreen);
		this.barracksBtn.onClick.AddListener (barracksBtnPressed);
	}

	public void hideAllScreens() {
		this.cityHallScreen.GetComponent<CityHallScreen> ().hideCityHallScreen ();
		this.loyaltyScreen.GetComponent<LoyaltyScreen> ().hideLoyaltyScreen ();
		this.lumberMillScreen.GetComponent<LumberMillScreen> ().hideLumberMillScreen ();
		this.farmScreen.GetComponent<FarmScreen> ().hideFarmScreen ();
		this.quarryScreen.GetComponent<QuarryScreen> ().hideQuarryScreen ();
		this.wheelOfFortuneScreen.GetComponent<WheelOfForuneScreen> ().hideSelf ();
		this.hidingPlaceScreen.GetComponent<HidingPlaceScreen> ().hideSelf ();
		this.ironMineScreen.GetComponent<IronMineScreen> ().hideSelf ();
		this.forgeScreen.GetComponent<ForgeScreen> ().hideSelf ();
		this.wallScreen.GetComponent <WallScreen> ().hideSelf ();
		this.marketScreen.GetComponent<MarketScreen> ().hideSelf ();
		this.warehouseScreen.GetComponent<WarehouseScreen> ().hideSelf ();

	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void barracksBtnPressed() {
		if (this.settingsView.GetComponent<SettingsScreenOperations> ().isGhaphicalRecruitmentSelected) {
			Utility.showGameObject (this.barracksScreen);
		} else {
			Utility.showGameObject (this.recruitScreen);
		}
	
	}


	void showCityHallScreen() {
		this.cityHallScreen.GetComponent<CityHallScreen> ().showCityHallScreen ();
	}

	void showLoyaltyScreen() {
		this.loyaltyScreen.GetComponent<LoyaltyScreen> ().showLoyaltyScreen ();
	}

	void showLumberMillScreen() {
		this.lumberMillScreen.GetComponent<LumberMillScreen> ().showLumberMillScreen ();
	}

	void showFarmScreen () {
		this.farmScreen.GetComponent<FarmScreen> ().showFarmScreen ();
	}

	void showQuarryScreen() {
		this.quarryScreen.GetComponent<QuarryScreen> ().showQuarryScreen ();
	}

	void showFortuneScreen() {
		this.wheelOfFortuneScreen.GetComponent<WheelOfForuneScreen> ().showSelf ();
	}

	void showHidingPlaceScreen() {
		this.hidingPlaceScreen.GetComponent<HidingPlaceScreen> ().showSelf ();
	}

	void showIronMineScreen() {
		this.ironMineScreen.GetComponent<IronMineScreen> ().showSelf ();
	}

	void showForgeScreen() {
		this.forgeScreen.GetComponent<ForgeScreen> ().showSelf ();
	}

	void showAcademyScreen() {
		this.academyScreen.GetComponent<AcademyScreen> ().showSelf ();	
	}

	void showWallScreen() {
		this.wallScreen.GetComponent<WallScreen> ().showSelf ();
	}

	void showMarketScreen() {
		this.marketScreen.GetComponent<MarketScreen> ().showSelf ();
	}
		
	void showWarehouseScreen() {
		this.warehouseScreen.GetComponent<WarehouseScreen> ().showSelf ();
	}


}
