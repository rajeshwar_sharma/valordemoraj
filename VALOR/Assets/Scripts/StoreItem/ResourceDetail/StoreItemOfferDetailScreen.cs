﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoreItemOfferDetailScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button backButton;
	public Button storeSettingBtn;
	public Button getInstantResourceTradeCell;
	public GameObject amountOfResourceCell;
	public Button accepteGoldBtn;

	private Button increaseBtn;
	private Button decreaseBtn;
	private Text amountOfResource;

	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
	}


	private void addObject() {
		foreach (Button buttn in amountOfResourceCell.GetComponentsInChildren<Button>()) {
			if (buttn.tag == "DecreaseBtn") {
				decreaseBtn = buttn;
			} 
			if (buttn.tag == "IncreaseBtn") {
				this.increaseBtn = buttn;
			}
		}
		foreach (Text text in amountOfResourceCell.GetComponentsInChildren<Text>()) {
			if (text.tag == "AmountOfResource") {
				amountOfResource = text;
			} 
		}
	}
	
	private void addListeners() {
		this.storeSettingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
		this.getInstantResourceTradeCell.onClick.AddListener(acceptGoldPressed);
		this.accepteGoldBtn.onClick.AddListener(acceptGoldPressed) ;
		this.decreaseBtn.onClick.AddListener (decreaseBtnPressed);
		this.increaseBtn.onClick.AddListener (inceaseBtnPressed);
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void decreaseBtnPressed() {
		int amount = System.Convert.ToInt32(amountOfResource.text);
		amountOfResource.text = (amount - 1).ToString ();
	}

	public void inceaseBtnPressed() {
		int amount = System.Convert.ToInt32(amountOfResource.text);
		amountOfResource.text = (amount + 1).ToString ();
	}

	public void acceptGoldPressed() {
	}

	public void hideSelf () {
		if (this.gameObject.activeInHierarchy){
			Utility.hideGameObject (this.gameObject);
		}
	}

	public void showSelf () {
		if (!this.gameObject.activeInHierarchy){
			Utility.showGameObject (this.gameObject);
		}
	}



}
