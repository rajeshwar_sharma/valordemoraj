﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StoreItemTruceScreen : MonoBehaviour {

	public GameObject settingsView;
	public GameObject getMoreGoldView;
	public Button backButton;
	public Button settingBtn;
	public Button getTruceCell;
	public GameObject bannerCell;
	public Button getMoreGoldBtn;


	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addListeners ();
	}


	void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
		this.getTruceCell.onClick.AddListener (getMoreGoldBtnPressed);
		this.getMoreGoldBtn.onClick.AddListener (getMoreGoldBtnPressed);
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void getMoreGoldBtnPressed() {
		if (getMoreGoldView.activeInHierarchy) {
			Utility.hideGameObject (this.getMoreGoldView);
		} else {
			Utility.showGameObjectAsLastSibling (this.getMoreGoldView);
		}
	}

	public void hideSelf () {
		if (this.gameObject.activeInHierarchy){
			Utility.hideGameObject (this.gameObject);
		}
	}

	public void showSelf () {
		if (!this.gameObject.activeInHierarchy){
			Utility.showGameObject (this.gameObject);
		}
	}




}
