﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StoreScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button storeSettingButton;
	public ResourcesScreen resourceScreenScript;
	public SpeedUpsScreen speedUpsScreenScript;
	public TacticsScreen tacticsScreenScript; 
	public CityAdminScreen cityAdminScreenScript;


	private Button getMoreGold;
	private Button buildingExtender;
	private Button resources;
	private Button speedUps;
	private Button tactics;
	private Button cityAdmin;

	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.hideAllObjects ();
	}

	private void addObject() {
		this.getMoreGold 		=  getButtonComponent (Objects.Store.getMoreGold);
		this.buildingExtender 	=  getButtonComponent (Objects.Store.buildingExtender);
		this.resources 			=  getButtonComponent (Objects.Store.resources);
		this.speedUps 			=  getButtonComponent (Objects.Store.speedUps);
		this.tactics 			=  getButtonComponent(Objects.Store.tactics);
		this.cityAdmin 			=  getButtonComponent (Objects.Store.cityAdmin);
	}

	Button getButtonComponent(string name) {
		return GameObject.Find (name).GetComponent<Button> ();
	}

	private void addListeners() {
		this.storeSettingButton.onClick.AddListener (settingsBtnPressed);
		this.getMoreGold.onClick.AddListener (getMoreGoldBtnPressed);
		this.buildingExtender.onClick.AddListener (buildingExtenderBtnPressed);
		this.resources.onClick.AddListener (resourcesBtnPressed);
		this.speedUps.onClick.AddListener (speedUpsBtnPressed);
		this.tactics.onClick.AddListener (tacticsBtnPressed);
		this.cityAdmin.onClick.AddListener (cityAdminBtnPressed);
	}


	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void hideAllObjects()  {
		this.resourceScreenScript.hidAllObjects ();
		this.resourceScreenScript.hideSelf ();
		this.speedUpsScreenScript.hideAllObjects ();
		this.speedUpsScreenScript.hideSelf ();
		this.tacticsScreenScript.hidAllObjects ();
		this.cityAdminScreenScript.hidAllObjects ();
	}



	void getMoreGoldBtnPressed() {
		print ("MoreGold Button Pressed");
	}

	void buildingExtenderBtnPressed() {
		print ("BuildingExtender Button Pressed");
	}

	void resourcesBtnPressed() {
		this.resourceScreenScript.showSelf ();
	}

	void speedUpsBtnPressed() {
		this.speedUpsScreenScript.showSelf ();
	}

	void tacticsBtnPressed() {
		this.tacticsScreenScript.showSelf ();
	}

	void cityAdminBtnPressed() {
		this.cityAdminScreenScript.showSelf ();
	}


}
