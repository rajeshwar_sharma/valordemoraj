﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedUpsScreen : MonoBehaviour {


	public GameObject settingsView;
	public Button backButton;
	public Button settingButton;
	public SpeedUpsReduceTimeBy15Screen speedUpsReduceTimeBy15Screen;
	public SpeedUpsReduceTimeBy1Screen speedUpsReduceTimeBy1Screen;
	public SpeedUpsReduceTimeBy2Screen speedUpsReduceTimeBy2Screen;
	public SpeedUpsReduceTimeBy8Screen speedUpsReduceTimeBy8Screen;

	private Button reduceBy15MinCell;
	private Button reduceBy1HourCell;
	private Button reduceBy2HourCell;
	private Button reduceBy8HourCell;



	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}
	
	void setupScreen() {
		this.addObject ();
		this.addListeners ();
	}

	private void addObject() {
		this.reduceBy15MinCell  = 	getButtonComponent (Objects.SpeedUpsScreen.reduceBy15MinCell);
		this.reduceBy1HourCell  = 	getButtonComponent (Objects.SpeedUpsScreen.reduceBy1HourCell);
		this.reduceBy2HourCell  = 	getButtonComponent (Objects.SpeedUpsScreen.reduceBy2HourCell);
		this.reduceBy8HourCell  = 	getButtonComponent (Objects.SpeedUpsScreen.reduceBy8HourCell);
	}

	private Button getButtonComponent(string name) {
		return GameObject.Find (name).GetComponent<Button> ();
	}

	private void addListeners() {
		this.backButton.onClick.AddListener (backBtnPressed);
		this.settingButton.onClick.AddListener (settingsBtnPressed);
		this.reduceBy15MinCell.onClick.AddListener (reduceBy15MinCellPressed);
		this.reduceBy1HourCell.onClick.AddListener (reduceBy1HourCellPressed);
		this.reduceBy2HourCell.onClick.AddListener (reduceBy2HourCellPressed);
		this.reduceBy8HourCell.onClick.AddListener (reduceBy8HourCellPressed);
	}

	public void hideAllObjects() {
		this.speedUpsReduceTimeBy15Screen.hideSelf ();
		this.speedUpsReduceTimeBy1Screen.hideSelf ();
		this.speedUpsReduceTimeBy2Screen.hideSelf ();
		this.speedUpsReduceTimeBy8Screen.hideSelf ();
	}

	public void hideSelf() {
		if (this.gameObject.activeInHierarchy){
			Utility.hideGameObject (this.gameObject);
		}
	}

	public void showSelf() {
		if (!this.gameObject.activeInHierarchy){
			Utility.showGameObject (this.gameObject);
		}
	}

	public void hidAllObjects() {
	
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void reduceBy15MinCellPressed() {
		this.speedUpsReduceTimeBy15Screen.showSelf ();
	}
		
	public void reduceBy1HourCellPressed() {
		this.speedUpsReduceTimeBy1Screen.showSelf ();
	}

	public void reduceBy2HourCellPressed() {
		this.speedUpsReduceTimeBy2Screen.showSelf ();
	}

	public void reduceBy8HourCellPressed() {
		this.speedUpsReduceTimeBy8Screen.showSelf ();
	}



}
