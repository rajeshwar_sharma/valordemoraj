﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TacticsScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button backButton;
	public Button SettingBtn;
	public StoreItemTruceScreen truceScreenScript;
	public StoreItemCombatScreen combatScreenScript;
	public StoreItemLootScreen lootScreenScript;
	public StoreItemPrestigeScreen prestigeScreenScript;
	public InstantSpyScreen instantSpyScreenScript;
	public RevealIncomingScreen revealIncomingScreenScript;


	private Button truceCell;
	private Button combatCell;
	private Button lootCell;
	private Button prestigeCell;
	private Button instantSpyCell;
	private Button revealTroopCell;


	void Start () {
		this.setupScreen ();
	}

	void setupScreen() {
		this.addObject ();
		this.addListeners ();

	}

	private void addObject() {
		this.truceCell 		= GameObject.Find (Objects.TacticsScreen.truceCell).GetComponent<Button> ();
		this.combatCell 	= GameObject.Find (Objects.TacticsScreen.combatCell).GetComponent<Button> ();
		this.lootCell 		= GameObject.Find (Objects.TacticsScreen.lootCell).GetComponent<Button> ();
		this.prestigeCell 	= GameObject.Find (Objects.TacticsScreen.prestigeCell).GetComponent<Button> ();
		this.instantSpyCell = GameObject.Find (Objects.TacticsScreen.instantSpyCell).GetComponent<Button> ();
		this.revealTroopCell= GameObject.Find (Objects.TacticsScreen.revealTroopCell).GetComponent<Button> ();
	}

	private void addListeners() {
		this.backButton.onClick.AddListener (backBtnPressed);
		this.SettingBtn.onClick.AddListener (settingsBtnPressed);
		this.truceCell.onClick.AddListener (truceCellPressed);
		this.combatCell.onClick.AddListener (combatCellPressed);
		this.lootCell.onClick.AddListener (lootCellPressed);
		this.prestigeCell.onClick.AddListener (prestigeCellPressed);
		this.instantSpyCell.onClick.AddListener (instantSpyCellPressed);
		this.revealTroopCell.onClick.AddListener (revealTroopCellPressed);
	}

	public void hideSelf() {
			Utility.hideGameObject (this.gameObject);
	}

	public void showSelf() {
			Utility.showGameObject (this.gameObject);
	}

	public void hidAllObjects() {
		this.truceScreenScript.hideSelf ();
		this.combatScreenScript.hideSelf ();
		this.lootScreenScript.hideSelf ();
		this.prestigeScreenScript.hideSelf ();
		this.instantSpyScreenScript.hideSelf ();
		this.revealIncomingScreenScript.hideSelf ();
		this.hideSelf ();
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void truceCellPressed() {
		this.truceScreenScript.showSelf ();
	}

	public void combatCellPressed() {
		this.combatScreenScript.showSelf ();
	}

	public void lootCellPressed() {
		this.lootScreenScript.showSelf ();
	}

	public void prestigeCellPressed() {
		this.prestigeScreenScript.showSelf (); 
	}

	public void instantSpyCellPressed() {
		this.instantSpyScreenScript.showSelf ();
	}

	public void revealTroopCellPressed() {
		this.revealIncomingScreenScript.showSelf ();
	}




}
