﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CityAdminScreen : MonoBehaviour {

	public GameObject settingsView;
	public GameObject getMoreGoldView;
	public Button backButton;
	public Button settingBtn;
	public StoreItemExtenderScreen extenderScreenScript;
	public StoreItemScholarScreen scholarScreenScript;

	private Button extenderCell;
	private Button scholarCell;


	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	void setupScreen() {
		this.addObject ();
		this.addListeners ();

	}

	private void addObject() {
		this.extenderCell = GameObject.Find (Objects.CityAdminScreen.extenderCell).GetComponent<Button> ();
		this.scholarCell = GameObject.Find (Objects.CityAdminScreen.scholarCell).GetComponent<Button> ();

	}

	private void addListeners() {
		this.backButton.onClick.AddListener (backBtnPressed);
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.extenderCell.onClick.AddListener (extenderCellPressed);
		this.scholarCell.onClick.AddListener (scholarCellPressed);
	}

	public void hideSelf() {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf() {
		Utility.showGameObject (this.gameObject);
	}

	public void hidAllObjects() {
		this.extenderScreenScript.hideSelf ();
		this.scholarScreenScript.hideSelf ();
		this.hideSelf ();
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void extenderCellPressed() {
		this.extenderScreenScript.showSelf ();
	}

	public void scholarCellPressed() {
		this.scholarScreenScript.showSelf ();
	}


}
	