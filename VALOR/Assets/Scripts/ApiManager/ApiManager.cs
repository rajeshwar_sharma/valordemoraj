﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface GetProcessIdInterface {
	 void getProcessIdCompleted (string processId);
}

public class ApiManager :MonoBehaviour , GetProcessIdInterface  {

	private InitCallManager initCallManager;
	private GetRequestManager villageGetRequestManager;
	private GetProcessDataManager getProcessDataManager;

	private ExpandCityDataManager expandCityDataManager;
	private CancelExpandCityDataManager cancelExpandCityDataManager;
	private GetTranslationDataManager getTranslationDataManager;

	private MarketDataManager marketDataManager;
	private RecruitTroopsDataManager recruitTroopsDataManager;






	void Start() {
		this.getManagers ();
		//this.getProcessId ();
		//expandCity ();
		//this.initCall();
		//getMarketDetail();
		recruitTroops ();
	}

	void getManagers() {
		this.initCallManager = GameObject.Find (Strings.Managers.initCallManager).GetComponent<InitCallManager>();
		this.villageGetRequestManager = GameObject.Find (Strings.Managers.villageGetRequestManager).GetComponent<GetRequestManager>();
		this.getProcessDataManager = GameObject.Find (Strings.Managers.getProcessDataManager).GetComponent<GetProcessDataManager> ();
		this.expandCityDataManager = GameObject.Find (Strings.Managers.expandCityDataManager).GetComponent<ExpandCityDataManager> ();
	
		this.marketDataManager = GameObject.Find (Strings.Managers.marketDataManager).GetComponent<MarketDataManager> ();
		this.recruitTroopsDataManager = GameObject.Find (Strings.Managers.recruitTroopsDataManager).GetComponent<RecruitTroopsDataManager> ();


	}

	void Update() {
	
	}

	/// <summary>
	/// Inits the call.
	/// </summary>

	void initCall() {
		this.initCallManager.makeInitDataRequest ();	
	}

	/// <summary>
	/// Gets the process identifier.
	/// </summary>

	void getProcessId() {
		this.villageGetRequestManager.getApiDataRequest (this);
	}

	/// <summary>
	/// Gets the process identifier when got response.
	/// </summary>
	/// <param name="processId">Process identifier.</param>

	public void getProcessIdCompleted (string processId) {
		if (Utility.isStringNotNullAndNotEmpty (processId)) {
			Utility.saveStringToPreferences (PreferencesKeys.processId, processId);
			this.getProcessData ();
		}
	}

	void getProcessData() {
		this.getProcessDataManager.getProcessData ();
	}

	void expandCity() {
		this.expandCityDataManager.expandCity ();
	}

	void getMarketDetail() {
		this.marketDataManager.getMarketDetail ();
	}

	void recruitTroops() {
		this.recruitTroopsDataManager.recruitTroops();
	}

}
