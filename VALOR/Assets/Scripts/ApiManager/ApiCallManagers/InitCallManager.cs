﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;

public class InitCallManager : MonoBehaviour,ApiResponseReceiver  {

	public GameObject loadIndicator;
	public GameObject goldPopUp;
	public InitModel initMode;
	public List<StandardWorldVM> stdModel;

	void Start() {
		this.initMode = new InitModel ();
	}

	void Update() {
	}

	public void makeInitDataRequest() {
		Utility.showGameObject (this.loadIndicator);
		UnityWebRequestManager requestManager = new UnityWebRequestManager();
		Dictionary<string,string> headerDict = new Dictionary<string, string> ();
		headerDict.Add("Accept",Strings.defaultAcceptType);

		InitDataModel initDataModel = new InitDataModel.Builder ()
			.setIfa ("2B1E1D39-E63E-47B6-9AB1-9BF867466D61")
			.setVillageVersion (0)
			.setLocale ("en")
			.setDeviceLocale ("en")
			.setVersion("3.3.6")
			.setDeviceModel("iPhone")
			.setLastNotificationPoll("0")
			.setDevicePlatform("Unknown iPhone")
			.setUserId(26442605)
			.setWid("603")
			.setSkipError(1)
			.setGameName("Valor")
			.setMacAddress("020000000000")
			.setUuid("132A891EFECF451392D72F16C51633ECFFFFFFFF")
			.setAId("384844988")
			.setSecondId("132A891EFECF451392D72F16C51633ECFFFFFFFF")
			.build();

		string url = Urls.BaseUrl.url + Urls.Endpoints.init;

		InitDataRequest request = new InitDataRequest (url,Strings.defaultContentType,headerDict, initDataModel);
		StartCoroutine (requestManager.makePostRequest<InitCallManager,InitModel>(request, this));

	}

	public void success <MODEL> (MODEL model) {
		InitModel initModel = model as InitModel;
		this.initMode = initModel;
		goldPopUp.GetComponent<ManageGoldPack> ().addObjectsToGridView (initMode);
		stdModel = new List<StandardWorldVM> ();
		stdModel = WorldToWorldVM.worldToStandardWorldVM (initModel.world,initMode.config);
		Utility.hideGameObject (loadIndicator);
	}

	public void failure(string error) {
		print (error);
		Utility.hideGameObject (loadIndicator);
	}
}
