﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetProcessDataManager : MonoBehaviour , ApiResponseReceiver {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void getProcessData() {

		UnityWebRequestManager requestManager = new UnityWebRequestManager();
		Dictionary<string,string> headerDict = new Dictionary<string, string> ();
		headerDict.Add("Accept",Strings.defaultAcceptType);

		GetProcessDataRequestModel processDataModel = new GetProcessDataRequestModel.Builder()
			.setIfa("7EF51FD5-AEED-4A48-931A-71B320704E18")
			.setLastNotificationPoll("11")
			.setSkipError(1)
			.setVid("679")
			.setUserId("283")
			.setUuId("71F017A788C74104A843818120135732FFFFFFFF")
			.setVillageVersion(0)
			.setHd(1)
			.setVersion("3.3.9")
			.setLocale("en")
			.setDeviceLocale("en-IN")
			.setGameName("Valor")
			.setWaiting(false)
			.setProcessID(this.getProcessId())
			.setMacAddress("020000000000")
			.setDeviceModel("iPhone")
			.setWid("225")
			.setOverViewPage(0)
			.setDeviceOS("11.2.6")
			.setDevicePlatform("Unknown iPhone")
			.setAid("1287054778")
			.setVillageVersion(0)
			.build();

		string url = Urls.BaseUrl.url + Urls.Endpoints.getProcessData;

		GetProcessDataRequest request = new GetProcessDataRequest (url,Strings.defaultContentType,headerDict, processDataModel);
		StartCoroutine (requestManager.makePostRequest<GetProcessDataManager,GetProcessDataModel>(request, this));
	}

	string getProcessId() {
		string processId = Utility.getValueFromPreferences (PreferencesKeys.processId);
		if (Utility.isStringNotNullAndNotEmpty (processId)) {
			return processId;
		}
		return "";
	}

	/// <summary>
	/// Success 
	/// </summary>
	/// <param name="model">Model.</param>
	/// <typeparam name="MODEL">The 1st type parameter.</typeparam>

	public void success <MODEL> (MODEL model) {
		GetProcessDataModel processDataModel = model as GetProcessDataModel;
		print (processDataModel.buildings);
	}

	/// <summary>
	/// Failure.
	/// </summary>
	/// <param name="error">Error.</param>

	public void failure(string error) {

	}
}
