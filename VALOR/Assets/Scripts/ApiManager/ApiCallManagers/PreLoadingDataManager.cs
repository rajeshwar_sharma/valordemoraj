﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PreLoadingDataManager : MonoBehaviour, ApiResponseReceiver{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void getPreLoading() {

		UnityWebRequestManager requestManager = new UnityWebRequestManager();
		Dictionary<string,string> headerDict = new Dictionary<string, string> ();
		headerDict.Add("Accept",Strings.defaultAcceptType);

		PreLoadingDataRequestModel requestModel = new PreLoadingDataRequestModel.Builder()
			.setIfa("DC98AD80-8CC5-4AB0-AF95-98AE843A69C1")
			.setSkipError(1)
			.setUserId("160")
			.setUuId("606FA4270AAB46FBA683847A4DEFFD50FFFFFFFF")
			.setVillageVersion(0)
			.setVersion("3.3.8")
			.setLocale("en")
			.setDeviceLocale("en-IN")
			.setGameName("Valor")
			.setMacAddress("020000000000")
			.setDeviceModel("iPhone")
			.setWid("251")
			.setDeviceOS("10.3.1")
			.setDevicePlatform("Unknown iPhone")
		    .setAid("1287054778")
			.build();

		string url = Urls.BaseUrl.url + Urls.Endpoints.preLoading;
		PreLoadingDataRequest request = new PreLoadingDataRequest (url,Strings.defaultContentType,headerDict, requestModel);
		StartCoroutine (requestManager.makePostRequest<PreLoadingDataManager,PreLoadingModel>(request, this));
	}


	/// <summary>
	/// Success 
	/// </summary>
	/// <param name="model">Model.</param>
	/// <typeparam name="MODEL">The 1st type parameter.</typeparam>

	public void success <MODEL> (MODEL model) {
		PreLoadingModel dataModel = model as PreLoadingModel;

	}

	/// <summary>
	/// Failure.
	/// </summary>
	/// <param name="error">Error.</param>

	public void failure(string error) {
		print (error);
	}
}

