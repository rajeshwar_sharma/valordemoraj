﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RallyCommandDataManager : MonoBehaviour, ApiResponseReceiver
{

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{

	}

	public void recruitTroops() {

		UnityWebRequestManager requestManager = new UnityWebRequestManager();
		Dictionary<string,string> headerDict = new Dictionary<string, string> ();
		headerDict.Add("Accept",Strings.defaultAcceptType);


		RallyCommandDataRequestModel requestModel = new RallyCommandDataRequestModel.Builder()
			.setIfa("DC98AD80-8CC5-4AB0-AF95-98AE843A69C1")
			.setUserId("160")
			.setVid("752")
			.setUuId("606FA4270AAB46FBA683847A4DEFFD50FFFFFFFF")
			.setVillageVersion(0)
			.setVersion("3.3.8")
			.setLocale("en")
			.setDeviceLocale("en-IN")
			.setGameName("Valor")
			.setMacAddress("020000000000")
			.setDeviceModel("iPhone")
			.setWid("251")
			.setDeviceOS("10.3.1")
			.setDevicePlatform("Unknown iPhone")
			.setAid("1287054778")
			.setClass("attackDetails")
			.setRally_t_r(0)
			.setRally_t_sf(4)
			.setRally_t_sc(0)
			.setRally_t_hc(0)
			.setRally_t_lc(0)
			.setRally_t_a(0)
			.setRally_t_c(0)
			.setRally_t_sw(0)
			.setTarget_x(0)
			.setRally_key("a")
			.setTarget_y(93)
		.setPop_success(8887)
			.setRally_t_n(0)
			.build();
		

		string url = Urls.BaseUrl.url + Urls.Endpoints.rallyCommands;
		RallyCommandDataReqest request = new RallyCommandDataReqest (url,Strings.defaultContentType,headerDict, requestModel);
		StartCoroutine (requestManager.makePostRequest<RallyCommandDataManager,RallyCommandModel>(request, this));
	}


	/// <summary>
	/// Success 
	/// </summary>
	/// <param name="model">Model.</param>
	/// <typeparam name="MODEL">The 1st type parameter.</typeparam>

	public void success <MODEL> (MODEL model) {
		RallyCommandModel dataModel = model as RallyCommandModel;

	}

	/// <summary>
	/// Failure.
	/// </summary>
	/// <param name="error">Error.</param>

	public void failure(string error) {
		print (error);
	}
}

