﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetRequestManager : MonoBehaviour , ApiResponseReceiver {

	GetProcessIdInterface responseWithId;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Gets the API data request.
	/// </summary>

	public void getApiDataRequest(GetProcessIdInterface processIdInterface) {

		this.responseWithId = processIdInterface;

		UnityWebRequestManager requestManager = new UnityWebRequestManager();
		Dictionary<string,string> headerDict = new Dictionary<string, string> ();
		headerDict.Add("Accept","application/json");

		GetRequestModel getDataModel = new GetRequestModel.Builder ()
			.setWaiting(false)
			.setSkipError(1)
			.setAid ("1287054778")
			.setLastNotificationPoll ("11")
			.setDeviceLocale ("en-IN")
			.setDeviceModel ("iPhone")
			.setDeviceOS ("11.2.6")
			.setDevicePlatform ("Unknown iPhone")
			.setEnter ("1")
			.setGameName ("Valor")
		    .setIfa ("7EF51FD5-AEED-4A48-931A-71B320704E18")
			.setLocale ("en")
			.setDeviceModel ("iPhone")
			.setMacAddress ("020000000000")
			.setOverviewPage ("0")
			.setUserId ("283")
			.setUuid ("71F017A788C74104A843818120135732FFFFFFFF")
			.setVersion ("3.3.9")
			.setWid ("225")
			.setVid ("679")
			.setVillageVersion (0)
			.setResume(1)
			.build();

		string url = Urls.BaseUrl.url + Urls.Endpoints.get;
		GetRequest request = new GetRequest (url,Strings.defaultContentType,headerDict,getDataModel);
		StartCoroutine (requestManager.makePostRequest<GetRequestManager,GetModel>(request, this));

	}

	/// <summary>
	/// Success 
	/// </summary>
	/// <param name="model">Model.</param>
	/// <typeparam name="MODEL">The 1st type parameter.</typeparam>

	public void success <MODEL> (MODEL model) {
		GetModel getModel = model as GetModel;
		if (Utility.isStringNotNullAndNotEmpty (getModel.process_id)) {
			this.responseWithId.getProcessIdCompleted (getModel.process_id);
		}
	}

	/// <summary>
	/// Failure.
	/// </summary>
	/// <param name="error">Error.</param>

	public void failure(string error) {
		
	}
}
