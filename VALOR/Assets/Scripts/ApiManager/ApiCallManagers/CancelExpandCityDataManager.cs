﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CancelExpandCityDataManager : MonoBehaviour, ApiResponseReceiver {

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void cancelExpandCity() {

		UnityWebRequestManager requestManager = new UnityWebRequestManager();
		Dictionary<string,string> headerDict = new Dictionary<string, string> ();
		headerDict.Add("Accept",Strings.defaultAcceptType);
			
			
		CancelExpandCityDataRequestModel cancelExpandDataModel = new CancelExpandCityDataRequestModel.Builder()
			.setIfa("DC98AD80-8CC5-4AB0-AF95-98AE843A69C1")
			.setVid("752")
			.setUserId("160")
			.setUuId("606FA4270AAB46FBA683847A4DEFFD50FFFFFFFF")
			.setVillageVersion(0)
			.setVersion("3.3.8")
			.setLocale("en")
			.setDeviceLocale("en-IN")
			.setGameName("Valor")
			.setMacAddress("020000000000")
			.setDeviceModel("iPhone")
			.setWid("251")
			.setDeviceOS("10.3.1")
			.setDevicePlatform("Unknown iPhone")
			.setAid("1287054778")
			.setKey("b_b")
			.setAssignmentID(1)
			.setClass("city_hall")
			.build();

		string url = Urls.BaseUrl.url + Urls.Endpoints.cancelExpandCity;

		CancelExpandCityDataRequest request = new CancelExpandCityDataRequest (url,Strings.defaultContentType,headerDict, cancelExpandDataModel);
		StartCoroutine (requestManager.makePostRequest<CancelExpandCityDataManager,ExpandCancelCityModel>(request, this));
	}


	/// <summary>
	/// Success the specified model.
	/// </summary>
	/// <param name="model">Model.</param>
	/// <typeparam name="MODEL">The 1st type parameter.</typeparam>

	public void success <MODEL> (MODEL model) {
		ExpandCancelCityModel expandCityModel = model as ExpandCancelCityModel;
		print ("rajeshwar..."+ expandCityModel.quests);
	}

	/// <summary>
	/// Failure.
	/// </summary>
	/// <param name="error">Error.</param>

	public void failure(string error) {
		print (error);
	}
}

