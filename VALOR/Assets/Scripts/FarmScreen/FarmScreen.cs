﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FarmScreen : MonoBehaviour {

	public GameObject farmBackBtn;
	public GameObject settingsBtn;
	public GameObject settingsScreen;


	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.addListerners ();
		this.hideFarmScreen ();
	}

	void addListerners() {
		this.farmBackBtn.GetComponent<Button> ().onClick.AddListener (hideFarmScreen);
		this.settingsBtn.GetComponent<Button> ().onClick.AddListener (showSettingsScreen);
	}

	public void hideFarmScreen() {
		Utility.hideGameObject (this.gameObject);
	}

	public void showFarmScreen() {
		Utility.showGameObject (this.gameObject);
	}

	void showSettingsScreen() {
		Utility.showGameObject (this.settingsScreen);
	}

}
