﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketScreen : MonoBehaviour {


	public GameObject settingsView;
	public Button backButton;
	public Button settingBtn;
	public GameObject sendBtnObject;
	public GameObject yourOfferBtnObject;
	public GameObject othersOfferBtnObject;
	public GameObject movementBtnObject;

	public ScrollRect scrollView;
	public GameObject sendVerticalContent;
	public GameObject yourOfferVerticalContent;
	public GameObject otherOfferVerticalContent;
	public GameObject movementVerticalContent;

	private Button sendBtn;
	private Button yourOfferBtn;
	private Button othersOfferBtn;
	private Button movementBtn;

	private Image sendBtnImage;
	private Image yourOfferBtnImage;
	private Image othersOfferBtnImage;
	private Image movementBtnImage;

	const string collapseArrow = "Market/dropdown_arrow_d@2x";
	const string expandArrow = "Market/dropdown_arrow@2x";


	// Use this for initialization
	void Start () {
		this.setupScreen ();
		this.hideSelf ();
	}
	
	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.sendBtnPressed ();
	}

	private void addObject() {
		this.sendBtn = sendBtnObject.GetComponent<Button> ();
		this.yourOfferBtn = yourOfferBtnObject.GetComponent<Button> ();
		this.othersOfferBtn = othersOfferBtnObject.GetComponent<Button> ();
		this.movementBtn = movementBtnObject.GetComponent<Button> ();

		this.sendBtnImage = sendBtnObject.GetComponent<Image> ();
		this.yourOfferBtnImage = yourOfferBtnObject.GetComponent<Image> ();
		this.othersOfferBtnImage = othersOfferBtnObject.GetComponent<Image> ();
		this.movementBtnImage = movementBtnObject.GetComponent<Image> ();
	}

	private void addListeners() {
		this.sendBtn.onClick.AddListener (sendBtnPressed);
		this.yourOfferBtn.onClick.AddListener (yourOfferBtnPressed);
		this.othersOfferBtn.onClick.AddListener (otherOfferBtnPressed);
		this.movementBtn.onClick.AddListener (movementBtnPressed);
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}

	void sendBtnPressed() {
		this.setSendBtnColor ();
		this.clearYourOfferBtnColor ();
		this.clearOtherOfferBtnColor ();
		this.clearMovementBtnColor ();
		scrollView.content = sendVerticalContent.GetComponent<RectTransform> ();
		Utility.showGameObject (this.sendVerticalContent);
		Utility.hideGameObject (this.yourOfferVerticalContent);
		Utility.hideGameObject (this.otherOfferVerticalContent);
		Utility.hideGameObject (this.movementVerticalContent);
	}

	void yourOfferBtnPressed() {
		this.setYourOfferBtnColor ();
		this.clearSendBtnColor ();
		this.clearOtherOfferBtnColor ();
		this.clearMovementBtnColor ();
		scrollView.content = yourOfferVerticalContent.GetComponent<RectTransform> ();
		Utility.showGameObject (this.yourOfferVerticalContent);
		Utility.hideGameObject (this.sendVerticalContent);
		Utility.hideGameObject (this.otherOfferVerticalContent);
		Utility.hideGameObject (this.movementVerticalContent);
	}

	void otherOfferBtnPressed() {
		this.setOtherOfferBtnColor ();
		this.clearSendBtnColor ();
		this.clearYourOfferBtnColor ();
		this.clearMovementBtnColor ();
		scrollView.content = otherOfferVerticalContent.GetComponent<RectTransform> ();
		Utility.showGameObject (this.otherOfferVerticalContent);
		Utility.hideGameObject (this.sendVerticalContent);
		Utility.hideGameObject (this.yourOfferVerticalContent);
		Utility.hideGameObject (this.movementVerticalContent);
	}

	void movementBtnPressed() {
		this.setMovementBtnColor ();
		this.clearSendBtnColor ();
		this.clearYourOfferBtnColor ();
		this.clearOtherOfferBtnColor ();
		scrollView.content = movementVerticalContent.GetComponent<RectTransform> ();
		Utility.showGameObject (this.movementVerticalContent);
		Utility.hideGameObject (this.sendVerticalContent);
		Utility.hideGameObject (this.yourOfferVerticalContent);
		Utility.hideGameObject (this.otherOfferVerticalContent);
	}

	private void setSendBtnColor() {
		var tempColor = sendBtnImage.color;
		tempColor.a = 255;
		sendBtnImage.color = tempColor;
	}

	private void setYourOfferBtnColor() {
		var tempColor = yourOfferBtnImage.color;
		tempColor.a = 255;
		yourOfferBtnImage.color = tempColor;
	}

	private void setOtherOfferBtnColor() {
		var tempColor = othersOfferBtnImage.color;
		tempColor.a = 255;
		othersOfferBtnImage.color = tempColor;
	}

	private void setMovementBtnColor() {
		var tempColor = movementBtnImage.color;
		tempColor.a = 255;
		movementBtnImage.color = tempColor;
	}

	private void clearSendBtnColor() {
		var tempColor = sendBtnImage.color;
		tempColor.a = 0;
		sendBtnImage.color = tempColor;
	}

	private void clearYourOfferBtnColor() {
		var tempColor = yourOfferBtnImage.color;
		tempColor.a = 0;
		yourOfferBtnImage.color = tempColor;
	}

	private void clearOtherOfferBtnColor() {
		var tempColor = othersOfferBtnImage.color;
		tempColor.a = 0;
		othersOfferBtnImage.color = tempColor;
	}

	private void clearMovementBtnColor() {
		var tempColor = movementBtnImage.color;
		tempColor.a = 0;
		movementBtnImage.color = tempColor;
	}


}
