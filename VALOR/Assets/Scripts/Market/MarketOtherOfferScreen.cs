﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketOtherOfferScreen : MonoBehaviour {


	public Button searchOfferBtn;
	public Image searchOfferArrowImg;
	public GameObject detailViewCell;

	public GameObject transportIncrementCell;
	public Button offerDetailCell;
	public Button offerValueDetailCell;
	public GameObject offerDetailScreen;

	private Button transportIncreaseBtn;
	private Button transportDecreaseBtn;
	private Text transportAmountOfResource;


	const string collapseArrow = "Market/dropdown_arrow_d@2x";
	const string expandArrow = "Market/dropdown_arrow@2x";

	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
	}

	private void addObject() {

		foreach (Button buttn in transportIncrementCell.GetComponentsInChildren<Button>()) {
			if (buttn.tag == Objects.decrementBtn) { this.transportDecreaseBtn = buttn; } 
			if (buttn.tag == Objects.incrementBtn) { this.transportIncreaseBtn = buttn; }
		}
		foreach (Text text in transportIncrementCell.GetComponentsInChildren<Text>()) {
			if (text.tag == "AmountOfResource") { transportAmountOfResource = text; } 
		}
	}
		
	private void addListeners() {
		this.searchOfferBtn.onClick.AddListener (searchOfferBtnPressed);

		this.transportDecreaseBtn.onClick.AddListener (this.decreaseBtnPressed);
		this.transportIncreaseBtn.onClick.AddListener (this.inceaseBtnPressed);

		this.offerDetailCell.onClick.AddListener (otherOffersOfferDetailCellPressed);
		this.offerValueDetailCell.onClick.AddListener (otherOffersOfferDetailCellPressed);
	}

	void searchOfferBtnPressed() {
		if (this.detailViewCell.activeInHierarchy) {
			Utility.hideGameObject (this.detailViewCell);
			Utility.setImageToImageObject (this.searchOfferArrowImg, expandArrow);
		} else {
			Utility.showGameObject (this.detailViewCell);
			Utility.setImageToImageObject (this.searchOfferArrowImg, collapseArrow);
		}
	}

	public void decreaseBtnPressed() {
		int amount = System.Convert.ToInt32(transportAmountOfResource.text);
		if (amount > 1) {
			transportAmountOfResource.text = (amount - 1).ToString ();
		}
	}

	public void inceaseBtnPressed() {
		int amount = System.Convert.ToInt32(transportAmountOfResource.text);
		transportAmountOfResource.text = (amount + 1).ToString ();
	}

	void otherOffersOfferDetailCellPressed() {
		this.offerDetailScreen.GetComponent<OfferDetailScreen> ().showSelf ();	
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}

}
