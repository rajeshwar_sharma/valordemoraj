﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MarketYourOfferScreen : MonoBehaviour {

	public Button createNewOfferBtn;
	public Image createNewOfferArrowImg;
	public GameObject DetailViewCell;

	public GameObject offerCell;
	public GameObject forCell;
	public GameObject transportCell;
	public GameObject ofOfferCell;

	private Button offerCellIncreaseBtn;
	private Button offerCellDecreaseBtn;
	private Text offerCellAmountOfResource;

	private Button forCellIncreaseBtn;
	private Button forCellDecreaseBtn;
	private Text forCellAmountOfResource;


	const string collapseArrow = "Market/dropdown_arrow_d@2x";
	const string expandArrow = "Market/dropdown_arrow@2x";

	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
	}

	private void addObject() {
		this.createNewOfferBtn.onClick.AddListener (createNewOfferBtnPressed);
		foreach (Button buttn in offerCell.GetComponentsInChildren<Button>()) {
			if (buttn.tag == Objects.decrementBtn) { this.offerCellDecreaseBtn = buttn; } 
			if (buttn.tag == Objects.incrementBtn) { this.offerCellIncreaseBtn = buttn; }
		}
		foreach (Text text in offerCell.GetComponentsInChildren<Text>()) {
			if (text.tag == "AmountOfResource") { offerCellAmountOfResource = text; } 
		}

		foreach (Button buttn in forCell.GetComponentsInChildren<Button>()) {
			if (buttn.tag == Objects.decrementBtn) { this.forCellDecreaseBtn = buttn; } 
			if (buttn.tag == Objects.incrementBtn) { this.forCellIncreaseBtn = buttn; }
		}
		foreach (Text text in forCell.GetComponentsInChildren<Text>()) {
			if (text.tag == "AmountOfResource") { forCellAmountOfResource = text; } 
		}

	}
		
	private void addListeners() {
		this.offerCellDecreaseBtn.onClick.AddListener (this.offerCellDecreaseBtnPressed);
		this.offerCellIncreaseBtn.onClick.AddListener (this.offerCellInceaseBtnPressed);

		this.forCellDecreaseBtn.onClick.AddListener (this.forCellDecreaseBtnPressed);
		this.forCellIncreaseBtn.onClick.AddListener (this.forCellInceaseBtnPressed);
	}


	void createNewOfferBtnPressed() {
		if (this.DetailViewCell.activeInHierarchy) {
			Utility.hideGameObject (this.DetailViewCell);
			Utility.setImageToImageObject (this.createNewOfferArrowImg, expandArrow);
		} else {
			Utility.showGameObject (this.DetailViewCell);
			Utility.setImageToImageObject (this.createNewOfferArrowImg, collapseArrow);
		}
	}

	public void offerCellDecreaseBtnPressed() {
		int amount = System.Convert.ToInt32(offerCellAmountOfResource.text);
		if (amount > 999) {
			offerCellAmountOfResource.text = (amount - 1000).ToString ();
		}
	}

	public void offerCellInceaseBtnPressed() {
		int amount = System.Convert.ToInt32(offerCellAmountOfResource.text);
		offerCellAmountOfResource.text = (amount + 1000).ToString ();
	}

	public void forCellDecreaseBtnPressed() {
		int amount = System.Convert.ToInt32(forCellAmountOfResource.text);
		if (amount > 999) {
			forCellAmountOfResource.text = (amount - 1000).ToString ();
		}
	}

	public void forCellInceaseBtnPressed() {
		int amount = System.Convert.ToInt32(forCellAmountOfResource.text);
		forCellAmountOfResource.text = (amount + 1000).ToString ();
	}
		
	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}



}
