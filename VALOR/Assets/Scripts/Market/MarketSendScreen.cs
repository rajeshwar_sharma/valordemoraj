﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MarketSendScreen : MonoBehaviour {

	public Slider woodSlider;
	public Text woodSliderTextField;

	public Slider claySlider;
	public Text claySliderTextField;

	public Slider ironSlider;
	public Text ironSliderTextField;


	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
	}

	private void addObject() {
		this.woodSlider.minValue = 0;
		this.woodSlider.maxValue = 5000;

		this.claySlider.minValue = 0;
		this.claySlider.maxValue = 5000;

		this.ironSlider.minValue = 0;
		this.ironSlider.maxValue = 5000;
	}


	private void addListeners() {
		this.woodSlider.onValueChanged.AddListener(delegate {woodSliderPressed(); });
		this.claySlider.onValueChanged.AddListener(delegate {claySliderPressed(); });
		this.ironSlider.onValueChanged.AddListener(delegate {ironSliderPressed(); });
	}


	void woodSliderPressed() {
		this.woodSliderTextField.text = ( (int) (this.woodSlider.value)).ToString ();
	}

	void claySliderPressed() {
		this.claySliderTextField.text = ( (int) (this.claySlider.value)).ToString ();
	}

	void ironSliderPressed() {
		this.ironSliderTextField.text = ( (int) (this.ironSlider.value)).ToString ();
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}



}
