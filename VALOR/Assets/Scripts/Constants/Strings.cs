﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Strings.
/// Contains list of strings used in the project
/// </summary>

public class Strings  {

	/// <summary>
	/// Network related error strings
	/// </summary>

	public const string networkNotAvailable = "Network not available";

	/// <summary>
	/// Network related strings
	/// </summary>
	public const string defaultContentType = "application/json";
	public const string defaultAcceptType =  "application/json";

	public struct MovementScreen {
		static public string noIncomingMovementMsg = "There is no incoming movement";
		static public string noOutgoingMovementMsg = "There is no outgoing movement";
	}

	public struct Managers {
		static public string initCallManager = "InitCallManager";
		static public string villageGetRequestManager = "VillageGetRequestManager";
		static public string getProcessDataManager = "GetProcessDataManager";
		static public string expandCityDataManager = "ExpandCityDataManager";

		static public string marketDataManager = "MarketDataManager";
		static public string recruitTroopsDataManager = "RecruitTroopsDataManager";


	}

}



public class Urls {

	/// <summary>
	/// BASE URLS
	/// </summary>

	public struct BaseUrl {
		static public string devUrl = "http://app.valor-game.com/";
		static public string stagingUrl = "http://192.214.104.241/";
		static public string url = "http://104.198.76.173/";
	}

	/// <summary>
	/// End Points
	/// </summary>

	public struct Endpoints {
		static public string  init = "api/init";
		static public string checkStatus = "api/check_event_status";
		static public string get = "village/get";
		static public string getProcessData = "village/get_process_data";
		static public string expandCity = "api/expand";
		static public string cancelExpandCity = "api/cancel_expand";
		static public string getTranslations = "api/get_translations";
		static public string preLoading = "api/pre_loading";
		static public string checkEventStatus = "api/check_event_status";
		static public string market = "village/market";
		static public string marketOther = "village/market_other";
		static public string recruitTroops = "api/recruit";
		static public string cancelRecruitTroops = "api/cancel_recruit";
		static public string rallyCommands = "api/rally";
		static public string cancelRallyCommands = "api/cancel_rally";




	}

}


public class PreferencesKeys {
	static public string processId = "processID";
}
