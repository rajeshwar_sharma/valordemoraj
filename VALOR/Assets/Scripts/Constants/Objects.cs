﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objects  {

	public const string decrementBtn = "DecreaseBtn";
	public const string incrementBtn = "IncreaseBtn";

	public class WholeLayout {
		public const string superView = "WholeLayout";
		public const string villageAndSideMenu = "VillageMenuStatusBottomBar";
	}

	/// <summary>
	///  Reports and mail objects
	/// </summary>

	public class ReportsAndMail {
		public const string mailBtn = "Mail";
		public const string reportBtn = "Reports";
		public const string mailTxt = "MailText";
		public const string reportTxt = "ReportText";
		public const string composeMailBtn = "ComposeMail";
		public const string mailAndReportVw = "MailAndReports";
		public const string arrow = "MessageReportArrow";
		public const string reportVerticalReport = "ReportsVerticalLayout";
		public const string contentReport = "ContentReport";
		public const string contentMail = "ContentMessage";


	}

	public class Notifications {
		public const string notificationVw = "Notifications";
		public const string notificationArrow = "NotificationArrow";
		public const string forumBtn = "ForumBtn";
		public const string notificationCell = "NotificationCell";
		public const string notificationVerticallayout = "NotificationVerticalLayout";
	}

	public class Ranking {
		public const string rankingVw = "Rankings";
		public const string rankingArrow = "RankingArrow";
		public const string worldRankTxt = "WorldRankText";
		public const string guildRankTxt = "GuildRankText";
		public const string worldRankBtn = "WorldRank";
		public const string guildRankBtn = "GuildRank";
		public const string globalBtn = "Global";
		public const string playerBtn = "Player";
		public const string pointsBtn = "Points";
		public const string globalRegionalView = "GlobalRegionalView";
		public const string playerGuildView = "PlayerGuildView";
		public const string pointsView = "PointsView";
	}

	public class TopBar {
		public const string message = "Message";
		public const string notificationBtn = "Notification";
		public const string rankingBtn = "Ranking";
		public const string openVillageBtn = "OpenVillageButton";
		public const string buyGoldBtn = "Coins";
		public const string sideMenuBtn = "SideMenuBtn";
	}

	public class GoldPopUp {
		public const string goldPopUpVw = "GoldPopUp";
		public const string crossBtn = "HideVwBtn"; 
		public const string grid = "ScrollViewGoldContent";
	}


	public class villageScreen {
		public const string cityHallBtn = "Cityhall";
		public const string workshopBtn = "Workshop";
		public const string lumbermillBtn = "Lumbermill";
		public const string warehouseBtn = "Warehouse";
		public const string farmBtn = "Farm";
		public const string forgeBtn = "Forge";
		public const string academyBtn = "Academy";
		public const string rallyPointBtn = "RallyPoint";
		public const string flagBtn = "Flag";
		public const string barracksBtn = "Barracks";
		public const string stable = "Stable";
		public const string fortune = "Fortune";
		public const string topMenuBar = "MenuBar";
	}

	public class SideMenu {
		public const string sideMenuView = "SideMenu";
	}

	public class Portal {
		public const string pointsView = "PointsBtn";
		public const string KSTView = "KSTBtn";
		public const string KSAView = "KSABtn";
		public const string KSDView = "KSDBtn";
		public const string RankView = "RankBtn";
		public const string portalScreen = "PortalScreen";
		public const string portalSettingsBtn = "PortalSettingsButton";
		public const string portalTitleBtn = "TitleImageAndButton";
	}

	public class BottomBar {
		public const string portalBtn = "PortalButton";
		public const string questsBtn = "QuestsButton";
		public const string mapBtn = "MapButton";
		public const string guildBtn = "GuildButton";
		public const string storeBtn = "StoreButton";
		public const string bottomBarView = "BottomBar";
	}

	public class SettingsScreen {
		public const string self = "SettingsScreen";
		public const string bannerSwitch = "SettingsBannerSwitch";
		public const string musicSwitch = "SettingsMusicSwitch";
		public const string soundSwitch = "SettingsSoundSwitch";
		public const string abbriviatedResources = "SettingsAbbreviatedResourceSwitch";
		public const string previousCitySwipeSwitch = "SettingsPreviousCitySwipeSwitch";
		public const string graphicalCitySwitch = "SettingsGraphicalCitySwitch";
		public const string graphicalRecruitmentSwitch = "SettingsGraphicalRecruitmentSwitch";
		public const string priotizeIncomingSwitch = "SettingsPriotizeIncomingSwitch";
		public const string notificationSFXSwitch = "SettingsNotificationSFXSwitch";
	}

	public class MyTitlesScreen {
		public const string self = "MyTitlesScreen";
		public const string backBtn = "MyTitlesBackButton";
		public const string settingsBtn = "MyTitlesSettingsScreenBtn";
		public const string privacySwitchBtn = "MyTitlesPublicPrivateSwitch";
		public const string collapseExpandBtn = "MyTitlesUnearnedTitles";
		public const string collapseExpandIcon = "MyTitlesUnearnedTitlesIcon";
		public const string unearnedTitleList = "MyTitlesUnearnedTitleList";
		public const string switchNubRight = "MyTitleSwitchNubRight";
		public const string switchNubLeft = "";
		public const string switchText = "MyTitleSwitchText";

	}

	public class WorldDetails {
		public const string self = "";
		public const string infoTab = "WorldDetailsButtonForInfoTab";
		public const string optionsTab = "WorldDetailsButtonForOptions";
		public const string infoBtn = "WorldDetailsOptionsInfoTabBtn";
		public const string optionsBtn = "WorldDetailsOptionsTxtBtn";
	}


	public class Guild {
		public const string guildSettingButton = "GuildSettingButton";
		public const string browseView = "BrowseScrollView";
		public const string invitesView = "InvitesScrollView";
		public const string createView = "CreateView";
		public const string guildScreen = "GuildScreen";
		public const string invitesButton = "Invites";
		public const string browseButton = "Browse";
		public const string createButton = "Create";
		public const string invitesImage = "InvitesImage";
		public const string browseImage = "BrowseImage";
		public const string createImage = "CreateImage";
	}

	public class GuildBrowseCell {
		public const string tag = "TagLabel";
		public const string points = "PointsLabel";
		public const string players = "PlayersNumber";
		public const string info = "InfoButton";
		public const string entryButton = "GuildRequestEntryButton";
	}

	public class Quests {
		public const string dailyAmulets = "DailyAmulets";
		public const string lumbermill = "LumbermillCell";
		public const string quarry = "QuarryCell";
		public const string ironmill = "Ironmill";
		public const string farm = "FarmCell";
		public const string cityhall = "CityhallCell";
		public const string barracks = "BarracksCell";
		public const string pointsCell = "PointsCell";
		public const string lancer = "Lancer";
		public const string guildMember = "GuildMember";
		public const string questsScreen = "QuestsScreen";
		public const string auroraButton = "Aurora_Image_Button";
		public const string questLevel = "QuestLevelButton";
		public const string dailyQuest = "DailyQuest";
		public const string cityExpansion = "CityExpansion";
		public const string recruitment = "Recruitment";
		public const string guild = "Guild";
		public const string dailyQuestCollapseArrow = "DailyQuestArrow";
		public const string cityExpansionCollapseArrow = "CityExpansionArrow";
		public const string recruitmentCollapseArrow = "RecruitmentArrow";
		public const string guildCollapseArrow = "GuildArrow";
		public const string questSettingButton = "QuestSettingButton";
		public const string questAuroraPopUp = "QuestAuroraPopUp";
	}

	public class Store {
		public const string storeScreen = "StoreScreen";
		public const string resourcesScreen = "ResourcesScreen";
		public const string storeSettingButton = "StoreSettingButton";
		public const string getMoreGold = "GetMoreGold";
		public const string buildingExtender = "BuildingExtender";
		public const string resources = "StoreResources";
		public const string speedUps = "SpeedUps";
		public const string tactics = "Tactics";
		public const string cityAdmin = "CityAdmin";
	}

	public class Resources {
		public const string resourcesSettingButton = "ResourcesSettingButton";
		public const string backButton = "ResourcesBackButton";
		public const string resourcesCell = "ResourcesCell";
		public const string clayCell = "ClayCell";
		public const string woodCell = "WoodCell";
		public const string ironCell = "IronCell";
		public const string clayBoosterCell = "ClayBoosterCell";
		public const string woodBoosterCell = "WoodBoosterCell";
		public const string ironBoosterCell = "IronBoosterCell";
		public const string instantResourceCell = "InstantResourceCell";

	}

	public class SpeedUpsScreen{
		public const string reduceBy15MinCell = "ReduceBy15MinCell";
		public const string reduceBy1HourCell = "ReduceBy1HourCell";
		public const string reduceBy2HourCell = "ReduceBy2HourCell";
		public const string reduceBy8HourCell = "ReduceBy8HourCell";

	}

	public class TacticsScreen{
		public const string truceCell = "TruceCell";
		public const string combatCell = "CombatCell";
		public const string lootCell = "LootCell";
		public const string prestigeCell = "PrestigeCell";
		public const string instantSpyCell = "InstantSpyCell";
		public const string revealTroopCell = "RevealTroopCell";
	}

	public class CityAdminScreen {
		public const string extenderCell = "QueueExtenderCell";
		public const string scholarCell = "PremiumSchorlarshipCell";
	}

	public class WorldType {
		public const string myWorldBtn = "MyWorldBtn";
		public const string standardWorldBtn = "StandardWorldBtn";
		public const string guildWorldBtn = "GuildWorldBtn";
		public const string ticketBtn = "WorldTypeTicketButton";
	}

	public class StandardWorld {
		public const string standardScrollView = "StandardScrollView";
		public const string exploredTag = "Explored";
		public const string unExploredTag = "UnExplored";

	}

	public class StandardWorldCell {
		public const string nameTag = "WorldNameLabel";
		public const string info = "StandardCellInfoBtn";
		public const string endedButton = "StandardEndedBtn";
	}

	public class worldMode {
		public const string worldScrollView = "WorldModeScrollView";

	}

	public class forgeDetailScreen {
		public const string lancerTitle = "LANCER";
		public const string sentryTitle = "SENTRY";
		public const string berserkerTitle = "BERSERKER";
		public const string scoutTitle = "SCOUT";
		public const string knightTitle = "KNIGHT";
		public const string guardianTitle = "GUARDIAN";
		public const string ramTitle = "RAM";
		public const string ballistaTitle = "BALLISTA";

		public const string lanceDescription = "While spears are aften considered an unsophisticated armament, their " +
			"long reach makes them highly effective against cavalry units. The study of spear technology can greatly " +
			"improve both offensive and defensive capabilities of your lancers. Research othese weapons can lead to new " +
			"discoveries such as hardened shaft heads for increased damage and steel shafts for increased durability.";
	
	
	}


}
