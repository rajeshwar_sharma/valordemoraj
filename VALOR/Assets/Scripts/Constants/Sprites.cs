﻿using System;
using System.Collections;
using UnityEngine;

	public class Sprites
	{
		/// <summary>
		/// Sprite Names 
		/// </summary>

		///	Reports and Mails sprites

		public class ReportsAndMails
		{
			public const string mailTextBg = "Popover/button_segmented_control_right@2x";
			public const string reportTextBg = "Popover/button_segmented_control_left@2x";
			public const string mailReportTransparentBg = "Popover/button_segmented_control_transparent@2x";
		}

		/// <summary>
		/// Bottom bar sprites
		/// </summary>

		public class BottomBar
		{
			public const string portatDeselectedButton = "Tabs/tab_bar0@2x"; 
			public const string portalSelectedButton = "Tabs/tab_bar0_d@2x";
			public const string questsDeselectedButton =  "Tabs/tab_bar1@2x";
			public const string questsSelectedButton = "Tabs/tab_bar1_d@2x";
			public const string mapSelectedButton =  "Tabs/tab_bar2_d@2x";
			public const string mapDeselectedButton = "Tabs/tab_bar2@2x";
			public const string guildSelectedButton ="Tabs/tab_bar3_d@2x"; 
			public const string guildDeselectedButton = "Tabs/tab_bar3@2x";
			public const string storeSelectedButton = "Tabs/tab_bar4_d@2x";
			public const string storeDeselectedButton = "Tabs/tab_bar4@2x";
		}

		/// <summary>
		/// Switch button sprites.
		/// </summary>

		public class SwitchButtonSprites {
			public const string switchButtonOn = "Settings/switch_on_img";
			public const string switchButtonOff = "Settings/switch_off_img";
		}

			

		public class MyTitlesButton {
			public const string titleListCollapseIcon = "Portal/MyTitles/button_collapse";
			public const string titleListExpandIcon = "Portal/MyTitles/button_expand";
		}

		public class Quests {
			public const string collapseArrow = "Quests/collapse_arrow_btn@2x";
			public const string expandArrow = "Quests/expand_arrow_btn@2x";
		}

		public class Guild {
			public const string invitesNormalImage = "Guild/icon_invite@2x";
			public const string invitesSelectedImage = "Guild/icon_invite_d@2x";
			public const string browseNormalImage = "Guild/icon_browse@2x";
			public const string browseSelectedImage = "Guild/icon_browse_d@2x";
			public const string createNormalImage = "Guild/icon_create@2x";
			public const string createSelectedImage = "Guild/icon_create_d@2x";
		}

		public class CityHall {
			public const string collapseIcon = "CityHall/collapseAllSection";
			public const string expandIcon = "CityHall/expandAllSection";
		}
	}

