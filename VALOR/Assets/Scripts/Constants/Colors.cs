﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Colors {
	static public Color portalTxtHighlight = new Color32(33,25,18,255);
	static public Color portalTxt = new Color32 (212, 191, 150,255);
	static public Color portalBtnHighlight = new Color32 (212, 191, 150, 255);
	static public Color portalBtn =  new Color32(33,25,18,255);
	static public Color movementToggleHightedBg = new Color32 (129,115,103,255);
	static public Color movementToggleDeHighlightedBg = new Color32 (215,199,155,255);
}
