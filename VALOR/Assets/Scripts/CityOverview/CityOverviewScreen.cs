﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CityOverviewScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button settingBtn;
	public Button incomingTroopBtn;
	public Image incomingArrowImg;
	public GameObject incomingDetailCell;
	public Button outgoingTroopBtn;
	public Image outgoingArrowImg;
	public GameObject outgingDetailCell;


	const string collapseArrow = "Market/dropdown_arrow@2x";
	const string expandArrow = "Market/dropdown_arrow_d@2x";
	// Use this for initialization
	void Start () {
		this.setupScreen ();
		//this.hideSelf ();
	}


	private void setupScreen() {
		this.addListeners ();
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.incomingTroopBtn.onClick.AddListener (incommingTroopBtnPressed);
		this.outgoingTroopBtn.onClick.AddListener (outgoingTroopBtnPressed);
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void incommingTroopBtnPressed(){
		if (incomingDetailCell.activeInHierarchy) {
			//Utility.hideGameObject (this.incomingDetailCell);
			this.incomingTroopBtn.gameObject.SetActive(true);
			Utility.setImageToImageObject (incomingArrowImg, collapseArrow);
			incomingDetailCell.gameObject.SetActive (false);
			this.outgoingTroopBtn.gameObject.SetActive (true);
			if (outgingDetailCell.activeInHierarchy) {
				this.outgingDetailCell.gameObject.SetActive (true);
			} else {
				this.outgingDetailCell.gameObject.SetActive (false);
			}
				
		}else {
			//Utility.showGameObjectAsLastSibling (this.incomingDetailCell);
			//Utility.setImageToImageObject (incomingArrowImg, expandArrow);
			this.incomingTroopBtn.gameObject.SetActive(true);
			Utility.setImageToImageObject (incomingArrowImg, expandArrow);
			incomingDetailCell.gameObject.SetActive (true);
			this.outgoingTroopBtn.gameObject.SetActive (true);
			if (outgingDetailCell.activeInHierarchy) {
				this.outgingDetailCell.gameObject.SetActive (true);
			} else {
				this.outgingDetailCell.gameObject.SetActive (false);
			}
		}
	}

	public void outgoingTroopBtnPressed() {
		if (outgingDetailCell.activeInHierarchy) {
			Utility.hideGameObject (this.outgingDetailCell);
			Utility.setImageToImageObject (outgoingArrowImg, collapseArrow);
		}else {
			Utility.showGameObjectAsLastSibling (this.outgingDetailCell);
			Utility.setImageToImageObject (outgoingArrowImg, expandArrow);
		}
	}




}
