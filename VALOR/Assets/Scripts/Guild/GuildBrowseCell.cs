﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuildBrowseCell : MonoBehaviour {

	public string tagLabel { get;set;}
	public string points { get; set;}
	public string playersNumber { get; set;}
	public int index { get; set;}

	public Text tagLbl;
	public Text pointsLbl;
	public Text playerNumbr;
	public GameObject infoButton; 
	public GameObject requestEntryBtn;
	public GameObject infoView;
	private GuildInfoScreen infoViewScript;

	IDictionary<int, GuildInfoViewModel> memberDict = new Dictionary<int, GuildInfoViewModel>()
	{
		{ 0, new GuildInfoViewModel() { 
				nameText = "James", tagText = "DP05", 
				memberText = "10", rankText = "18",totalPointsText = "39495P", 
				averageText = "2323P", pointsOfBestText= "13562P"  }
		},
		{ 1, new GuildInfoViewModel() { 
				nameText = "Dawn", tagText = "DP06", 
				memberText = "17", rankText = "11",totalPointsText = "39495P", 
				averageText = "4323P", pointsOfBestText= "13562P"  }
		},
		{ 2, new GuildInfoViewModel(){ 
				nameText = "Paul", tagText = "DP08", 
				memberText = "18", rankText = "10",totalPointsText = "39495P", 
				averageText = "2023P", pointsOfBestText= "13562P" }
		},
		{ 3, new GuildInfoViewModel(){ 
				nameText = "Andrew", tagText = "DP09", 
				memberText = "15", rankText = "19",totalPointsText = "39495P", 
				averageText = "1353P", pointsOfBestText= "13562P" }
		},
		{ 4, new GuildInfoViewModel(){ 
				nameText = "Mark", tagText = "DP12", 
				memberText = "12", rankText = "19",totalPointsText = "39495P", 
				averageText = "1123P", pointsOfBestText= "13562P" }
		},
		{ 5, new GuildInfoViewModel(){ 
				nameText = "Jack", tagText = "DP11", 
				memberText = "06", rankText = "19",totalPointsText = "39495P", 
				averageText = "2623P", pointsOfBestText= "13562P" }
		},
		{ 6, new GuildInfoViewModel(){ 
				nameText = "Jones", tagText = "DP14", 
				memberText = "08", rankText = "19",totalPointsText = "39495P", 
				averageText = "823P", pointsOfBestText= "13562P" }
		}
	};
	// Use this for initialization
	void Start () {
		this.addObject ();
		this.addListners ();
		this.hideAllObj ();
	}
		
	void addObject() {
		tagLbl.text 	 = tagLabel;
		pointsLbl.text 	 = points;
		playerNumbr.text = playersNumber;
		this.infoViewScript = infoView.GetComponent<GuildInfoScreen> ();
	}

	void addListners () {
		infoButton.GetComponent<Button> ().onClick.AddListener (infoAction);
		requestEntryBtn.GetComponent<Button> ().onClick.AddListener (requestEntryAction);
	}

	void hideAllObj() {
//		Utility.hideGameObject (this.infoView);
	}

	void infoAction() {

		if (this.memberDict.ContainsKey(index)) {
			GuildInfoViewModel dictValue = this.memberDict[index];

			this.infoViewScript.title.text = dictValue.nameText;
			this.infoViewScript.nameText.text = dictValue.nameText;
			this.infoViewScript.tagText.text = dictValue.tagText;
			this.infoViewScript.memberText.text = dictValue.memberText;
			this.infoViewScript.rankText.text = dictValue.rankText;
			this.infoViewScript.totalPointsText.text = dictValue.totalPointsText;
			this.infoViewScript.averageText.text = dictValue.averageText;
			this.infoViewScript.pointsOfBestText.text = dictValue.pointsOfBestText;

			Utility.showGameObject (this.infoView);
			//Utility.showGameObjectAsLastSibling (this.infoView);

		}

	}

	void requestEntryAction() {
		print ("Request Entry Button Pressed" + index);
	}


}
