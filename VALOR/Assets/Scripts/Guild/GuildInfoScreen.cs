﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuildInfoScreen : MonoBehaviour {

	public GameObject settingsView;
	public GuildMemeberScreen guildMemberScript;

	public Text title;
	public Button backButton;
	public Button settingBtn;
	public Button memberCell;

	public Text nameText;
	public Text tagText;
	public Text memberText;
	public Text rankText;
	public Text totalPointsText;
	public Text averageText;
	public Text pointsOfBestText;


	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListners ();
		this.hideSelf ();
	}

	private void addObject () {

	}

	private void addListners () {
		this.backButton.onClick.AddListener (backBtnPressed);
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.memberCell.onClick.AddListener (memberBtnPressed);
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void memberBtnPressed() {
		this.guildMemberScript.showSelf () ;
	}

	public void hideSelf() {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf() {
		Utility.showGameObject (this.gameObject);
	}



}
