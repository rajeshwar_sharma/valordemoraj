﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuildMemeberScreen : MonoBehaviour {


	public GameObject settingsView;
	public Button backButton;
	public Button settingBtn;

	public GameObject userNameText;
	private Text nameText;
	private RectTransform nameTextRectTransform;
	private LayoutElement nameTextLayoutElement;
	private float stringWidth;
 



	void Start () {

		this.setupScreen ();

		this.nameText = userNameText.GetComponent<Text> ();
		this.nameTextRectTransform = userNameText.GetComponent<RectTransform> ();
		this.nameTextLayoutElement = userNameText.GetComponent<LayoutElement>();

		this.nameText.text = "fvraytryyfyt566r55eetttt";
		this.stringWidth = nameTextRectTransform.rect.height;

	}

	private void setupScreen() {
		this.addListeners ();
		this.hideSelf ();
	}


	void OnGUI() {
//		var textDimensions = GUI.skin.label.CalcSize(new GUIContent("fvraytryyfyt566r55eett"));
//		print ("Width of string = " + textDimensions);
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void hideSelf() {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf() {
		Utility.showGameObject (this.gameObject);
	}




}
