﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuildScreen : MonoBehaviour {

	public GameObject guildBrowseCell;
	public GameObject settingsView;
	public GameObject invitesImageObject;
	public GameObject browseImgeObject;
	public GameObject createImageObject;

	public GameObject guildInfoScreen;

	private GameObject browseScrollView;
	private GameObject invitesScrollView;
	private GameObject createScrollView;


	private VerticalLayoutGroup browseLayoutGroup;
	private Button invitesButton;
	private Button browseButton;
	private Button createButton;
	private Button guildSettingButton;

	private Text invitesText;
	private Text browseText;
	private Text createText;

	const string invitesNormalImage = "Guild/icon_invite@2x";
	const string invitesSelectedImage = "Guild/icon_invite_d@2x";

	const string browseNormalImage = "Guild/icon_browse@2x";
	const string browseSelectedImage = "Guild/icon_browse_d@2x";

	const string createNormalImage = "Guild/icon_create@2x";
	const string createSelectedImage = "Guild/icon_create_d@2x";

	const string selectedTextColor = "FFFFCBFF";


	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.setDefaultImages ();
		this.addCellsToContent ();
		this.addListners ();
		this.hideAllViews ();
		this.browseButtonAction ();
	}

	void OnDisable() {
		this.browseButtonAction ();
	}

	void setDefaultImages() {
		this.setInvitesDefaultImage ();
		this.setBrowseDefaultImage ();
		this.setCreateDefaultImage ();
	}
		
	void addObject() {
		this.browseScrollView  = GameObject.Find (Objects.Guild.browseView);
		this.invitesScrollView = GameObject.Find (Objects.Guild.invitesView);
		this.createScrollView  = GameObject.Find (Objects.Guild.createView);
		this.browseLayoutGroup = browseScrollView.GetComponentInChildren<VerticalLayoutGroup> ();
		this.guildSettingButton = getButtonComponent (Objects.Guild.guildSettingButton);
		this.invitesButton = getButtonComponent (Objects.Guild.invitesButton);
		this.browseButton  = getButtonComponent (Objects.Guild.browseButton);
		this.createButton  = getButtonComponent (Objects.Guild.createButton);

		this.invitesText = GameObject.Find (Objects.Guild.invitesButton).GetComponentInChildren<Text> ();
		this.browseText  = GameObject.Find (Objects.Guild.browseButton).GetComponentInChildren<Text> ();
		this.createText  = GameObject.Find (Objects.Guild.createButton).GetComponentInChildren<Text> ();
	}

	void addCellsToContent() {
		for (int i = 0; i < 10; i++) {
			GameObject cell = Instantiate (guildBrowseCell) as GameObject;
			cell.transform.SetParent (browseLayoutGroup.transform, false);

			LayoutElement layoutElement = cell.GetComponent<LayoutElement>();
			if (layoutElement == null) {
				layoutElement = cell.gameObject.AddComponent<LayoutElement>();
			}
			layoutElement.preferredHeight = 120;
			GuildBrowseCell cellScript = cell.GetComponent <GuildBrowseCell>();
			cellScript.tagLabel = "DL0" + i;
			cellScript.points = "012345"+ i + "P";
			cellScript.playersNumber = ( 77 + i).ToString();
			cellScript.index = i;
			cellScript.infoView = this.guildInfoScreen;
		}
	}

	void infoAction() {
		print ("Info Action Button pressed");
	}


	Button getButtonComponent(string name) {
		return GameObject.Find (name).GetComponent<Button> ();
	}

	Image getImageComponent(string name) {
		return GameObject.Find (name).GetComponent<Image> ();
	}

	void addListners() {
		this.guildSettingButton.onClick.AddListener (settingsBtnPressed);
		this.invitesButton.onClick.AddListener (invitesButtonAction);
		this.browseButton.onClick.AddListener (browseButtonAction);
		this.createButton.onClick.AddListener (createButtonAction);
	}


	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	void invitesButtonAction() {
		
		if (!invitesScrollView.activeInHierarchy) {
			this.showInvites ();
			this.hideBrowse ();
			this.hideCreate ();
			this.invitesText.color = hexToColor (selectedTextColor);
			this.browseText.color = Color.black;
			this.createText.color = Color.black;
			this.setInvitesSelectedImage ();
			this.setBrowseDefaultImage ();
			this.setCreateDefaultImage ();
		}
	}

	void browseButtonAction() {

		if (!browseScrollView.activeInHierarchy) {
			this.showBrowse ();
			this.hideInvites ();
			this.hideCreate ();
			this.browseText.color = hexToColor (selectedTextColor);
			this.invitesText.color = Color.black;
			this.createText.color = Color.black;
			this.setBrowseSelectedImage ();
			this.setCreateDefaultImage ();
			this.setInvitesDefaultImage ();
		}
	}

	void createButtonAction() {
		if (!createScrollView.activeInHierarchy) {
			this.showCreate ();
			this.hideBrowse ();
			this.hideInvites ();
			this.createText.color = hexToColor (selectedTextColor);
			this.browseText.color = Color.black;
			this.invitesText.color = Color.black;
			this.setBrowseDefaultImage ();
			this.setCreateSelectedImage ();
			this.setInvitesDefaultImage ();
		}
	}

	public void hideAllViews() {
		this.hideBrowse ();
		this.hideInvites ();
		this.hideCreate ();
	}

	void hideInvites() {
		Utility.hideGameObject (invitesScrollView);
	}

	void hideBrowse() {
		Utility.hideGameObject (browseScrollView);
	}

	void hideCreate() {
		Utility.hideGameObject (createScrollView);
	}

	void showCreate() {
		Utility.showGameObject (createScrollView);
	}

	void showBrowse() {
		Utility.showGameObject (browseScrollView);
	}

	void showInvites() {
		Utility.showGameObject (invitesScrollView);
	}

	void setInvitesDefaultImage() {
		Utility.setImageToGameObject (invitesImageObject, invitesNormalImage);
	}

	void setBrowseDefaultImage() {
		Utility.setImageToGameObject (browseImgeObject, browseNormalImage);
	}

	void setCreateDefaultImage() {
		Utility.setImageToGameObject (createImageObject, createNormalImage);
	}

	void setInvitesSelectedImage() {
		Utility.setImageToGameObject (invitesImageObject, invitesSelectedImage);
	}

	void setBrowseSelectedImage() {
		Utility.setImageToGameObject (browseImgeObject, browseSelectedImage);
	}

	void setCreateSelectedImage() {
		Utility.setImageToGameObject (createImageObject, createSelectedImage);
	}


	public static Color hexToColor(string hex)
	{
		hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
		byte a = 255;//assume fully visible unless specified in hex
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		//Only use alpha if the string has enough characters
		if(hex.Length == 8){
			a = byte.Parse(hex.Substring(6,2), System.Globalization.NumberStyles.HexNumber);
		}
		return new Color32(r,g,b,a);
	}


}

