﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine.UI;

public class Utility: MonoBehaviour  {

	/// <summary>
	/// Ises the string not null and not empty.
	/// </summary>
	/// <returns><c>true</c>, if string not null and not empty was ised, <c>false</c> otherwise.</returns>
	/// <param name="str">String.</param>

	public static bool isStringNotNullAndNotEmpty(string str) {

		if ((str != "") && (str != null)) {
			return true;
		} else {
			return false;
		}

	}

	/// <summary>
	/// Ises the greater than zero.
	/// </summary>
	/// <returns><c>true</c>, if greater than zero was ised, <c>false</c> otherwise.</returns>
	/// <param name="num">Number.</param>

	public static bool isGreaterThanZero(int num) {
	
		if (num > 0) {
			return true;
		} else {
			return false;
		}
	}

	/// <summary>
	/// Sets the image to game object.
	/// </summary>
	/// <param name="objectName">Object name.</param>
	/// <param name="name">Name.</param>

	public static void setImageToGameObject(string objectName, string spriteName) {
		GameObject gameObj = GameObject.Find (objectName);
		Sprite spr = Resources.Load<Sprite> (spriteName);
		gameObj.GetComponent<Image> ().sprite = spr;

	}

	/// <summary>
	/// Sets the image to game object.
	/// </summary>
	/// <param name="objectName">Object name.</param>
	/// <param name="spriteName">Sprite name.</param>

	public static void setImageToGameObject(GameObject gameObj, string spriteName) {
		Sprite spr = Resources.Load<Sprite> (spriteName);
		gameObj.GetComponent<Image> ().sprite = spr;

	}

	public static void setImageToImageObject(Image image, string spriteName) {
		Sprite spr = Resources.Load<Sprite> (spriteName);
		image.sprite = spr;

	}


	/// <summary>
	/// Sets the color to text.
	/// </summary>
	/// <param name="objectName">Object name.</param>
	/// <param name="colorCode">Color code.</param>

	public static void setColorToText(string objectName,Color colorCode) {
		GameObject gameObj = GameObject.Find (objectName);
		gameObj.GetComponent<Text> ().color = colorCode;
	}

	public static void setColorToBtn(string objectName,string hexValue) {
	}

	public static void hideGameObject(GameObject obj) {
		if (obj.activeInHierarchy) {
			obj.SetActive (false);
		}
	}

	public static void showGameObject(GameObject obj) {
		if (!obj.activeInHierarchy) {
			obj.SetActive (true);
		}
	}

	public static void showGameObjectAsLastSibling(GameObject obj) {
		if (!obj.activeInHierarchy) {
			obj.transform.SetAsLastSibling();
			obj.SetActive (true);
		}
	}
		
	/// <summary>
	/// Sets the color with color code to text.
	/// </summary>
	/// <param name="objectName">Object name.</param>
	/// <param name="colorCode">Color code.</param>

//	public static void setColorWithColorCodeToText(string objectName,string colorCode) {
//		GameObject gameObj = GameObject.Find (objectName);
//		//gameObj.GetComponent<Text> ().color = Color;
//	}


	/// <summary>
	/// Saves the string to preferences.
	/// </summary>
	/// <param name="key">Key.</param>
	/// <param name="value">Value.</param>

	public static void saveStringToPreferences(string key,string value) {
		PlayerPrefs.SetString (key, value);
		PlayerPrefs.Save ();
	}


	/// <summary>
	/// Gets the value from preferences.
	/// </summary>
	/// <returns>The value from preferences.</returns>
	/// <param name="key">Key.</param>

	public static string getValueFromPreferences(string key) {
		if (Utility.isStringNotNullAndNotEmpty (key)) {
			string processID = PlayerPrefs.GetString (key);
			return processID;
		}
		return "";
	}

}


