﻿using UnityEngine;
using System.Collections;

public class RallyCommandDataRequestModel : CommonRequestModel
{

	public string ifa;
	public string aid;
	public string user_id;
	public string vid;
	public string uuid;
	public int village_version;
	public string version;
	public string locale;
	public string device_locale;
	public string game_name;
	public string mac_address;
	public string device_model;
	public string wid;
	public string device_os;
	public string device_platform;
	public string @class;
	public int rally_t_r;
	public int rally_t_sf;
	public int rally_t_sc;
	public int rally_t_hc;
	public int rally_t_lc;
	public int rally_t_a;
	public int rally_t_c;
	public int rally_t_sw;
	public int target_x;
	public string rally_key;
	public int target_y;
	public int pop_success;
	public int rally_t_n;

	public RallyCommandDataRequestModel(Builder builder) {
		this.ifa = builder.ifa;
		this.aid = builder.aid;
		this.user_id = builder.user_id;
		this.vid = builder.vid;
		this.uuid = builder.uuid;
		this.village_version = builder.village_version;
		this.version = builder.version;
		this.locale = builder.locale;
		this.device_locale = builder.device_locale;
		this.game_name = builder.game_name;
		this.mac_address = builder.mac_address;
		this.device_model = builder.device_model;
		this.wid = builder.wid;
		this.device_os = builder.device_os;
		this.device_platform = builder.device_platform;
		this.@class = builder.@class;
		this.rally_t_r = builder.rally_t_r;
		this.rally_t_sf = builder.rally_t_sf;
		this.rally_t_sc = builder.rally_t_sc;
		this.rally_t_hc = builder.rally_t_hc;
		this.rally_t_lc = builder.rally_t_lc;
		this.rally_t_a = builder.rally_t_a;
		this.rally_t_c = builder.rally_t_c;
		this.rally_t_sw = builder.rally_t_sw;
		this.target_x = builder.target_x;
		this.rally_key = builder.rally_key;
		this.target_y = builder.target_y;
		this.pop_success = builder.pop_success;
		this.rally_t_n = builder.rally_t_n;

	}
	public class Builder {

		public string ifa = "";
		public string aid = "";
		public string user_id = "";
		public string vid = "";
		public string uuid = "";
		public int village_version = 0;
		public string version = "";
		public string locale = "";
		public string device_locale = "";
		public string game_name = "";
		public string mac_address = "";
		public string device_model = "";
		public string wid = "";
		public string device_os = "";
		public string device_platform = "";
		public string @class = "";

		public int rally_t_r;
		public int rally_t_sf;
		public int rally_t_sc;
		public int rally_t_hc;
		public int rally_t_lc;
		public int rally_t_a;
		public int rally_t_c;
		public int rally_t_sw;
		public int target_x;
		public string rally_key;
		public int target_y;
		public int pop_success;
		public int rally_t_n;


		public Builder setIfa(string ifa) {
			if (Utility.isStringNotNullAndNotEmpty (ifa)) {
				this.ifa = ifa;
			}
			return this;
		}


		public Builder setUserId(string userId) {
			if (Utility.isStringNotNullAndNotEmpty (userId)) {
				this.user_id = userId;
			}
			return this;
		}

		public Builder setVid(string vid) {
			if (Utility.isStringNotNullAndNotEmpty (vid)) {
				this.vid = vid;
			}
			return this;
		}

		public Builder setUuId(string uuid) {
			if (Utility.isStringNotNullAndNotEmpty (uuid)) {
				this.uuid = uuid;
			}
			return this;
		}

		public Builder setVillageVersion(int villageVersion) {
			if(Utility.isGreaterThanZero(villageVersion)) {
				this.village_version = villageVersion;
			}	
			return this;
		}

		public Builder setAid(string aid) {
			if (Utility.isStringNotNullAndNotEmpty (aid)) {
				this.aid = aid;
			}
			return this;
		}

		public Builder setVersion(string version) {
			if (Utility.isStringNotNullAndNotEmpty (version)) {
				this.version = version;
			}

			return this;
		}



		public Builder setLocale(string locale) {
			if (Utility.isStringNotNullAndNotEmpty (locale)) {
				this.locale = locale;
			}

			return this;
		}

		public Builder setDeviceLocale(string device_locale) {
			if (Utility.isStringNotNullAndNotEmpty (device_locale)) {
				this.device_locale = device_locale;
			}

			return this;
		}


		public Builder setGameName(string game_name) {
			if (Utility.isStringNotNullAndNotEmpty (game_name)) {
				this.game_name = game_name;
			}
			return this;
		}



		public Builder setMacAddress(string macAddress) {
			if (Utility.isStringNotNullAndNotEmpty (macAddress)) {
				this.mac_address = macAddress;
			}
			return this;
		}


		public Builder setDeviceModel(string deviceModel) {
			if (Utility.isStringNotNullAndNotEmpty (deviceModel)) {
				this.device_model = deviceModel;
			}
			return this;
		}


		public Builder setWid(string wid) {
			if (Utility.isStringNotNullAndNotEmpty (wid)) {
				this.wid = wid;
			}
			return this;
		}


		public Builder setDeviceOS(string deviceOS) {
			if (Utility.isStringNotNullAndNotEmpty (deviceOS)) {
				this.device_os = deviceOS;
			}
			return this;
		}


		public Builder setDevicePlatform(string devicePlatform) {
			if (Utility.isStringNotNullAndNotEmpty (devicePlatform)) {
				this.device_platform = devicePlatform;
			}
			return this;
		}

		public RallyCommandDataRequestModel build() {
			RallyCommandDataRequestModel  model = new RallyCommandDataRequestModel (this);
			return model;
		} 

		public Builder setRally_t_r(int rally_t_r) {
			this.rally_t_r = rally_t_r;
			return this;
		}

		public Builder setRally_t_sf(int rally_t_sf) {
			this.rally_t_sf = rally_t_sf;
			return this;
		}

		public Builder setRally_t_sc(int rally_t_sc) {
			this.rally_t_sc = rally_t_sc;
			return this;
		}

		public Builder setRally_t_hc(int rally_t_hc) {
			this.rally_t_hc = rally_t_hc;
			return this;
		}

		public Builder setRally_t_lc(int rally_t_lc) {
			this.rally_t_lc = rally_t_lc;
			return this;
		}

		public Builder setRally_t_a(int rally_t_a) {
			this.rally_t_a = rally_t_a;
			return this;
		}

		public Builder setRally_t_c(int rally_t_c) {
			this.rally_t_c = rally_t_c;
			return this;
		}

		public Builder setRally_t_sw(int rally_t_sw) {
			this.rally_t_sw = rally_t_sw;
			return this;
		}

		public Builder setTarget_x(int target_x) {
			this.target_x = target_x;
			return this;
		}

		public Builder setRally_key(string rally_key) {
			this.rally_key = rally_key;
			return this;
		}

		public Builder setTarget_y(int target_y) {
			this.target_y = target_y;
			return this;
		}

		public Builder setPop_success(int pop_success) {
			this.pop_success = pop_success;
			return this;
		}

		public Builder setRally_t_n(int rally_t_n) {
			this.rally_t_n = rally_t_n;
			return this;
		}

		public Builder setClass(string @class) {
			this.@class = @class;
			return this;
		}
	}
}

