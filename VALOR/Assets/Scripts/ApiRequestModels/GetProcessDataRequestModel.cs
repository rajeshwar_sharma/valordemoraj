﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetProcessDataRequestModel : CommonRequestModel  {
	
	public string ifa;
	public string last_notification_poll;
	public int skip_error;
	public string vid;
	public string user_id;
	public string uuid;
	public int village_version;
	public int HD;
	public string aid;
	public string version;
	public string locale;
	public string device_locale;
	public string game_name;
	public bool waiting;
	public string process_id;
	public string mac_address;
	public string device_model;
	public string wid;
	public int overviews_page;
	public string device_os;
	public string device_platform;

	public GetProcessDataRequestModel(Builder builder) {
		this.ifa = builder.ifa;
		this.last_notification_poll = builder.last_notification_poll;
		this.skip_error = builder.skip_error;
		this.vid = builder.vid;
		this.user_id = builder.user_id;
		this.uuid = builder.uuid;
		this.village_version = builder.village_version;
		this.HD = builder.HD;
		this.aid = builder.aid;
		this.version = builder.version;
		this.locale = builder.locale;
		this.device_locale = builder.device_locale;
		this.game_name = builder.game_name;
		this.waiting = builder.waiting;
		this.process_id = builder.process_id;
		this.mac_address = builder.mac_address;
		this.device_model = builder.device_model;
		this.wid = builder.wid;
		this.overviews_page = builder.overviews_page;
		this.device_os = builder.device_os;
		this.device_platform = builder.device_platform;
	}


	public class Builder {

		public string ifa = "";
		public string last_notification_poll = "";
		public int skip_error = 0;
		public string vid = "";
		public string user_id = "";
		public string uuid = "";
		public int village_version = 0;
		public int HD = 0;
		public string aid = "";
		public string version = "";
		public string locale = "";
		public string device_locale = "";
		public string game_name = "";
		public bool waiting = false;
		public string process_id = "";
		public string mac_address = "";
		public string device_model = "";
		public string wid = "";
		public int overviews_page = 0;
		public string device_os = "";
		public string device_platform;

		public Builder setIfa(string ifa) {
			if (Utility.isStringNotNullAndNotEmpty (ifa)) {
				this.ifa = ifa;
			}
			return this;
		}


		public Builder setLastNotificationPoll(string lastNotificationPoll) {
			if (Utility.isStringNotNullAndNotEmpty (lastNotificationPoll)) {
				this.last_notification_poll = lastNotificationPoll;
			}
			return this;
		}

		public Builder setSkipError(int error) {
			this.skip_error = error;
			return this;
		}

		public Builder setVid(string vid) {
			if (Utility.isStringNotNullAndNotEmpty (vid)) {
				this.vid = vid;
			}
			return this;
		}

		public Builder setUserId(string userId) {
			if (Utility.isStringNotNullAndNotEmpty (userId)) {
				this.user_id = userId;
			}
			return this;
		}

		public Builder setUuId(string uuid) {
			if (Utility.isStringNotNullAndNotEmpty (uuid)) {
				this.uuid = uuid;
			}
			return this;
		}

		public Builder setVillageVersion(int villageVersion) {
			if(Utility.isGreaterThanZero(villageVersion)) {
				this.village_version = villageVersion;
			}	
			return this;
		}

		public Builder setHd(int HD) {
			this.HD = HD;
			return this;
		}

		public Builder setAid(string aid) {
			if (Utility.isStringNotNullAndNotEmpty (aid)) {
				this.aid = aid;
			}

			return this;
		}

		public Builder setVersion(string version) {
			if (Utility.isStringNotNullAndNotEmpty (version)) {
				this.version = version;
			}

			return this;
		}



		public Builder setLocale(string locale) {
			if (Utility.isStringNotNullAndNotEmpty (locale)) {
				this.locale = locale;
			}

			return this;
		}

		public Builder setDeviceLocale(string device_locale) {
			if (Utility.isStringNotNullAndNotEmpty (device_locale)) {
				this.device_locale = device_locale;
			}

			return this;
		}


		public Builder setGameName(string game_name) {
			if (Utility.isStringNotNullAndNotEmpty (game_name)) {
				this.game_name = game_name;
			}
			return this;
		}


		public Builder setWaiting(bool isWaiting) {
			this.waiting = isWaiting;
			return this;
		}


		public Builder setProcessID(string processId) {
			if (Utility.isStringNotNullAndNotEmpty (processId)) {
				this.process_id = processId;
			}
			return this;
		}


		public Builder setMacAddress(string macAddress) {
			if (Utility.isStringNotNullAndNotEmpty (macAddress)) {
				this.mac_address = macAddress;
			}
			return this;
		}


		public Builder setDeviceModel(string deviceModel) {
			if (Utility.isStringNotNullAndNotEmpty (deviceModel)) {
				this.device_model = deviceModel;
			}
			return this;
		}
			 

		public Builder setWid(string wid) {
			if (Utility.isStringNotNullAndNotEmpty (wid)) {
				this.wid = wid;
			}
			return this;
		}

		public Builder setOverViewPage(int overviews_page) {
			this.overviews_page = overviews_page;
			return this;
		}



		public Builder setDeviceOS(string deviceOS) {
			if (Utility.isStringNotNullAndNotEmpty (deviceOS)) {
				this.device_os = deviceOS;
			}
			return this;
		}


		public Builder setDevicePlatform(string devicePlatform) {
			if (Utility.isStringNotNullAndNotEmpty (devicePlatform)) {
				this.device_platform = devicePlatform;
			}
			return this;
		}

		public GetProcessDataRequestModel build() {
			GetProcessDataRequestModel requestModel = new GetProcessDataRequestModel (this);
			return requestModel;
		} 
	}


}
