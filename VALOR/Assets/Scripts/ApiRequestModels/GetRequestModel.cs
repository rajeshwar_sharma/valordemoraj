﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetRequestModel : CommonRequestModel {

	public string aid;
	public int skip_error;
	public string last_notification_poll;
	public string device_locale;
	public string device_model;
	public string device_os;
	public string device_platform;
	public string enter;
	public string ifa;
	public string locale;
	public string overviews_page;
	public string user_id;
	public string uuid;
	public string version;
	public string game_name;
	public string mac_address;
	public string wid;
	public string vid;
	public int villages_version;
	public int resume;
	public bool waiting;

	public GetRequestModel(Builder builder) {
		this.last_notification_poll = builder.last_notification_poll;
		this.aid = builder.aid;
		this.device_locale = builder.device_locale;
		this.device_model = builder.device_model;
		this.device_os = builder.device_os;
		this.device_platform = builder.device_platform;
		this.enter = builder.enter;
		this.ifa = builder.ifa;
		this.locale = builder.locale;
		this.overviews_page = builder.overviews_page;
		this.user_id = builder.user_id;
		this.uuid = builder.uuid;
		this.version = builder.version;
		this.wid = builder.wid;
		this.vid = builder.vid;
		this.villages_version = builder.villages_version;
		this.resume = builder.resume;
		this.mac_address = builder.mac_address;
		this.game_name = builder.game_name;
	}


public class Builder {

	public bool waiting = false;
	public string aid = "";
	public string last_notification_poll = "";
	public string device_locale = "";
	public string device_model = "";
	public string device_os = "";
	public string device_platform = "";
	public string enter = "";
	public string ifa = "";
	public string locale = "";
	public string overviews_page = "";
	public string user_id = "";
	public string uuid = "";
	public string version = "";
	public string game_name = "";
	public string mac_address = "";
	public int skip_error = 0;
	public string wid = "";
	public string vid = "";
	public int villages_version = 0;
	public int resume = 0;

		public Builder setResume(int resumeVal) {
			this.resume = resumeVal;
			return this;
		}

		public Builder setLastNotificationPoll(string notificationPoll) {
			if (Utility.isStringNotNullAndNotEmpty(notificationPoll)) {
				this.last_notification_poll = notificationPoll;
			}
			return this;
		}

		public Builder setAid(string aid) {
			if (Utility.isStringNotNullAndNotEmpty(aid)) {
				this.aid = aid;
			}
			return this;
		}

		public Builder setDeviceLocale(string deviceLocale) {
			if (Utility.isStringNotNullAndNotEmpty (deviceLocale)) {
					this.device_locale = deviceLocale;
			}
			return this;
		}

		public Builder setDeviceModel(string deviceModel) {
			if (Utility.isStringNotNullAndNotEmpty(deviceModel)) {
				this.device_model = deviceModel;
			}
			return this;
		}
			
		public Builder setDeviceOS(string deviceOS) {
			if (Utility.isStringNotNullAndNotEmpty(deviceOS)) {
				this.device_os = deviceOS;
			}
			return this;
		}

		public Builder setDevicePlatform(string devicePlatform) {
			if (Utility.isStringNotNullAndNotEmpty(devicePlatform)) {
				this.device_platform = devicePlatform;
			}
			return this;
		}

		public Builder setEnter(string enter) {
			if (Utility.isStringNotNullAndNotEmpty(enter)) {
				this.enter = enter;
			}
			return this;
		}

		public Builder setIfa(string ifa) {
			if (Utility.isStringNotNullAndNotEmpty(ifa)) {
				this.ifa = ifa;
			}
			return this;
		}

		public Builder setLocale(string locale) {
			if (Utility.isStringNotNullAndNotEmpty(locale)) {
				this.locale = locale;
			}
			return this;
		}


		public Builder setOverviewPage(string overviewsPage) {
			if (Utility.isStringNotNullAndNotEmpty(overviewsPage)) {
				this.overviews_page = overviewsPage;
			}
			return this;
		}


		public Builder setUserId(string userId) {
			if (Utility.isStringNotNullAndNotEmpty(userId)) {
				this.user_id = userId;
			}
			return this;
		}


		public Builder setUuid(string uuid) {
			if (Utility.isStringNotNullAndNotEmpty(uuid)) {
				this.uuid = uuid;
			}
			return this;
		}


		public Builder setVersion(string version) {
			if (Utility.isStringNotNullAndNotEmpty(version)) {
				this.version = version;
			}
			return this;
		}


		public Builder setGameName(string gameName) {
			if (Utility.isStringNotNullAndNotEmpty (gameName)) {
				this.game_name = gameName;
			}

			return this;
		}

		public Builder setMacAddress(string macAddress) {
			if (Utility.isStringNotNullAndNotEmpty (macAddress)) {
				this.mac_address = macAddress;
			}

			return this;
		}

		public Builder setSkipError (int skipError) {
			this.skip_error = skipError;
			return this;
		}
			

		public Builder setWid(string wid)  {
		
			if (Utility.isStringNotNullAndNotEmpty (wid)) {
				this.wid = wid;
			}
			return this;
		}

		public Builder setVid(string vid) {
			if (Utility.isStringNotNullAndNotEmpty (vid)) {
				this.vid = vid;
			}
			return this;
		}


		public Builder setVillageVersion(int villageVersion) {

				this.villages_version = villageVersion;

			return this;
		}

		public Builder setWaiting(bool waiting) {
			this.waiting = waiting;
			return this;
		}


		public GetRequestModel build() {
			GetRequestModel requestModel = new GetRequestModel (this);
			return requestModel;
		} 
	}

}
