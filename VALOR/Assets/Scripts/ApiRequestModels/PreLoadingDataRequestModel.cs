﻿using UnityEngine;
using System.Collections;

public class PreLoadingDataRequestModel : CommonRequestModel{

	public string ifa;
	public int skip_error;
	public string aid;
	public string user_id;
	public string uuid;
	public int village_version;
	public string version;
	public string locale;
	public string device_locale;
	public string game_name;
	public string mac_address;
	public string device_model;
	public string wid;
	public string device_os;
	public string device_platform;


	public PreLoadingDataRequestModel(Builder builder) {
		this.ifa = builder.ifa;
		this.skip_error = builder.skip_error;
		this.aid = builder.aid;
		this.user_id = builder.user_id;
		this.uuid = builder.uuid;
		this.village_version = builder.village_version;
		this.version = builder.version;
		this.locale = builder.locale;
		this.device_locale = builder.device_locale;
		this.game_name = builder.game_name;
		this.mac_address = builder.mac_address;
		this.device_model = builder.device_model;
		this.wid = builder.wid;
		this.device_os = builder.device_os;
		this.device_platform = builder.device_platform;
		}


	public class Builder {

		public string ifa = "";
		public int skip_error = 0;
		public string aid = "";
		public string user_id = "";
		public string uuid = "";
		public int village_version = 0;
		public string version = "";
		public string locale = "";
		public string device_locale = "";
		public string game_name = "";
		public string mac_address = "";
		public string device_model = "";
		public string wid = "";
		public string device_os = "";
		public string device_platform;

		public Builder setIfa(string ifa) {
			if (Utility.isStringNotNullAndNotEmpty (ifa)) {
				this.ifa = ifa;
			}
			return this;
		}


		public Builder setUserId(string userId) {
			if (Utility.isStringNotNullAndNotEmpty (userId)) {
				this.user_id = userId;
			}
			return this;
		}

		public Builder setUuId(string uuid) {
			if (Utility.isStringNotNullAndNotEmpty (uuid)) {
				this.uuid = uuid;
			}
			return this;
		}

		public Builder setVillageVersion(int villageVersion) {
			if(Utility.isGreaterThanZero(villageVersion)) {
				this.village_version = villageVersion;
			}	
			return this;
		}

		public Builder setAid(string aid) {
			if (Utility.isStringNotNullAndNotEmpty (aid)) {
				this.aid = aid;
			}
			return this;
		}

		public Builder setVersion(string version) {
			if (Utility.isStringNotNullAndNotEmpty (version)) {
				this.version = version;
			}

			return this;
		}



		public Builder setLocale(string locale) {
			if (Utility.isStringNotNullAndNotEmpty (locale)) {
				this.locale = locale;
			}

			return this;
		}

		public Builder setDeviceLocale(string device_locale) {
			if (Utility.isStringNotNullAndNotEmpty (device_locale)) {
				this.device_locale = device_locale;
			}

			return this;
		}


		public Builder setGameName(string game_name) {
			if (Utility.isStringNotNullAndNotEmpty (game_name)) {
				this.game_name = game_name;
			}
			return this;
		}



		public Builder setMacAddress(string macAddress) {
			if (Utility.isStringNotNullAndNotEmpty (macAddress)) {
				this.mac_address = macAddress;
			}
			return this;
		}


		public Builder setDeviceModel(string deviceModel) {
			if (Utility.isStringNotNullAndNotEmpty (deviceModel)) {
				this.device_model = deviceModel;
			}
			return this;
		}


		public Builder setWid(string wid) {
			if (Utility.isStringNotNullAndNotEmpty (wid)) {
				this.wid = wid;
			}
			return this;
		}


		public Builder setDeviceOS(string deviceOS) {
			if (Utility.isStringNotNullAndNotEmpty (deviceOS)) {
				this.device_os = deviceOS;
			}
			return this;
		}


		public Builder setDevicePlatform(string devicePlatform) {
			if (Utility.isStringNotNullAndNotEmpty (devicePlatform)) {
				this.device_platform = devicePlatform;
			}
			return this;
		}

		public PreLoadingDataRequestModel build() {
			PreLoadingDataRequestModel  model = new PreLoadingDataRequestModel (this);
			return model;
		} 

		public Builder setSkipError(int error) {
			this.skip_error = error;
			return this;
		}
	}
}

