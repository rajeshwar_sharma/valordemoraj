﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 

public class InitDataModel:CommonRequestModel  {

	public string ifa;
	public int villages_version;
	public string locale;
	public string device_locale;
	public string last_notification_poll;
	public string version;
	public string device_model;
	public string device_platform;
	public int user_id;
	public string wid;
	public int skip_error;
	public string game_name;
	public string mac_address;
	public string uuid;
	public string aid;
	public string userid;

	public InitDataModel(Builder builder) {

		this.ifa = builder.ifa;
		this.villages_version = builder.villagesVersion;
		this.locale = builder.locale;
		this.device_locale = builder.deviceLocale;
		this.last_notification_poll = builder.lastNotificationPoll;
		this.version = builder.version;
		this.device_model = builder.deviceModel;
		this.device_platform = builder.devicePlatform;
		this.user_id = builder.userId;
		this.wid = builder.wid;
		this.skip_error = builder.skipError;
		this.game_name = builder.gameName;
		this.mac_address = builder.macAddress;
		this.uuid = builder.uuid;
		this.aid = builder.aId;
		this.userid = builder.secondUserId;
	}


	public class Builder {
		public string ifa = "";
		public int villagesVersion = -1;
		public string locale = "";
		public string deviceLocale = "";
		public string lastNotificationPoll = "";
		public string version = "";
		public string deviceModel = "";
		public string devicePlatform = "";
		public int userId = -1;
		public string wid = "";
		public int skipError = -1;
		public string gameName = "";
		public string macAddress = "";
		public string uuid = "";
		public string aId = "";
		public string secondUserId = "";

		public Builder setIfa(string ifa) {
			
			if (Utility.isStringNotNullAndNotEmpty(ifa)) {
				this.ifa = ifa;
			}
			return this;
		}

		public Builder setVillageVersion(int villageVersion) {

			if (Utility.isGreaterThanZero (villageVersion)) {
				this.villagesVersion = villageVersion;
			}
			return this;
		}

		public Builder setLocale(string locale) {
			
			if (Utility.isStringNotNullAndNotEmpty(locale)) {
				this.locale = locale;
			}
			return this;
		}

		public Builder setDeviceLocale(string deviceLocale) {
		
			if (Utility.isStringNotNullAndNotEmpty (deviceLocale)) {
			
				this.deviceLocale = deviceLocale;

			}
			return this;
		}

		public Builder setLastNotificationPoll(string lastNotificationPoll) {
		
			if (Utility.isStringNotNullAndNotEmpty (lastNotificationPoll)) {
			
				this.lastNotificationPoll = lastNotificationPoll;

			}
			return this;
		}

		public Builder setVersion(string version) {
		
			if (Utility.isStringNotNullAndNotEmpty (version)) {
			
				this.version = version;
			
			}
			return this;
		}

		public Builder setDeviceModel(string deviceModel) {

			if (Utility.isStringNotNullAndNotEmpty (deviceModel)) {

				this.deviceModel = deviceModel;

			}
			return this;
		}

		public Builder setDevicePlatform(string devicePlatform) {
		
			if (Utility.isStringNotNullAndNotEmpty (devicePlatform)) {

				this.devicePlatform = devicePlatform;

			}
			return this; 
		
		}

		public Builder setUserId(int userId) {

			if (Utility.isGreaterThanZero (userId)) {

				this.userId = userId;

			}
			return this; 

		}


		public Builder setWid(string wid) {

			if (Utility.isStringNotNullAndNotEmpty (wid)) {

				this.wid = wid;

			}
			return this; 

		}

		public Builder setSkipError(int skipError) {

			if (Utility.isGreaterThanZero (skipError)) {

				this.skipError = skipError;

			}
			return this; 

		}

		public Builder setGameName (string gameName) {

			if (Utility.isStringNotNullAndNotEmpty (gameName)) {

				this.gameName = gameName;

			}
			return this;
		}

		public Builder setMacAddress(string macAddress) {
		
			if (Utility.isStringNotNullAndNotEmpty(macAddress)) {

				this.macAddress = macAddress;

			}
			return this;
		}

		public Builder setUuid(string uuid) {
		
			if (Utility.isStringNotNullAndNotEmpty (uuid)) {

				this.uuid = uuid;

			}
			return this;
		}

		public Builder setAId(string aId) {
		
			if (Utility.isStringNotNullAndNotEmpty (aId)) {

				this.aId = aId;

			}
			return this;
		}

		public Builder setSecondId(string secondUserId) {
		
			if (Utility.isStringNotNullAndNotEmpty (secondUserId)) {

				this.secondUserId = secondUserId;
				
			}
			return this;
		}

		public InitDataModel build() {
			InitDataModel requestModel = new InitDataModel (this);
			return requestModel;
		} 
	}

}
