﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrivacySwitch : MonoBehaviour {

	public GameObject switchLeftNub;
	public GameObject switchRightNub;
	private Button switchBtn;
	private Text switchText;
	private bool state;

	// Use this for initialization
	void Start () {
		this.setupPrivacySwitch ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Setups the privacy switch.
	/// </summary>

	void setupPrivacySwitch() {
		this.getObjects ();
		this.addListeners ();
		this.switchLeftNub.SetActive (false);
		this.switchText.alignment = TextAnchor.MiddleLeft;
	}

	/// <summary>
	/// Gets the objects.
	/// </summary>

	void getObjects() {
		//this.switchLeftNub = GameObject.Find (Objects.MyTitlesScreen.switchNubLeft);
		//this.switchRightNub = GameObject.Find (Objects.MyTitlesScreen.switchNubRight);
		this.switchText = GameObject.Find (Objects.MyTitlesScreen.switchText).GetComponent<Text> ();
		this.switchBtn = gameObject.GetComponent<Button> ();
 	}

	/// <summary>
	/// Adds the listeners.
	/// </summary>

	void addListeners() {
		this.switchBtn.onClick.AddListener (switchButtonClicked);
	}

	/// <summary>
	/// Switchs the button clicked.
	/// </summary>

	void switchButtonClicked() {
		this.changeNubVisibility ();
	}

	/// <summary>
	/// Changes the nub visibility.
	/// </summary>

	void changeNubVisibility() {
		if (this.switchRightNub.activeInHierarchy) {
			this.makePrivacyPrivate ();
		} else {
			this.makePrivacyPublic ();
		}
	}

	/// <summary>
	/// Makes the privacy private.
	/// </summary>

	void makePrivacyPrivate() {
		this.changeTextAlignment ();
		this.switchLeftNub.SetActive (true);
		this.switchRightNub.SetActive (false);
	}

	/// <summary>
	/// Makes the privacy public.
	/// </summary>

	void makePrivacyPublic() {
		this.changeTextAlignment ();
		this.switchLeftNub.SetActive (false);
		this.switchRightNub.SetActive (true);
	}

	/// <summary>
	/// Changes the text alignment.
	/// </summary>

	void changeTextAlignment() {
		if (this.switchRightNub.activeInHierarchy) {
			this.switchText.alignment = TextAnchor.MiddleRight;
			this.switchText.text = "Private";
		} else {
			this.switchText.alignment = TextAnchor.MiddleLeft;
			this.switchText.text = "Public";
		}
	}
}
