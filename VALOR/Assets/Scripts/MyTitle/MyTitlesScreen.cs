﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyTitlesScreen : MonoBehaviour {

	public GameObject myTitleScreen;
	public Button backBtn;
	public Button unearnedTitlesBtn;
	public GameObject unearnedTitleList;
	public GameObject unearnedIcon;
	public Button settingsBtn;
	public GameObject settingsScreen;

	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Setups the screen.
	/// </summary>

	void setupScreen() {
		this.getObjects ();
		this.addListerners ();
		this.toggleVisibilityUnearnedList ();
		this.hideMyTitleScreen ();
	}

	/// <summary>
	/// Gets the objects.
	/// </summary>

	void getObjects() {
		this.myTitleScreen = GameObject.Find (Objects.MyTitlesScreen.self);
		this.unearnedTitleList = GameObject.Find (Objects.MyTitlesScreen.unearnedTitleList);
		this.unearnedTitlesBtn = GameObject.Find (Objects.MyTitlesScreen.collapseExpandBtn).GetComponent<Button> ();
		this.unearnedIcon = GameObject.Find (Objects.MyTitlesScreen.collapseExpandIcon);
		this.backBtn = GameObject.Find (Objects.MyTitlesScreen.backBtn).GetComponent<Button> ();
	}

	void addListerners() {
		this.unearnedTitlesBtn.onClick.AddListener (toggleListExpandCollapse);
		this.backBtn.onClick.AddListener (hideMyTitleScreen);
		this.settingsBtn.onClick.AddListener (showSettingsScreen);
	}

	/// <summary>
	/// Hides my title screen.
	/// </summary>

	public void hideMyTitleScreen() {
		Utility.hideGameObject (this.myTitleScreen);
	}

	/// <summary>
	/// Shows my title screen.
	/// </summary>

	public void showMyTitleScreen() {
		Utility.showGameObject (this.myTitleScreen);
	}

	/// <summary>
	/// Hides my title list.
	/// </summary>

	public void hideMyTitleList() {
		Utility.hideGameObject (this.unearnedTitleList);

	}

	/// <summary>
	/// Shows my title list.
	/// </summary>

	public void showMyTitleList() {
		Utility.showGameObject (this.unearnedTitleList);
	}

	/// <summary>
	/// Toggles the list expand collapse.
	/// </summary>

	public void toggleListExpandCollapse() {
		this.changeUnearnedListIcon ();
		this.toggleVisibilityUnearnedList ();
	}

	/// <summary>
	/// Toggles the visibility unearned list.
	/// </summary>

	void toggleVisibilityUnearnedList() {
		if (this.unearnedTitleList.activeInHierarchy) {
			this.hideMyTitleList ();
		} else {
			this.showMyTitleList ();
		}
	}

	public void changeUnearnedListIcon() {
		if (this.unearnedTitleList.activeInHierarchy) {
			Utility.setImageToGameObject (Objects.MyTitlesScreen.collapseExpandIcon, Sprites.MyTitlesButton.titleListCollapseIcon);
		} else {
			Utility.setImageToGameObject (Objects.MyTitlesScreen.collapseExpandIcon, Sprites.MyTitlesButton.titleListExpandIcon);
		}
	}

	public void showSettingsScreen() {
		this.settingsScreen.GetComponent<SettingsScreenOperations> ().openPopUp ();
	}


}
