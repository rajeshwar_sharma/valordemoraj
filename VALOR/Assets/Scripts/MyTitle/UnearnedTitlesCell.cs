﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnearnedTitlesCell : MonoBehaviour {

	public GameObject worldDetailsScreen;

	// Use this for initialization
	void Start () {
		this.setupCell ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Setups the cell.
	/// </summary>

	void setupCell() {
		this.addListeners ();
		this.hideWorldDetailScreen ();
	}

	/// <summary>
	/// Adds the listeners.
	/// </summary>

	void addListeners() {
		gameObject.GetComponent<Button> ().onClick.AddListener (this.showWorldDetailScreen);
	}

	/// <summary>
	/// Shows the world detail screen.
	/// </summary>

	void showWorldDetailScreen() {
		this.worldDetailsScreen.GetComponent<WorldDetails> ().showWorldDetailScreen ();
	}

	/// <summary>
	/// Hides the world detail screen.
	/// </summary>

	void hideWorldDetailScreen() {
		this.worldDetailsScreen.GetComponent<WorldDetails> ().hideWorldDetailScreen ();
	}

}
