﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsScreenOperations : MonoBehaviour {

	public Button closeBtn;
	public Button helpBtn;
	public Button bannerSwitchBtn;
	public Button playerNameChangeBtn;
	public Button emailChangeBtn;
	public Button helpGeneralBtn;
	public Button accountTransferBtn;
	public Button languageEnglishBtn;
	public Button pushSettingsBtn;
	public Button musicSwitchBtn;
	public Button soundSwitchBtn;
	public Button abbreviatedResourceSwitchBtn;
	public Button previousSwipeSwitchBtn;
	public Button graphicalCitySwitchBtn;
	public Button graphicalRecruitmentSwitchBtn;
	public Button priotizeIncomingSwitchBtn;
	public Button notificationSFXSwitchBtn;

	public InputField usernameField;
	public InputField emailField;

	private GameObject settingsScreen;
	private GameObject cityOverviewScreen;
	private GameObject settingHelpScreen;

	public bool isGhaphicalRecruitmentSelected = false;
	// Use this for initialization
	void Start () {
		this.setupSettingsScreen ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
	void setupSettingsScreen() {
		this.getObjects ();
		this.addListerners ();
		this.closePopUp ();
		this.hideAllObject ();
	}

	void getObjects() {
		this.settingsScreen = GameObject.Find (Objects.SettingsScreen.self);
		this.cityOverviewScreen = GameObject.Find ("CityOverviewScreen");
		this.settingHelpScreen = GameObject.Find("SettingHelpScreen");
	}

	void addListerners() {
		this.closeBtn.onClick.AddListener (closePopUp);
		this.helpBtn.onClick.AddListener (helpBtnPressed);
		this.helpGeneralBtn.onClick.AddListener (helpBtnPressed);
		this.graphicalCitySwitchBtn.onClick.AddListener (() => switchButtonPressed(Objects.SettingsScreen.graphicalCitySwitch));
		this.graphicalRecruitmentSwitchBtn.onClick.AddListener (() => switchButtonPressed(Objects.SettingsScreen.graphicalRecruitmentSwitch));

	}

	public void closePopUp() {
		if (this.settingsScreen.activeInHierarchy) {
			this.settingsScreen.SetActive (false);
		}
	}

	public void openPopUp() {
		if (!this.settingsScreen.activeInHierarchy) {
			this.setPlaceHolders ();
			this.settingsScreen.SetActive (true);
		}
	}

	public void hideAllObject() {
		Utility.hideGameObject (this.cityOverviewScreen);
		Utility.hideGameObject (this.settingHelpScreen);
	}
		
	public void helpBtnPressed(){
		Utility.showGameObjectAsLastSibling(this.settingHelpScreen);
	}


	public void switchButtonPressed(string objectName) {
		switch (objectName) {

		case Objects.SettingsScreen.bannerSwitch:
			print ("I am in banner switch");			
			break;
		case Objects.SettingsScreen.musicSwitch:
			print ("I am in Music switch");
			break;
		case Objects.SettingsScreen.soundSwitch:
			print ("I am in sound switch");
			break;
		case Objects.SettingsScreen.abbriviatedResources:
			print ("I am in abbriviated Resources");
			break;
		case Objects.SettingsScreen.previousCitySwipeSwitch:
			print ("I am in previous swipe");
			break;
		case Objects.SettingsScreen.graphicalCitySwitch:
			if (cityOverviewScreen.activeInHierarchy) {
				Utility.hideGameObject (this.cityOverviewScreen);
			} else {
				Utility.showGameObject (this.cityOverviewScreen);
			}
			print ("I am in graphical city");
			break;
		case Objects.SettingsScreen.graphicalRecruitmentSwitch:
			print ("I am in graphical Recruitment switch");

			isGhaphicalRecruitmentSelected = !isGhaphicalRecruitmentSelected;

			break;
		case Objects.SettingsScreen.priotizeIncomingSwitch:
			print ("I am in priotize Incoming switch");
			break;
		case Objects.SettingsScreen.notificationSFXSwitch:
			print ("I am in notification SFX switch");
			break;
		}
	}


	void setPlaceHolders() {
	}

}
