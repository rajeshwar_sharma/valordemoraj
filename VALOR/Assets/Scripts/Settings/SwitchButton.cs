﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchButton : MonoBehaviour {
	
	public GameObject switchButton;
	public bool switchState;

	// Use this for initialization
	void Start () {
		this.addListeners ();
	}

	void addListeners() {
		this.switchButton.GetComponent<Button> ().onClick.AddListener (changeSwitchState);
	}

	void changeSwitchState() {
		if (switchState == false) {
			this.changeStateToOn ();
		} else {
			this.changeStateToOff ();
		}
	}

	public void changeStateToOn() {
		this.switchState = true;
		this.switchPressed ();
		Utility.setImageToGameObject (gameObject.name, Sprites.SwitchButtonSprites.switchButtonOn);
	}

	public void changeStateToOff() {
		this.switchState = false;
		Utility.setImageToGameObject (gameObject.name, Sprites.SwitchButtonSprites.switchButtonOff);
	}

	void switchPressed() {
		//GameObject.Find (Objects.SettingsScreen.self).GetComponent<SettingsScreenOperations> ().switchButtonPressed (gameObject.name);
	}

}
