﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetMoreTicketsScreen : MonoBehaviour {

	public Button closeBtn;
	public GameObject getMoreTicketScreen;

	// Use this for initialization
	void Start () {
		this.setupGetMoreTicketsScreen ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupGetMoreTicketsScreen() {
		this.addListeners ();	
		this.hideGetMoreTicketScreen ();
	}

	void addListeners() {
		this.closeBtn.onClick.AddListener (hideGetMoreTicketScreen);
	}

	public void hideGetMoreTicketScreen() {
		Utility.hideGameObject (this.getMoreTicketScreen);
	}

	public void showGetMoreTicketScreen() {
		Utility.showGameObject (this.getMoreTicketScreen);
	}
}
