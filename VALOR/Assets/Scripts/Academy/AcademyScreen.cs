﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AcademyScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button backButton;
	public IronPopUp ironPopUpScript;
	public GetMoreResourcePopUp GetMoreResourcePopUp;
	public Button settingBtn;
	public GameObject basicBtnObject;
	public Button basicCheckBoxBtn;
	public Button basicBuyFarmBtn;
	public GameObject advancedBtnObject;
	public Button advScholarshipBuyGoldBtn;
	public Button advScholarBuyGoldBtn;
	public Button advPrestigeBuyGoldBtn;


	public ScrollRect scrollRect;
	public GameObject basicVerticalContent;
	public GameObject advancedVerticalContent;

	private Button basicBtn;
	private Button advancedBtn;
	private Image basicBtnImage;
	private Image advancedBtnImage;

	private bool isUnchecked = true;
	const string checkedImage = "Academy/checkmark_on_btn@2x";
	const string uncheckedImage = "Academy/checkbox_off_btn@2x";
	// Use this for initialization   
	void Start () {
		this.setupScreen ();
		this.hideSelf ();
	}
	
	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.basicBtnPressed ();
	}

	private void addObject () {
		this.basicBtn = basicBtnObject.GetComponent<Button> ();
		this.advancedBtn = advancedBtnObject.GetComponent<Button> ();
		this.basicBtnImage = basicBtnObject.GetComponent<Image> ();
		this.advancedBtnImage = advancedBtnObject.GetComponent<Image> ();
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
		this.basicBtn.onClick.AddListener (basicBtnPressed);
		this.basicCheckBoxBtn.onClick.AddListener (basicCheckBoxBtnPressed);
		this.basicBuyFarmBtn.onClick.AddListener (basicBuyFarmBtnPressed);
		this.advancedBtn.onClick.AddListener (advancedBtnPressed);

		this.advScholarshipBuyGoldBtn.onClick.AddListener (advScholarshipBuyGoldBtnPressed);
		this.advScholarBuyGoldBtn.onClick.AddListener (advScholarBuyGoldBtnPressed);
		this.advPrestigeBuyGoldBtn.onClick.AddListener (advPrestigeBuyGoldBtnPressed);
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	void basicBtnPressed() {
		this.clearBasicBtnColor ();
		this.setAdvanceBtnColor ();
		scrollRect.content = basicVerticalContent.GetComponent<RectTransform> ();
		Utility.hideGameObject (this.advancedVerticalContent);
		Utility.showGameObject (this.basicVerticalContent);
	}

	void advancedBtnPressed() {
		clearAdvanceBtnColor ();
		setBasicBtnColor ();
		scrollRect.content = advancedVerticalContent.GetComponent<RectTransform> ();
		Utility.hideGameObject (this.basicVerticalContent);
		Utility.showGameObject (this.advancedVerticalContent);
	}
		
	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}

	private void basicCheckBoxBtnPressed() {
		Image checkBoxImage = basicCheckBoxBtn.GetComponentInParent<Image> ();
		if (isUnchecked) {
			Utility.setImageToImageObject (checkBoxImage, uncheckedImage);
		} else {
			Utility.setImageToImageObject (checkBoxImage, checkedImage);
		}
		isUnchecked = !isUnchecked;
	}

	private void basicBuyFarmBtnPressed() {
		this.ironPopUpScript.showSelfWith ("ACTIVATE PREMIUM?", "This action requires activation of the Premium Scholarship.");
	}

	private void advScholarshipBuyGoldBtnPressed() {
		GetMoreResourcePopUp.showSelf ();
	}
	private void advScholarBuyGoldBtnPressed() {
		GetMoreResourcePopUp.showSelf ();
	}

	private void advPrestigeBuyGoldBtnPressed() {
		this.ironPopUpScript.showSelfWith ("PRESTIGE", "Are you Sure?");
	}

	private void setBasicBtnColor() {
		var tempColor = basicBtnImage.color;
		tempColor.a = 255;
		basicBtnImage.color = tempColor;
	}

	private void clearBasicBtnColor() {
		var tempColor = basicBtnImage.color;
		tempColor.a = 0;
		basicBtnImage.color = tempColor;
	}
	private void setAdvanceBtnColor() {
		var tempColor = advancedBtnImage.color;
		tempColor.a = 255;
		advancedBtnImage.color = tempColor;
	}

	private void clearAdvanceBtnColor() {
		var tempColor = advancedBtnImage.color;
		tempColor.a = 0;
		advancedBtnImage.color = tempColor;
	}

}
