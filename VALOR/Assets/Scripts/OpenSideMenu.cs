﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class OpenSideMenu : MonoBehaviour {

	public Button sideMenuButton; 
	public RectTransform sideMenu;
	public RectTransform canvasPosition;
	private bool isSideMenuClose = true;
	public float speed = 50000.0F;
	private float startTime;
	private float journeyLength;
	// Use this for initialization
	void Start () {

		Button btn = sideMenuButton.GetComponent<Button>();
		btn.onClick.AddListener(openMenu);
		startTime = Time.time;
		journeyLength = Vector3.Distance(sideMenu.position, canvasPosition.position);

	}

	// Update is called once per frame
	void Update () {
		
//		float step = 1.0 * Time.deltaTime;
//		sideMenu.transform.position = Vector3.MoveTowards(sideMenu.transform.position, canvasPosition.transform.position, step);
//		sideMenu.position = Vector3.Lerp (sideMenu.position, canvasPosition.position,1);
	}

	void openMenu() {
		
	}

	void startAnimation() {
		float distCovered = (Time.time - startTime) * speed;
		float fracJourney = distCovered / journeyLength;
		sideMenu.position = Vector3.Lerp(sideMenu.position, canvasPosition.position, fracJourney);
	}

}
