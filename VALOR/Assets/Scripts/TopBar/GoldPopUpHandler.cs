﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldPopUpHandler : MonoBehaviour {

	private GameObject popUpView;
	private Button crossBtn;
	private Button addMoreGold;

	// Use this for initialization
	void Start () {
		this.getObjects ();
		this.addListeners ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Gets the objects.
	/// </summary>

	void getObjects() {
		this.popUpView = GameObject.Find (Objects.GoldPopUp.goldPopUpVw);
		this.crossBtn = GameObject.Find (Objects.GoldPopUp.crossBtn).GetComponent<Button>();
		this.addMoreGold = GameObject.Find (Objects.TopBar.buyGoldBtn).GetComponent<Button>();
	}

	/// <summary>
	/// Adds the listeners.
	/// </summary>

	void addListeners() {
		this.crossBtn.onClick.AddListener (closePopUpView);
		this.addMoreGold.onClick.AddListener (openPopUpView);
	}


	/// <summary>
	/// Closes the pop up view.
	/// </summary>

	void closePopUpView() {
		if (popUpView.activeInHierarchy) {
			popUpView.SetActive (false);
		}
	}

	/// <summary>
	/// Opens the pop up view.
	/// </summary>

	void openPopUpView() {
		if (!popUpView.activeInHierarchy) {
			popUpView.transform.SetAsLastSibling();
			popUpView.SetActive (true);
		}
	}
}
