﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum arrowPosition {
	message = 0 , notification , ranking
};

enum sideMenuPosition{
	rest = 0 , open , close
};

public class MenuOperations : MonoBehaviour {
	
	public GameObject mailReportVw;
	public GameObject notificationVw;
	public GameObject rankingVw;
	private GameObject arrow;
	private GameObject notificationArrow;
	private GameObject rankingArrow;
	private GameObject goldPopUpView;
	private Button openVillageBtn;
	private Button messageBtn;
	private Button notificationBtn;
	private Button rankingBtn;
	private GameObject superView;
	public Button sideMenuBtn;
	private float offSet;
	private Vector3 sideMenuOpenPosition;
	private Vector3 sideMenuClosedPosition;
	private RectTransform sideMenuView;
	private float speed = 6.5f;
	private sideMenuPosition sideMenuState;
	private bool sideMenuFlag;
	private float deltaTime;
	private Button forumBtn;

	// Use this for initialization
	void Start () {
		sideMenuFlag = false;
		this.setupTopBar ();
	}

	// Update is called once per frame
	void Update () {
		if (this.sideMenuFlag == true) {
			this.deltaTime = Time.deltaTime*speed + this.deltaTime;
			this.superView.transform.localPosition = Vector2.Lerp (this.sideMenuClosedPosition,this.sideMenuOpenPosition,this.deltaTime);
			if (this.superView.transform.localPosition == this.sideMenuOpenPosition) {
				this.sideMenuFlag = false;
				this.deltaTime = 0.0f;
				this.swapPositions ();
			}
		} 
	}

	/// <summary>
	/// Setups the top bar.
	/// </summary>

	private void setupTopBar() {
		this.getObjects ();
		this.setSideMenuPosition ();
		this.addListeners ();
		this.hidePopUps ();
	}

	/// <summary>
	/// Get the Game objects.
	/// </summary>

	private void getObjects() {
		this.messageBtn = GameObject.Find (Objects.TopBar.message).GetComponent<Button>();
		this.openVillageBtn = GameObject.Find (Objects.TopBar.openVillageBtn).GetComponent<Button> ();
		this.forumBtn = GameObject.Find (Objects.Notifications.forumBtn).GetComponent<Button> ();
		this.notificationBtn = GameObject.Find (Objects.TopBar.notificationBtn).GetComponent<Button> ();
		this.rankingBtn = GameObject.Find (Objects.TopBar.rankingBtn).GetComponent<Button>();
		this.mailReportVw = GameObject.Find (Objects.ReportsAndMail.mailAndReportVw);
		this.rankingVw = GameObject.Find (Objects.Ranking.rankingVw);
		this.notificationVw = GameObject.Find (Objects.Notifications.notificationVw);
		this.arrow = GameObject.Find (Objects.ReportsAndMail.arrow);
		this.notificationArrow = GameObject.Find (Objects.Notifications.notificationArrow);
		this.rankingArrow = GameObject.Find (Objects.Ranking.rankingArrow);
		this.goldPopUpView = GameObject.Find (Objects.GoldPopUp.goldPopUpVw);
		this.sideMenuBtn = GameObject.Find (Objects.TopBar.sideMenuBtn).GetComponent<Button>();
		this.superView = GameObject.Find (Objects.WholeLayout.villageAndSideMenu);
		this.sideMenuView = GameObject.Find (Objects.SideMenu.sideMenuView).GetComponent<RectTransform>();
	}

	/// <summary>
	/// Adds the listeners to buttons.
	/// </summary>

	private void addListeners() {
		this.openVillageBtn.onClick.AddListener (showVillageScreen);
		this.messageBtn.onClick.AddListener (activeInactiveMessageReportView);
		this.notificationBtn.onClick.AddListener (activeInactiveNotificationView);
		this.rankingBtn.onClick.AddListener (activeInactiveRankingView);
		this.sideMenuBtn.onClick.AddListener (setSuperViewPosition);
	}

	private void setSideMenuPosition() {
		this.sideMenuState = sideMenuPosition.rest;
		this.offSet = this.sideMenuView.rect.width;
		this.sideMenuClosedPosition = this.superView.transform.localPosition;
		this.sideMenuOpenPosition = new Vector3 (this.superView.transform.localPosition.x + this.offSet, this.superView.transform.localPosition.y,0); 
	}

	/// <summary>
	/// Hides the pop up .
	/// -> Message and report pop up
	/// -> Notification pop up
	/// -> Rankings pop up
	/// </summary>

	public void hidePopUps() {
		this.hideMessageReportPopUp ();
		this.hideNotificationPopUp ();
		this.hideRankingPopUp ();
		this.hideGoldPackPopUp ();
		this.hideBottomBar ();
	}

	public void hideBottomBar() {
		GameObject.Find (Objects.BottomBar.bottomBarView).GetComponent<BottomBarMenuButtons> ().hideAllViews ();
	}

	/// <summary>
	/// Hides the message report pop up.
	/// </summary>

	private void hideMessageReportPopUp() {
		if (this.mailReportVw.activeInHierarchy) {
			this.mailReportVw.SetActive (false);
			this.hideBottomBar ();
		}
	}

	/// <summary>
	/// Hides the notification pop up.
	/// </summary>

	private void hideNotificationPopUp() {
		if (this.notificationVw.activeInHierarchy) {
			this.notificationVw.SetActive (false);
			this.hideBottomBar ();
		}
	}

	/// <summary>
	/// Hides the ranking pop up.
	/// </summary>

	private void hideRankingPopUp() {
		if (this.rankingVw.activeInHierarchy) {
			this.hideBottomBar ();
			this.rankingVw.SetActive (false);
		}
	}

	/// <summary>
	/// Hides the gold pack pop up.
	/// </summary>

	private void hideGoldPackPopUp() {
		if (this.goldPopUpView.activeInHierarchy) {
			this.hideBottomBar ();
			this.goldPopUpView.SetActive (false);
		}
	}

	/// <summary>
	/// Sets the arrow position.
	/// </summary>


	void setArrowPosition(arrowPosition position) {
		
		switch (position)
		{
		case arrowPosition.message:
			Vector3 messagePosition = new Vector3 (this.messageBtn.transform.position.x, this.arrow.transform.position.y);
			this.arrow.transform.position = messagePosition;
			this.arrow.transform.localPosition = new Vector3 (this.arrow.transform.localPosition.x,this.arrow.transform.localPosition.y, 1);

			break;
		case arrowPosition.notification:
			Vector3 notificationPosition = new Vector3 (this.notificationBtn.transform.position.x, this.arrow.transform.position.y, 0);
			this.notificationArrow.transform.position = notificationPosition;
			this.notificationArrow.transform.localPosition = new Vector3 (this.notificationArrow.transform.localPosition.x,this.notificationArrow.transform.localPosition.y, 1);
			break;
		case arrowPosition.ranking:
			Vector3 rankingPosition = new Vector3 (this.rankingBtn.transform.position.x, this.rankingArrow.transform.position.y);
			this.rankingArrow.transform.position = rankingPosition;
			this.rankingArrow.transform.localPosition = new Vector3 (this.rankingArrow.transform.localPosition.x,this.rankingArrow.transform.localPosition.y, 1);

			break;
		default:
			break;
		}
	}

	private void showVillageScreen() {
		this.hidePopUps ();
		GameObject.Find ("VillageArea").GetComponent<VillageScreen> ().hideAllScreens ();

	}
		
	/// <summary>
	/// Actives/Inactives message report view.
	/// </summary>
	/// 
	private void activeInactiveMessageReportView() {
		if (this.mailReportVw.activeInHierarchy) {
			this.mailReportVw.SetActive (false);
		} else {
			this.hideNotificationPopUp ();
			this.hideRankingPopUp ();
			this.mailReportVw.SetActive (true);
			this.setArrowPosition (arrowPosition.message);
		}
	}

	/// <summary>
	/// Actives/Inactives Notification view.
	/// </summary>
	/// 
	private void activeInactiveNotificationView() {
		if (this.notificationVw.activeInHierarchy) {
			this.notificationVw.SetActive (false);
		} else {
			this.notificationVw.SetActive (true);
			this.hideMessageReportPopUp ();
			this.hideRankingPopUp ();
			this.setArrowPosition (arrowPosition.notification);
		}
	}

	/// <summary>
	/// Actives/Inactives Ranking view.
	/// </summary>
	/// 
	private void activeInactiveRankingView() {
		if (this.rankingVw.activeInHierarchy) {
			this.rankingVw.SetActive (false);
		} else {
			this.rankingVw.SetActive (true);
			this.hideNotificationPopUp ();
			this.hideMessageReportPopUp ();
			this.setArrowPosition (arrowPosition.ranking);
		}
	}

	private void toggleSideMenu() {
		if (this.sideMenuState == sideMenuPosition.close) {
			this.sideMenuState = sideMenuPosition.open;
		} else if (this.sideMenuState == sideMenuPosition.open) {
			this.sideMenuState = sideMenuPosition.close;
		} 
	}

	private void sideMenuStateChange() {
		switch(this.sideMenuState) {
		case sideMenuPosition.open:
		//	this.setSuperViewPosition (sideMenuClosedPosition, sideMenuOpenPosition);
			break;
		case sideMenuPosition.close:
			// this.setSuperViewPosition (sideMenuOpenPosition,sideMenuClosedPosition);
			break;
		default:
			break;
		}
	}

	void setSuperViewPosition() {
		this.sideMenuFlag = true;
	}

	void swapPositions() {
		Vector2 changePosition = this.sideMenuClosedPosition;
		this.sideMenuClosedPosition = this.sideMenuOpenPosition;
		this.sideMenuOpenPosition = changePosition;
	}
}

