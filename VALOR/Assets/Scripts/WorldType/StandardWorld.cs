﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StandardWorld : MonoBehaviour {

	public GameObject settingsView;
	public GameObject standardWorldCell;
	public Button backBtn;
	public Button settingsBtn;
	public InitCallManager initManager;

	private GameObject standardScrollView;
	private bool cellPopulated = false;
	private VerticalLayoutGroup [] standardLayoutGroup;
	private VerticalLayoutGroup exploredLayoutGroup;
	private VerticalLayoutGroup unexploredLayoutGroup;
	//private string[] userName = new string[]{"Standard [Legacy] (619)", "Gigs (618)", "Muppets Revenenge (624)", "Fennir (629)","", ""};
	private List<string> worldInfo = new List<string>();
	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		//this.hideAllObjects ();
		this.addListeners ();
		this.hideSelf ();
	}



	private void createWorldsArray() {
		worldInfo.Clear ();
		if (this.gameObject.activeInHierarchy) {
			foreach (StandardWorldVM obj in initManager.stdModel) {
				string str = "";
				str = obj.worldName + " (" + obj.worldId + ")";
				worldInfo.Add (str);
			}
		}
	}

	void OnBecameVisible() {
		this.createWorldsArray ();
		this.addExploredAndUnexploredWorlds ();
		LayoutRebuilder.ForceRebuildLayoutImmediate (this.standardScrollView.GetComponentInChildren<RectTransform>());
	}

	void OnDisable() {

	}

	void addExploredAndUnexploredWorlds() {
		if (cellPopulated == false) {
			this.addCellsToContent (Objects.StandardWorld.exploredTag, worldInfo);
			//this.addCellsToContent (Objects.StandardWorld.unExploredTag, worldInfo);
			cellPopulated = true;
		}
	}

	private void addObject() {
		this.standardScrollView = GameObject.Find (Objects.StandardWorld.standardScrollView);
		this.standardLayoutGroup = standardScrollView.GetComponentsInChildren<VerticalLayoutGroup> ();
		foreach (VerticalLayoutGroup eachLayoutGroup in this.standardLayoutGroup) {
			if (eachLayoutGroup.tag == Objects.StandardWorld.exploredTag) {
				this.exploredLayoutGroup = eachLayoutGroup;
			}else if (eachLayoutGroup.tag == Objects.StandardWorld.unExploredTag) {
				this.unexploredLayoutGroup = eachLayoutGroup;
			}
		}
	}

	private void addListeners() {
		this.settingsBtn.onClick.AddListener (settingsBtnPressed);
		this.backBtn.onClick.AddListener (backBtnPressed);
	}


	private void addCellsToContent(string parentLayout,List<string> worldInfomation) {
		foreach (string world in worldInfomation) {
			GameObject cell = Instantiate (standardWorldCell) as GameObject;
			StandardWorldCell cellScript = cell.GetComponent <StandardWorldCell> ();
			cellScript.nameLabel = world;
			cellScript.index = worldInfomation.IndexOf (world);
			if (parentLayout == Objects.StandardWorld.exploredTag) {
				cell.transform.SetParent (exploredLayoutGroup.transform, false);
				cellScript.endedBtn.GetComponentInChildren<Text>().text = "Return";
			} else if (parentLayout == Objects.StandardWorld.unExploredTag) {
				cell.transform.SetParent (unexploredLayoutGroup.transform, false);
			}
			LayoutElement layoutElement = cell.GetComponent<LayoutElement> ();
			if (layoutElement == null) {
				layoutElement = cell.gameObject.AddComponent<LayoutElement> ();
			}
			layoutElement.preferredHeight = 120;
		}
	}


	public void hideAllObjects()  {
		Utility.hideGameObject (this.gameObject);
	}
		
	private void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	private void backBtnPressed() {
		this.hideSelf ();
	}

	void hideSelf() {
		if (this.gameObject.activeInHierarchy){
			Utility.hideGameObject (this.gameObject);
		}
	}

}
