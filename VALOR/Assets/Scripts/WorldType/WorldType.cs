﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldType : MonoBehaviour {

	public GameObject settingsView;
	public GameObject getMoreTicketScreen;
	public GameObject standardWorldScreen;
	public GameObject worldModeScreen;
	public Button backBtn;
	public Button settingsBtn;
	public GameObject initManager;
	public GameObject myWorldCountText;
	public GameObject standardWorldCountText;
	public GameObject guildWorldCountText;

	private Button ticketBtn;
	private Button myWorldBtn;
	private Button standardWorldBtn;
	private Button guildWorldBtn;



	// Use this for initialization
	void Start () {
		this.setupScreen ();

	}

	void OnBecameVisible() {
		this.setTitles ();
	}

	void setupScreen() {
		this.addObjects ();
		this.addListeners ();
		this.hideAllObjects ();
	}

	private void addObjects() {
		this.ticketBtn 		  = GameObject.Find (Objects.WorldType.ticketBtn).GetComponent<Button> ();
		this.myWorldBtn 	  = GameObject.Find (Objects.WorldType.myWorldBtn).GetComponent<Button> ();
		this.standardWorldBtn = GameObject.Find (Objects.WorldType.standardWorldBtn).GetComponent<Button> ();
		this.guildWorldBtn 	  = GameObject.Find (Objects.WorldType.guildWorldBtn).GetComponent<Button> ();
	}
		

	private void addListeners() {
		this.backBtn.onClick.AddListener (backBtnPressed);
		this.settingsBtn.onClick.AddListener (settingsBtnPressed);
		this.ticketBtn.onClick.AddListener (ticketBtnPressed);
		this.myWorldBtn.onClick.AddListener (myWorldBtnPressed);
		this.standardWorldBtn.onClick.AddListener (standardBtnPressed);
		this.guildWorldBtn.onClick.AddListener (guildWorldBtnPressed);

	}

	private void hideAllObjects() {
		//this.hideStandardWorld ();
		//this.hideMyWorld ();
	}

	private void backBtnPressed() {
		if (this.gameObject.activeInHierarchy){
			Utility.hideGameObject (this.gameObject);
		}
	}

	private void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}
		
	private void ticketBtnPressed() {
		if (getMoreTicketScreen.activeInHierarchy) {
			Utility.hideGameObject (this.getMoreTicketScreen);
		} else {
			Utility.showGameObjectAsLastSibling (this.getMoreTicketScreen);
		}
	}
		
	public void myWorldBtnPressed() {
		this.showMyWorld ();
	}

	public void standardBtnPressed() {
		this.showStandardWorld ();
	}

	public void guildWorldBtnPressed() {
		print ("guildWorldBtnPressed");
	}

	public void hideMyWorld() {
		Utility.hideGameObject (this.worldModeScreen);
	}

	public void showMyWorld() {
		Utility.showGameObject (this.worldModeScreen);
	}

	public void hideStandardWorld() {
		Utility.hideGameObject (this.standardWorldScreen);
	}

	public void showStandardWorld() {
		Utility.showGameObject (this.standardWorldScreen);
	}

	public void setTitles() {
		if (this.gameObject.activeInHierarchy) {
			foreach (List<WorldSelectionValueModel> item in this.initManager.GetComponent<InitCallManager>().initMode.world.world_selection_order) {
				for (int i = 0; i < item.Count; i++) {
					WorldSelectionValueModel value = item [i];
					if (value.title == "My Worlds") {
						this.myWorldCountText.GetComponent<Text> ().text = value.subtitle;
					} else if (value.title == "Standard") {
						this.standardWorldCountText.GetComponent<Text> ().text = value.subtitle;
					} else {
						this.guildWorldCountText.GetComponent<Text> ().text = value.subtitle;
					}
				}
			}
		}
	}
}
