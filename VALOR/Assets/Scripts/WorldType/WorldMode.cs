﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldMode : MonoBehaviour {

	public GameObject settingsView;
	public GameObject standardWorldCell;
	public Button backBtn;
	public Button settingsBtn;
	private GameObject worldScrollView;
	private VerticalLayoutGroup worldLayoutGroup;
	private string[] userName = new string[]{"Standard [Legacy] (619)", "Gigs (618)", "Muppets Revenenge (624)", "Fennir (629)","Nostos", ""};

	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
		this.addCellsToContent ();	
		this.hideSelf ();
	}


	private void addObject() {
		this.worldScrollView = GameObject.Find (Objects.worldMode.worldScrollView);
		this.worldLayoutGroup = worldScrollView.GetComponentInChildren<VerticalLayoutGroup> ();
	
	}
		
	private void addListeners() {
		this.settingsBtn.onClick.AddListener (settingsBtnPressed);
		this.backBtn.onClick.AddListener (backBtnPressed);
	}

	private void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}


	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void hideSelf() {
		if (this.gameObject.activeInHierarchy){
			Utility.hideGameObject (this.gameObject);
		}
	}

	private void addCellsToContent() {
		for (int i = 0; i < 5; i++) {
			GameObject cell = Instantiate (standardWorldCell) as GameObject;
			cell.transform.SetParent (worldLayoutGroup.transform, false);
			LayoutElement layoutElement = cell.GetComponent<LayoutElement>();
			if (layoutElement == null) {
				layoutElement = cell.gameObject.AddComponent<LayoutElement>();
			}
			layoutElement.preferredHeight = 120;
			StandardWorldCell cellScript = cell.GetComponent <StandardWorldCell>();
			cellScript.nameLabel =  this.userName[i];
			cellScript.index = i;

			if (i == 2 || i == 3 || i== 4) {
				cellScript.setReturnButton();
			}
		}
	}





}
