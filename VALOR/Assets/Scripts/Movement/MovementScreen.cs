﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovementScreen : MonoBehaviour {

	public GameObject incomingObj;
	public GameObject outgoingObj;
	public GameObject incomingSegment;
	public GameObject outgoingSegment;
	public GameObject noMovementCell;


	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.addListerners ();
	}

	void addListerners() {
		this.incomingObj.GetComponent<Button>().onClick.AddListener (incomingBtnPressed);
		this.outgoingObj.GetComponent<Button>().onClick.AddListener (outgoingBtnPressed);
	}


	void incomingBtnPressed() {
		this.toggleButtons (this.incomingObj, this.outgoingObj);
		this.changeTextColors (this.incomingObj, this.outgoingObj);
		this.activeSegment (this.incomingSegment);
		this.inActiveSegment (this.outgoingSegment);
		this.changeNoMovementText (Strings.MovementScreen.noIncomingMovementMsg);
	}

	void outgoingBtnPressed() {
		this.toggleButtons (this.outgoingObj, this.incomingObj);
		this.changeTextColors (this.outgoingObj, this.incomingObj);
		this.activeSegment (this.outgoingSegment);
		this.inActiveSegment (this.incomingSegment);
		this.changeNoMovementText (Strings.MovementScreen.noOutgoingMovementMsg);
	}

	void toggleButtons(GameObject activeImage,GameObject inActiveImage) {
		activeImage.GetComponent<Image> ().color = Colors.movementToggleHightedBg;
		inActiveImage.GetComponent<Image> ().color = Colors.movementToggleDeHighlightedBg;
	}

	void changeTextColors(GameObject activeText,GameObject inActiveText) {
		activeText.GetComponentInChildren<Text> ().color = Color.black;
		inActiveText.GetComponentInChildren<Text> ().color = Color.white;
	}

	void activeSegment(GameObject segment) {
		Utility.hideGameObject (segment);
	}

	void inActiveSegment(GameObject segment) {
		Utility.hideGameObject (segment);
	}

	void changeNoMovementText(string txt) {
		this.noMovementCell.GetComponentInChildren<Text> ().text = txt;
	}
		
}
