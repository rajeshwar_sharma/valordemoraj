﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;


/// <summary>
/// Unity web request manager.
/// This manager is used make the network request
/// </summary>

public class UnityWebRequestManager :  NetworkInterface {


	/// <summary>
	/// Makes the Get request.
	/// </summary>
	/// <returns>The request.</returns>
	/// <param name="request"> Contains a get request</param>.</param>
	/// <param name="responseCallBack">Response call back for success or failure</param>

//	public IEnumerator getRequest<T> (CommonRequest request, ApiResponseReceiver responseCallBack) {
//	
//		if (this.isInternetAvailable()) {
//			using (UnityWebRequest webRequest = UnityWebRequest.Get (request.url)) {
//				webRequest.url = request.url;
//				yield return webRequest.SendWebRequest ();
//				//this.callbackHandler (webRequest, responseCallBack);
//			}
//		} else {
//			responseCallBack.failure(Strings.networkNotAvailable);
//
//		}
//
//	}

	/// <summary>
	/// Makes the post request.
	/// </summary>
	/// <param name="request"> Constains a post request</param>
	/// <param name="responseCallBack">Constains a response callback for success and failure.</param>

	public IEnumerator makePostRequest<T,MODEL> (CommonRequest request,ApiResponseReceiver responseCallBack) {

		if (this.isInternetAvailable()){
			using (UnityWebRequest webRequest = UnityWebRequest.Post (request.url, this.getRawData (request))) {
				this.setHeaders (webRequest, request);
				webRequest.url = request.url;
				webRequest.uploadHandler = this.setUploadHandler (request);
				yield return webRequest.SendWebRequest();
				this.callbackHandler <T,MODEL> (webRequest, responseCallBack);
			}
		}
		else {

			responseCallBack.failure(Strings.networkNotAvailable); 

		}

	}

	private void callbackHandler<T,MODEL> (UnityWebRequest request,ApiResponseReceiver responseCallBack) {
		if (request.isNetworkError || request.isHttpError) {
			Error error = new Error (request.responseCode, request.error);
			ErrorHandler<T>.errorResolver (error, responseCallBack);
		} else {
			MODEL model = JsonConvert.DeserializeObject<MODEL>(request.downloadHandler.text);
			responseCallBack.success(model);
		}
	}

	/// <summary>
	/// Sets the headers.
	/// </summary>
	/// <param name="webRequest">Web request.</param>
	/// <param name="request">Request.</param>
		
	private void setHeaders(UnityWebRequest webRequest,CommonRequest request) {
	
		if (request.headers != null) {
			if (request.headers.Values.Count != 0) {
					this.addheadersToRequest (webRequest, request.headers);
				} else {
					this.addheadersToRequest (webRequest, CustomRequestSettings.Instance.getDefaultHeaders());
				}
			}
	}

	private void addheadersToRequest(UnityWebRequest request,Dictionary<string,string> headers) {
		foreach(KeyValuePair<string, string> entry in headers) {
			request.SetRequestHeader (entry.Key,entry.Value);
		}
	}

	/// <summary>
	/// Sets the upload handler.
	/// </summary>
	/// <returns>The upload handler.</returns>
	/// <param name="request">Request.</param>

	private UploadHandlerRaw setUploadHandler(CommonRequest request) {
		string rawData = JsonUtility.ToJson (request.requestModel);
		byte[] bytes = getBytes (rawData);
		UploadHandlerRaw uploadHandler= new UploadHandlerRaw(bytes);
		uploadHandler.contentType= this.getContentType(request.contentType);
		return uploadHandler;
	}

	/// <summary>
	/// Gets the type of the content.
	/// </summary>
	/// <returns>The content type.</returns>
	/// <param name="contentType">Content type.</param>

	private string getContentType(string contentType) {
		if (contentType == "") {
			return CustomRequestSettings.Instance.getDefaultContentType();
		}
		return contentType;
	}

	/// <summary>
	/// Gets the raw data.
	/// </summary>
	/// <returns>The raw data.</returns>
	/// <param name="request">Request.</param>

	private string getRawData(CommonRequest request) {
		return JsonUtility.ToJson (request.requestModel);
	}

	/// <summary>
	/// Gets the bytes.
	/// </summary>
	/// <returns>The bytes.</returns>
	/// <param name="str">String.</param>

	private static byte[] getBytes(string str){
		byte[] bytes = System.Text.Encoding.UTF8.GetBytes(str);
		return bytes;
	}

	/// <summary>
	/// Is internet available.
	/// </summary>
	/// <returns><c>true</c>, if internet available was ised, <c>false</c> otherwise.</returns>

	private bool isInternetAvailable() {
		if (Application.internetReachability == NetworkReachability.NotReachable) {
			return false;
		} 
		return true;
	}
}
