﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomRequestSettings  {

	/// <summary>
	/// The instance.
	/// </summary>

	private static CustomRequestSettings instance;

	private CustomRequestSettings() {}

	/// <summary>
	/// Gets the instance.
	/// </summary>
	/// <value>The instance.</value>

	public static CustomRequestSettings Instance
	{
		get 
		{
			if (instance == null)
			{
				instance = new CustomRequestSettings();
			}
			return instance;
		}
	}

	private string baseURL = "";
	private string contentType = "";
	private Dictionary<string, string> headers = new Dictionary<string, string>
	{
		{ "", "" }
	};
		
	public string baseUrl {

		get {
			return baseURL;
		}
	}

	public string defaultContentType {

		get {
			return contentType;
		}
	}

	public Dictionary <string,string> defaultHeaders  {
	
		get {
			return headers;
		}
	}

	/// <summary>
	/// Gets the base URL.
	/// </summary>
	/// <returns>The base URL.</returns>

	public string getBaseUrl() {
		return baseUrl;
	}

	/// <summary>
	/// Get default type of the content.
	/// </summary>
	/// <returns>Default content type.</returns>

	public string getDefaultContentType() {
		return defaultContentType;	
	}

	/// <summary>
	/// Get default headers.
	/// </summary>
	/// <returns>The default headers.</returns>

	public Dictionary<string,string> getDefaultHeaders() {
		return defaultHeaders;
	}

	/// <summary>
	/// Sets the base URL.
	/// </summary>
	/// <param name="url">URL.</param>

	public void setBaseUrl(string url) {
		baseURL = url;
	}

	/// <summary>
	/// Sets the default type of the content.
	/// </summary>
	/// <param name="contenttype">Contenttype.</param>

	public void setDefaultContentType(string contenttype) {
		contentType = contenttype;
	}

	/// <summary>
	/// Sets the default headers.
	/// </summary>
	/// <param name="headersDict">Headers dict.</param>

	public void setDefaultHeaders(Dictionary <string,string> headersDict) {
		headers = headersDict;
	}

	/// <summary>
	/// Sets the default settings.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="contentType">Content type.</param>
	/// <param name="headers">Headers.</param>

	public void setDefaultSettings(string url,string contentType,Dictionary<string,string> headers) {
		this.setBaseUrl (url);
		this.setDefaultContentType (contentType);
		this.setDefaultHeaders (headers);
	}
 }
