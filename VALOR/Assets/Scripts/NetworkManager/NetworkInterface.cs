﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;


/// <summary>
/// Network interface.
/// </summary>

public interface NetworkInterface  {
	IEnumerator makePostRequest <T,MODEL> (CommonRequest request, ApiResponseReceiver responseCallBack);
	//IEnumerator getRequest<T,MODEL>  (CommonRequest request, ApiResponseReceiver<T> responseCallBack,MODEL responseModel);
}
