﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Error.
/// Error class with error code and description
/// </summary>

public class Error {

	public long errorCode = -1;
	public string description = "";

	public Error(long code,string description) {
		this.errorCode = code;
		this.description = description;
	}
}

/// <summary>
/// Error handler.
/// Error resolver to send appropriate message
/// </summary>

public class ErrorHandler<T>  {

	/// <summary>
	/// Errors the resolver.
	/// </summary>
	/// <param name="error">Error.</param>
	/// <param name="responseCallBack">Response call back.</param>

	static public void errorResolver(Error error,ApiResponseReceiver responseCallBack) {
		 string errorMessage = ErrorList.getMessageForErrorCode(error);
		 responseCallBack.failure (errorMessage);
	}
}

/// <summary>
/// Error list.
/// Error list class contains list of error with messages
/// </summary>

public class ErrorList  {

	/// <summary>
	/// The errors.
	/// Dictionary contains error list
	/// </summary>

	static public Dictionary<long,string> errors = new Dictionary<long, string> ();

	/// <summary>
	/// Gets the message for error code.
	/// </summary>
	/// <returns>The message for error code.</returns>
	/// <param name="error">Error.</param>

	static public string getMessageForErrorCode(Error error) {
		ErrorList.setErrorDictionary ();
		if (errors.ContainsKey (error.errorCode)) {
			string message = errors [error.errorCode];
			return message;
		} else {
			return error.description;
		}
	}

	/// <summary>
	/// Sets the error dictionary.
	/// </summary>

	 static public void setErrorDictionary() {
		errors.Add (404, "Page Not found");
	}
}


