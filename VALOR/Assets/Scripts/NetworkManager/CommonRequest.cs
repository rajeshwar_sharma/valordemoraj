﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Common request model.
/// </summary>

public class CommonRequestModel {
}
	

/// <summary>
/// Common request.
/// </summary>

public class CommonRequest  {
	public string url = "";
	public string contentType = "";
	public Dictionary<string,string> headers;
	public CommonRequestModel requestModel;
}

/// <summary>
/// Common get request.
/// </summary>

public class CommonGetRequest:CommonRequest {

	/// <summary>
	/// The get request body.
	/// </summary>

	public Dictionary<string,string> getRequestBody = new Dictionary<string, string> ();

	/// <summary>
	/// Initializes a new instance of the <see cref="CommonGetRequest"/> class.
	/// </summary>
	/// <param name="url">URL.</param>

	public  CommonGetRequest(string url) {
		this.url = url;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="CommonGetRequest"/> class.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="body">Body.</param>

	public  CommonGetRequest(string url,Dictionary<string,string> body) {
		this.url = url;
		this.getRequestBody = body;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="CommonGetRequest"/> class.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="body">Body.</param>
	/// <param name="headers">Headers.</param>

	public CommonGetRequest(string url, Dictionary<string,string> body , Dictionary <string,string> headers) {
		this.url = url;
		this.getRequestBody = body;
		this.headers = headers;
	}
}
