﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ApiResponseReceiver{

	void success<MODEL>(MODEL response);
	void failure(string error);

}