﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldDetails : MonoBehaviour {

	public GameObject infoTab;
	public GameObject optionsTab;
	public Button infoTabBtn;
	public Button optionsTabBtn;
	public Button backBtn;
	public Button optionBtn;
	public GameObject optionSection;
	public GameObject infoTabSection;
	public Button settingBtn;
	public GameObject settingsScreen;


	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.addListeners ();
		this.hideWorldDetailScreen ();
	}

	void getObjects() {

	}

	void addListeners() {
		this.infoTabBtn.onClick.AddListener (infoBtnPressed);
		this.optionsTabBtn.onClick.AddListener (optionsBtnPressed);
		this.backBtn.onClick.AddListener (hideWorldDetailScreen);
		this.settingBtn.onClick.AddListener (showSettingsScreen);
	}

	void infoBtnPressed() {
		this.hideOptionsTab ();
		this.showInfoTab ();
	}

	void optionsBtnPressed() {
		this.hideInfoTab ();
		this.showOptionsTab ();
	}

	void hideOptionsTab() {
		Utility.hideGameObject (this.optionsTab);
		Utility.hideGameObject (this.optionSection);
	}

	void hideInfoTab() {
		Utility.hideGameObject (this.infoTab);
		Utility.hideGameObject (this.infoTabSection);
	}

	void showOptionsTab() {
		Utility.showGameObject (this.optionsTab);
		Utility.showGameObject (this.optionSection);
	}

	void showInfoTab() {
		Utility.showGameObject (this.infoTab);
		Utility.showGameObject (this.infoTabSection);
	}

	public void hideWorldDetailScreen() {
		Utility.hideGameObject (this.gameObject);
	}

	public void showWorldDetailScreen() {
		Utility.showGameObject (this.gameObject);
	}

	public void showSettingsScreen() {
		this.settingsScreen.GetComponent<SettingsScreenOperations> ().openPopUp ();
	}
}
