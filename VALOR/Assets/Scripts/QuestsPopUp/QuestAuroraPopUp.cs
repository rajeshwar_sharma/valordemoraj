﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestAuroraPopUp : MonoBehaviour {

	private Button closeButton;
	private Button okButton;
	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}

	private void setupScreen() {
		this.addObject ();
		this.addListners ();
	}

	private void addObject () {
		Button[] allButtons;
		allButtons = GameObject.Find ("QuestAuroraPopUp").GetComponentsInChildren <Button> ();
		if (allButtons != null) {
			foreach (Button button in allButtons) {
				if (button.tag == "close") {
					this.closeButton = button;
				} else {
					this.okButton = button;
				}
			}
		}
	}

	private void addListners () {
		this.closeButton.onClick.AddListener (closeBtnPressed);
		this.okButton.onClick.AddListener (closeBtnPressed);
	}

	private void closeBtnPressed () {
		if (this.gameObject.activeInHierarchy) {
			Utility.hideGameObject (this.gameObject);
		}
	}
}
