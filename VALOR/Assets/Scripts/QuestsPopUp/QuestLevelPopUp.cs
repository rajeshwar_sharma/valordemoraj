﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestLevelPopUp : MonoBehaviour {

	private Button closeButton;

	public Text headerTitle;

	const string wheelAmuletImage = "QuestLevelPopUp/wheelAmulet@2x";
	const string wheelClayImage = "QuestLevelPopUp/wheelClay@2x";
	const string wheelIronImage = "QuestLevelPopUp/wheelIron@2x";
	const string wheelWoodImage = "QuestLevelPopUp/wheelWood@2x";
	const string curlWeaponImage = "QuestLevelPopUp/t_a@2x";
	const string bowsWeaponImage = "QuestLevelPopUp/t_c@2x";
	const string arrowsWeaponImage = "QuestLevelPopUp/t_sf@2x";
	const string swordsWeaponImage = "QuestLevelPopUp/t_sw@2x";


	// Use this for initialization
	void Start () {
		this.setupScreen ();
	}
	
	private void setupScreen() {
		this.addObject ();
		this.addListners ();
	}

	private void addObject () {
		this.closeButton = GameObject.Find ("QuestLevelPopUp").GetComponentInChildren <Button> ();

	}
		
	private void addListners () {
		this.closeButton.onClick.AddListener (closeBtnPressed);
	}

	private void closeBtnPressed () {
			Utility.hideGameObject (this.gameObject);
	}

	public void hideSelf() {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf() {
			Utility.showGameObject (this.gameObject);
	}

	public void showSelfWith(string header) {
		headerTitle.text = header;
		Utility.showGameObject (this.gameObject);
	}


}
