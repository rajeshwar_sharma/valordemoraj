﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageGoldPack : MonoBehaviour {

	private GridLayoutGroup grid;
	public 	GameObject buyCell;
	public RectTransform gridRect;
	public Button closeBtn;
	public GameObject goldPopUp;

	// Use this for initialization
	void Start () {
		this.setupGoldPack ();
	}

	// Update is called once per frame
	void Update () {
		
	}

	void setupGoldPack() {
		this.getObjects ();
		this.addListerners ();
	}

	void getObjects() {
		this.grid = GameObject.Find (Objects.GoldPopUp.grid).GetComponent<GridLayoutGroup> ();
		this.gridRect = (RectTransform)grid.transform;
	}

	void addListerners() {

	}

	public void addObjectsToGridView(InitModel initMod) {

		float cellDimension = (float)((gridRect.rect.width / 2)-20);
		grid.spacing = new Vector2 (15, 15);
		grid.padding = new RectOffset (15,15,15,15);
		grid.cellSize = new Vector2 (cellDimension, cellDimension);

		for (int i = 0; i < (initMod.android_google_gold.data.Count-1); i++) {
			GameObject newCell = Instantiate (buyCell) as GameObject;
			GoldModel[] goldMod = new GoldModel[initMod.android_google_gold.data.Count];
			initMod.android_google_gold.data.Values.CopyTo (goldMod, 0);
			newCell.GetComponent<GoldPack> ().setLabels (goldMod[i]);
			newCell.transform.SetParent (grid.transform);
			newCell.transform.localScale = new Vector3 (1, 1 , 1);
			newCell.transform.localPosition = new Vector3 (0, 0, 0);
		}
	}
}
