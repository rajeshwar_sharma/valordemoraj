﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GoldPack : MonoBehaviour {

	public string packName { get;set;}
	public string cost { get; set;}
	public string symbol { get; set;}
		
	// Use this for initialization
	void Start () {
		this.initializeObjects ();
	}

	
	// Update is called once per frame
	void Update () {

	}

	void initializeObjects() {
		GameObject.Find ("MoneyLabel").GetComponent<Text> ().name = cost;
		GameObject.Find ("ActualMoneyLabel").GetComponent<Text> ().name = cost;
		GameObject.Find ("PackName").GetComponent<Text> ().name = packName;
 	}


	public void setLabels(GoldModel goldPack) {
		GameObject.Find ("MoneyLabel").GetComponent<Text> ().text = goldPack.gold.ToString();
		GameObject.Find ("ActualMoneyLabel").GetComponent<Text> ().text = "$" + goldPack.price.ToString();
		GameObject.Find ("PackName").GetComponent<Text> ().text = goldPack.title;
	}
}




