﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RallyCommandDataReqest : CommonRequest
{

	/// <summary>
	/// Initializes a new instance of the <see cref="RallyCommandDataReqest"/> class.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="contentType">Content type.</param>
	/// <param name="headers">Headers.</param>
	/// <param name="requestModel">Request model.</param>
	public RallyCommandDataReqest(string url,string contentType,Dictionary<string,string> headers,RallyCommandDataRequestModel requestModel) {
		this.url = url;
		this.contentType = contentType;
		this.headers = headers;
		this.requestModel = requestModel;
	}
}

