﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitDataRequest : CommonRequest {

	/// <summary>
	/// Initializes a new instance of the <see cref="InitDataRequest"/> class.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="contentType">Content type.</param>
	/// <param name="headers">Headers.</param>
	/// <param name="requestModel">Request model.</param>

	public InitDataRequest(string url,string contentType,Dictionary<string,string> headers,InitDataModel requestModel) {
		this.url = url;
		this.contentType = contentType;
		this.headers = headers;
		this.requestModel = requestModel;
	}
}