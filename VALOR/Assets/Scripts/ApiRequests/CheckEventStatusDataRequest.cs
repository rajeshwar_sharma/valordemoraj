﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckEventStatusDataRequest : CommonRequest
{
	public CheckEventStatusDataRequest(string url,string contentType,Dictionary<string,string> headers,CheckEventStatusDataRequestModel requestModel) {
		this.url = url;
		this.contentType = contentType;
		this.headers = headers;
		this.requestModel = requestModel;
	}
}

