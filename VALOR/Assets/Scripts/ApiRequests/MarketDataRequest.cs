﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MarketDataRequest : CommonRequest
{
	/// <summary>
	/// Initializes a new instance of the <see cref="MarketDataRequest"/> class.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="contentType">Content type.</param>
	/// <param name="headers">Headers.</param>
	/// <param name="requestModel">Request model.</param>
	public MarketDataRequest(string url,string contentType,Dictionary<string,string> headers,MarketDataRequestModel requestModel) {
		this.url = url;
		this.contentType = contentType;
		this.headers = headers;
		this.requestModel = requestModel;
	}
}

