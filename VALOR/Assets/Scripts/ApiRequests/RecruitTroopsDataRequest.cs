﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RecruitTroopsDataRequest : CommonRequest
{
	/// <summary>
	/// Initializes a new instance of the <see cref="RecruitTroopsDataRequest"/> class.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="contentType">Content type.</param>
	/// <param name="headers">Headers.</param>
	/// <param name="requestModel">Request model.</param>
	public RecruitTroopsDataRequest(string url,string contentType,Dictionary<string,string> headers,RecruitTroopsDataRequestModel requestModel) {
		this.url = url;
		this.contentType = contentType;
		this.headers = headers;
		this.requestModel = requestModel;
	}
}

