﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetProcessDataRequest : CommonRequest {

	/// <summary>
	/// Initializes a new instance of the <see cref="GetDataRequest"/> class.
	/// </summary>
	/// <param name="url">URL.</param>
	/// <param name="contentType">Content type.</param>
	/// <param name="headers">Headers.</param>
	/// <param name="requestModel">Request model.</param>

	public GetProcessDataRequest(string url,string contentType,Dictionary<string,string> headers,GetProcessDataRequestModel requestModel) {
		this.url = url;
		this.contentType = contentType;
		this.headers = headers;
		this.requestModel = requestModel;
	}
}
