﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WorldToWorldVM  {

	public World worldModel;

	public static List<StandardWorldVM> worldToStandardWorldVM(World world,Config config) {
		List<StandardWorldVM> standardWorlds = new List<StandardWorldVM> ();
		Dictionary<string,string[]> dict = new Dictionary<string,string[]>();
		dict = world.worlds_in_v2 as Dictionary<string,string[]>;
		List<string> keys = new List<string> (world.worlds_in_v2.Keys);
		foreach (string key in keys)
		{
			List<string> worldIdLists = new List<string> (world.worlds_in_v2 [key]);
			foreach (var worldId in worldIdLists) {
				StandardWorldVM obj = new StandardWorldVM ();
				obj.worldId = worldId;
				obj.worldName = config.world_names[obj.worldId];
				standardWorlds.Add (obj);
			}
		}
		return standardWorlds;
	}
}
