﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class StandardWorldVM  {
	public string worldName { get; set;}
	public string worldId { get; set;}
}
