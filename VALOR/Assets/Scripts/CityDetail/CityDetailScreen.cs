﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CityDetailScreen : MonoBehaviour {

	public GameObject settingsView;
	public Button backButton;
	public Button settingBtn;
	public Button playerBtn;
	public Button guildBtn;
	public Button centerOnMapBtn;
	public Button sendTroopsBtn;
	public Button instantSpyBtn;
	public Button sendResourceBtn;
	public Button bookmarkBtn;

	void Start () {
		this.setupScreen ();
		this.hideSelf ();
	}

	private void setupScreen() {
		this.addListeners ();
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
		this.playerBtn.onClick.AddListener (playerBtnPressed);
		this.guildBtn.onClick.AddListener (guildBtnPressed);
		this.centerOnMapBtn.onClick.AddListener (centerOnMapBtnPressed);
		this.sendTroopsBtn.onClick.AddListener (sendTroopsBtnPressed);
		this.instantSpyBtn.onClick.AddListener (instantSpyBtnPressed);
		this.sendResourceBtn.onClick.AddListener (sendResourceBtnPressed);
		this.bookmarkBtn.onClick.AddListener (bookmarkBtnPressed);
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void playerBtnPressed() {  }

	public void guildBtnPressed() { }

	public void centerOnMapBtnPressed() { }

	public void sendTroopsBtnPressed() { }

	public void instantSpyBtnPressed() { }

	public void sendResourceBtnPressed() { }

	public void bookmarkBtnPressed() { }



	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}



}
