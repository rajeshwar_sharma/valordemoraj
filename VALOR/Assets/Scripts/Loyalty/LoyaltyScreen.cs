﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoyaltyScreen : MonoBehaviour {


	public GameObject loyaltyScreen;
	public GameObject loyaltyBackBtn;
	public GameObject loyaltySettingsBtn;
	public GameObject settingsScreen;


	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView (){
		this.addListeners ();
		this.hideLoyaltyScreen ();
	}

	void addListeners() {
		this.loyaltyBackBtn.GetComponent<Button> ().onClick.AddListener (hideLoyaltyScreen);
		this.loyaltySettingsBtn.GetComponent<Button> ().onClick.AddListener (showSettingsScreen);
	}

	public void hideLoyaltyScreen() {
		Utility.hideGameObject (loyaltyScreen);
	}

	public void showLoyaltyScreen() {
		Utility.showGameObject (loyaltyScreen);
	}

	public void showSettingsScreen() {
		this.settingsScreen.GetComponent<SettingsScreenOperations> ().openPopUp ();
	}
}
