﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LumberMillScreen : MonoBehaviour {

	public Button lumberMillBackBtn;
	public Button settingsBtn;
	public GameObject settingsScreen;
	int terminatingPosition = 40;

	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.addListeners ();
		this.hideLumberMillScreen ();
	}

	void addListeners() {
		this.settingsBtn.onClick.AddListener (showSettingsScreen);
		this.lumberMillBackBtn.onClick.AddListener (hideLumberMillScreen);
	}

	public void hideLumberMillScreen (){
		Utility.hideGameObject (this.gameObject);
	}

	public void showLumberMillScreen() {
		Utility.showGameObject (this.gameObject);
	}

	void showSettingsScreen() {
		this.settingsScreen.GetComponent<SettingsScreenOperations> ().openPopUp ();
	}
		
}
