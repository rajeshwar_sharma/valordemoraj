﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SettingHelpScreen : MonoBehaviour {

	public Button backButton;
	public Button closeButton;

	// Use this for initialization
	void Start () {
		this.setupScreen ();
		//this.hideSelf ();
	}
		
	private void setupScreen() {
		this.addListeners ();
	}

	private void addListeners() {
		this.backButton.onClick.AddListener (backBtnPressed);
		this.closeButton.onClick.AddListener (backBtnPressed);
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}






}
