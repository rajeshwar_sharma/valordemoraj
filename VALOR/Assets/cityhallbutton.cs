﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cityhallbutton : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.setupView ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void setupView() {
		this.gameObject.GetComponent<Button> ().onClick.AddListener (cityHallButtonPressed);
	}

	void cityHallButtonPressed() {
		print ("City Hall Button Pressed");
	}
}
