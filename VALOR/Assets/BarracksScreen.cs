﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class BarracksScreen : MonoBehaviour,IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerUpHandler, IPointerDownHandler {

	public ScrollRect scrollRect;
	public RectTransform backgroundRect;
	public RectTransform horizontalLayoutRect;
	public RectTransform LancerImageRect;
	public RectTransform SentryImageRect;
	public RectTransform BerserkerImageRect;
	public RectTransform ScoutImageRect;
	public RectTransform KnightImageRect;
	public RectTransform GuardianImageRect;
	public RectTransform RamImageRect;
	public RectTransform BallistaImageRect;

	public GameObject settingsView;
	public Button backButton;
	public Button settingBtn;

	double previousScrollPos = 0.0;
	Vector2 zoomInPos = new Vector2(600,680);
	Vector2 zoomOutPos = new Vector2(530,600);

	public void OnPointerDown(PointerEventData eventData)
	{
		scrollRect.OnBeginDrag(eventData);
		Debug.Log("OnPointerDown was called for object " + gameObject.name);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		scrollRect.OnEndDrag(eventData);
		Debug.Log("OnPointerUp was called for object " + gameObject.name);
	}

	public void OnBeginDrag (PointerEventData eventData){
		Debug.Log  ("OnBeginDrag");	
	}
	public void OnDrag (PointerEventData eventData){
		scrollRect.OnDrag(eventData);
		print ("OnDrag");
	
	}
	public void OnEndDrag (PointerEventData eventData){
		print ("OnEndDrag");
	}


	// Use this for initialization
	void Start () {

		scrollRect.onValueChanged.AddListener(scrollViewDragging);
		//LancerImageRect.sizeDelta = zoomInPos;
		//SentryImageRect.sizeDelta = zoomOutPos;
		//LancerImageRect.localPosition = new Vector2 (LancerImageRect.localPosition.x, -18);
		//SentryImageRect.localPosition = new Vector2 (SentryImageRect.localPosition.x, -80);
		this.addListeners ();
		this.hideSelf ();
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
	}

	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}


	float x = 0;
	Vector2 scrollPos = new Vector2 ();
	void scrollViewDragging(Vector2 scrollPos) {
		x = 0;
		this.scrollPos = scrollPos;

			if (scrollPos.x < .295) {
				//Use for Scroll Barracks Background View-> 900 , 820
				x = (float)(900 - (Math.Round (scrollPos.x, 4) * 270));

			} else if (scrollPos.x < .75) {
				//Use for Stable Background View->  +15, -62
				x = (float)(900 - ((Math.Round (scrollPos.x, 4) - 0.295) * 100) - 915);
			} else if ((Math.Round (scrollPos.x, 3)) >= previousScrollPos) {
				//Use for Workshop Background View->  -860 , -900 , For Forword Scrolling
				x = (float)(900 - ((Math.Round (scrollPos.x, 4) - 0.75) * 160) - 1760);
			} else {
				if (scrollPos.x < .815){
					x = (float)(900 - ((Math.Round (scrollPos.x, 4) - 0.295) * 100) - 915);
				}
			}



		
	//	print (x+"  "+ horizontalLayoutRect.localPosition.x  +"  "+scrollPos.x  );
		float y = backgroundRect.localPosition.y;
		backgroundRect.localPosition = new Vector2(x,y);

		//updateFrontScroll ();
		//CancelInvoke("updateFrontScroll");
		//InvokeRepeating ("updateFrontScroll", 0.02f, 0f);
		//doScroll();
		//CancelInvoke("doScroll");
		//InvokeRepeating ("doScroll", 0.03f, 0f);

	}
		
	enum Attackers { Lancer, Sentry, Berserker, Scout, Knight, Guardian, Ram, Ballista};
	Attackers currentAttacker =  Attackers.Lancer;


	public void doScroll() {
		print ("Inside DoScroll" +" " );
		previousScrollPos = (Math.Round (scrollPos.x, 3));
		switch (currentAttacker) {
			case Attackers.Lancer:
				{	
				if ((Math.Round (scrollPos.x, 3)) < 0.02) {
					horizontalLayoutRect.localPosition = new Vector2 ((-400), horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement();
					currentAttacker = Attackers.Lancer;
				} else {
					//This increase the ScrollRect to 0.152 and Characters Pos to -850
					horizontalLayoutRect.localPosition = new Vector2 (-850, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement();
					currentAttacker = Attackers.Sentry;
				}
				break;
				}

			case Attackers.Sentry:
				{
				if (0.101 <= (Math.Round (scrollPos.x, 3)) && (Math.Round (scrollPos.x, 3)) <= 0.152) {
					horizontalLayoutRect.localPosition = new Vector2 ((-850), horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Sentry;
				} else if ((Math.Round (scrollPos.x, 3)) > 0.152) {
					//This increase the ScrollRect to 0.265 and Characters Pos to -1300  
					horizontalLayoutRect.localPosition = new Vector2 (-1300, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Berserker;
				} else {
					//This decrease the ScrollRect to 0 and Characters Pos to -400 
					horizontalLayoutRect.localPosition = new Vector2 (-400, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Lancer;
				}
				break;
				}

			case Attackers.Berserker:
				{
				if (0.242 <= (Math.Round (scrollPos.x, 3)) && (Math.Round (scrollPos.x, 3)) <= 0.295) {



					horizontalLayoutRect.localPosition = new Vector2 ((-1300), horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Berserker;
				} else if ((Math.Round (scrollPos.x, 3)) > 0.295) {
					//This increase the ScrollRect to 0.405 and Characters Pos to -1780  
					horizontalLayoutRect.localPosition = new Vector2 (-1780, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Scout;
				} else {
					//This decrease the ScrollRect to 0.132 and Characters Pos to -850 
					horizontalLayoutRect.localPosition = new Vector2 (-850, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Sentry;
				}
				break;
				}

			case Attackers.Scout:
				{
				if (0.370 <= (Math.Round (scrollPos.x, 3)) && (Math.Round (scrollPos.x, 3)) <= 0.45) {
					horizontalLayoutRect.localPosition = new Vector2 ((-1780), horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Scout;
				} else if ((Math.Round (scrollPos.x, 3)) > 0.45) {
					//This increase the ScrollRect to 0.706 and Characters Pos to -1780  
					horizontalLayoutRect.localPosition = new Vector2 (-2300, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Knight;
				} else {
					//This decrease the ScrollRect to 0.405 and Characters Pos to -1300 
					horizontalLayoutRect.localPosition = new Vector2 (-1300, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Berserker;
				}
					break;
				}
			case Attackers.Knight:
				{
				if (0.520 <= (Math.Round (scrollPos.x, 3)) && (Math.Round (scrollPos.x, 3)) <= 0.590) {
					horizontalLayoutRect.localPosition = new Vector2 ((-2300), horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Knight;
				} else if ((Math.Round (scrollPos.x, 3)) > 0.590) {
					//This increase the ScrollRect to 0.706 and Characters Pos to -2800  
					horizontalLayoutRect.localPosition = new Vector2 (-2800, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Guardian;
				} else {
					//This decrease the ScrollRect to 0.56 and Characters Pos to -1780 
					horizontalLayoutRect.localPosition = new Vector2 (-1780, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Scout;
				}
				break;
				}
			case Attackers.Guardian:
				{
				if (0.690 <= (Math.Round (scrollPos.x, 3)) && (Math.Round (scrollPos.x, 3)) <= 0.749) {
					horizontalLayoutRect.localPosition = new Vector2 ((-2800), horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Guardian;
				} else if ((Math.Round (scrollPos.x, 3)) > 0.749) {
					//This increase the ScrollRect to 0.853 and Characters Pos to -2800  
					horizontalLayoutRect.localPosition = new Vector2 (-3300, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Ram;
				} else {
					//This decrease the ScrollRect to 0.56 and Characters Pos to -1780 
					horizontalLayoutRect.localPosition = new Vector2 (-2300, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Knight;
				}
					break;
				}

			case Attackers.Ram:
				{
				if (0.815 <= (Math.Round (scrollPos.x, 3)) && (Math.Round (scrollPos.x, 3)) <= 0.885) {
					horizontalLayoutRect.localPosition = new Vector2 ((-3300), horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Ram;
				} else if ((Math.Round (scrollPos.x, 3)) > 0.885) {
					//This increase the ScrollRect to 0.706 and Characters Pos to -3800  
					horizontalLayoutRect.localPosition = new Vector2 (-3800, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Ballista;
				} else {
					//This decrease the ScrollRect to 0.56 and Characters Pos to -2800 
					horizontalLayoutRect.localPosition = new Vector2 (-2800, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement ();
					currentAttacker = Attackers.Guardian;
				}
					break;
				}
			case Attackers.Ballista:
				{
				if ((Math.Round (scrollPos.x, 3)) >= 0.965) {
					horizontalLayoutRect.localPosition = new Vector2 ((-3800), horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement();
					currentAttacker = Attackers.Ballista;
				} else {
					//This increase the ScrollRect to 0.1 and Characters Pos to -3300
					horizontalLayoutRect.localPosition = new Vector2 (-3300, horizontalLayoutRect.localPosition.y);
					scrollRect.StopMovement();
					currentAttacker = Attackers.Ram;
				}
					break;
				}
		}
	
	}







	public void updateFrontScroll(){
		//Scrolling Forward -> Right to Left x decreases
		if ( (Math.Round (scrollPos.x, 3)) >= previousScrollPos) {

			print ("Going Forward " + x + "->"+ horizontalLayoutRect.localPosition.x +" " + Math.Round (scrollPos.x, 3) + "  "+ previousScrollPos);

			if (x > 896) {
				horizontalLayoutRect.localPosition = new Vector2 (-400, horizontalLayoutRect.localPosition.y);
			}

			//Sentry Image in Center
			if (x <= 895 && x > 856) {

				// Vector3 endPos = new Vector3 (-850, horizontalLayoutRect.localPosition.y, 0);
				// horizontalLayoutRect.localPosition = Vector3.Lerp(horizontalLayoutRect.localPosition, endPos, Time.deltaTime*20.0f);

				horizontalLayoutRect.localPosition = new Vector2 (-850, horizontalLayoutRect.localPosition.y);
			
				scrollRect.StopMovement();
				this.animateObjects (LancerImageRect,SentryImageRect,BerserkerImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Berserker Image in Center
			if (x <= 856 && x > 820) {
				horizontalLayoutRect.localPosition = new Vector2 (-1300, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (SentryImageRect,BerserkerImageRect,ScoutImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Scout Image in Center
			if (x <= 10 && x > -18) {
				horizontalLayoutRect.localPosition = new Vector2 (-1780, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (BerserkerImageRect,ScoutImageRect,KnightImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Knight Image in Center
			if (x <= -18 && x > -38) {
				horizontalLayoutRect.localPosition = new Vector2 (-2300, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (ScoutImageRect,KnightImageRect,GuardianImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}
			//Guardian Image in Center
			if (x <= -38 && x > -62) {
				horizontalLayoutRect.localPosition = new Vector2 (-2800, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (KnightImageRect,GuardianImageRect,RamImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}
			//Ram Image in Center
			if (x <= -62.5 && x > -880) {
				horizontalLayoutRect.localPosition = new Vector2 (-3300, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (GuardianImageRect,RamImageRect,BallistaImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}
			//Ballista Image in Center
			if (x <= -880 ) {
				horizontalLayoutRect.localPosition = new Vector2 (-3800, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				BallistaImageRect.sizeDelta = zoomInPos;
				RamImageRect.sizeDelta = zoomOutPos;
				BallistaImageRect.localPosition = new Vector2 (BallistaImageRect.localPosition.x, -22);
				RamImageRect.localPosition = new Vector2 (RamImageRect.localPosition.x, -70);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}
			scrollRect.StopMovement();

		} else {

			print ("Return Scrolling " +  Math.Round (scrollPos.x, 3) + "  "+ previousScrollPos);

			//Lancer Image in Center
			if (x >= 870) {
				
//				Vector3 endPos = new Vector3 (-400, horizontalLayoutRect.localPosition.y, 0);
//				horizontalLayoutRect.localPosition = Vector3.Lerp(horizontalLayoutRect.localPosition, endPos, Time.deltaTime*20.0f);
				horizontalLayoutRect.localPosition = new Vector2 ((-400), horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();

				LancerImageRect.sizeDelta = zoomInPos;
				SentryImageRect.sizeDelta = zoomOutPos;
				LancerImageRect.localPosition = new Vector2 (LancerImageRect.localPosition.x, -18);
				SentryImageRect.localPosition = new Vector2 (SentryImageRect.localPosition.x, -80);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Sentry Image in Center
			if (x >= 834 && x < 870) {
				horizontalLayoutRect.localPosition = new Vector2 (-850, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (LancerImageRect,SentryImageRect,BerserkerImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Berserker Image in Center
			if (x >= -0 && x < 834) {
				horizontalLayoutRect.localPosition = new Vector2 (-1300, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (SentryImageRect,BerserkerImageRect,ScoutImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Scout Image in Center
			if (x >= -41 && x < -0) {
				horizontalLayoutRect.localPosition = new Vector2 (-1780, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (BerserkerImageRect,ScoutImageRect,KnightImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Knight Image in Center
			if (x >= -68 && x < -41) {
				horizontalLayoutRect.localPosition = new Vector2 (-2300, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (ScoutImageRect,KnightImageRect,GuardianImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Guardian Image in Center
			if (x >= -870 && x < -68) {
				horizontalLayoutRect.localPosition = new Vector2 (-2800, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (KnightImageRect,GuardianImageRect,RamImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			//Ram Image in Center
			if (x >= -894 && x < -870) {
				horizontalLayoutRect.localPosition = new Vector2 (-3300, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				this.animateObjects (GuardianImageRect, RamImageRect, BallistaImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}
			//Ballista Image in Center
			if (x < -894) {
				horizontalLayoutRect.localPosition = new Vector2 (-3800, horizontalLayoutRect.localPosition.y);
				scrollRect.StopMovement();
				//this.animateObjects (GuardianImageRect, RamImageRect, BallistaImageRect);
				previousScrollPos = (Math.Round (scrollPos.x, 3));
				return;
			}

			scrollRect.StopMovement();

		}
	//	previousScrollPos = (Math.Round (scrollPos.x, 3));
		scrollRect.StopMovement();
	}



	public void animateObjects(RectTransform leftObject, RectTransform midObject, RectTransform rightObject){
		//For Zooming
		leftObject.sizeDelta = zoomOutPos;
		midObject.sizeDelta = zoomInPos;
		rightObject.sizeDelta = zoomOutPos;

//		Vector3 scale = new Vector3(0.8f,0.8f,0f);
//		leftObject.transform.localScale = Vector3.Lerp (leftObject.transform.localScale, scale, Time.deltaTime*10.0f);

		//For Y Positions
		leftObject.localPosition = new Vector2 (leftObject.localPosition.x, -80);
		midObject.localPosition = new Vector2 (midObject.localPosition.x, -20);
		rightObject.localPosition = new Vector2 (rightObject.localPosition.x, -80);

	}












}
