﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OfferDetailScreen : MonoBehaviour {


	public GameObject settingsView;
	public GameObject cityDetailScreen;
	public Button backButton;
	public Button settingBtn;
	public Button cityBtn;
	public GameObject incrementerCell;
	public Button acceptBtn;

	private Button increaseBtn;
	private Button decreaseBtn;
	private Text amountOfResource;

	void Start () {
		this.setupScreen ();
		this.hideSelf ();
	}
	
	private void setupScreen() {
		this.addObject ();
		this.addListeners ();
	}

	private void addObject() {
		foreach (Button buttn in incrementerCell.GetComponentsInChildren<Button>()) {
			if (buttn.tag == Objects.decrementBtn) {
				this.decreaseBtn = buttn;
			} 
			if (buttn.tag == Objects.incrementBtn) {
				this.increaseBtn = buttn;
			}
		}
		foreach (Text text in incrementerCell.GetComponentsInChildren<Text>()) {
			if (text.tag == "AmountOfResource") {
				amountOfResource = text;
			} 
		}
	}

	private void addListeners() {
		this.settingBtn.onClick.AddListener (settingsBtnPressed);
		this.backButton.onClick.AddListener (backBtnPressed);
		this.cityBtn.onClick.AddListener (cityBtnPressed);
		this.acceptBtn.onClick.AddListener (acceptBtnPressed);
		this.decreaseBtn.onClick.AddListener (decreaseBtnPressed);
		this.increaseBtn.onClick.AddListener (inceaseBtnPressed);
	}
		
	public void settingsBtnPressed() {
		if (settingsView.activeInHierarchy) {
			Utility.hideGameObject (this.settingsView);
		} else {
			Utility.showGameObjectAsLastSibling (this.settingsView);
		}
	}

	public void backBtnPressed() {
		this.hideSelf ();
	}

	public void cityBtnPressed() {
		this.cityDetailScreen.GetComponent<CityDetailScreen> ().showSelf ();
	}

	public void decreaseBtnPressed() {
		int amount = System.Convert.ToInt32(amountOfResource.text);
		if (amount > 1) {
			amountOfResource.text = (amount - 1).ToString ();
		}
	}

	public void inceaseBtnPressed() {
		int amount = System.Convert.ToInt32(amountOfResource.text);
		amountOfResource.text = (amount + 1).ToString ();
	}

	public void acceptBtnPressed() {
		
	}

	public void hideSelf () {
		Utility.hideGameObject (this.gameObject);
	}

	public void showSelf () {
		Utility.showGameObject (this.gameObject);
	}


}
